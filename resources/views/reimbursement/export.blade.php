<!DOCTYPE html>
<html>
<head>
    <title>Rembes</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Nama</b></th>
                <th><b>Status</b></th>
                <th><b>Keterangan</b></th>
                <th><b>Total</b></th>
            </tr>
        </thead>
        <tbody>
            @php 
                $total_all = 0;
            @endphp
            @foreach($reimbursements as $key => $reimbursement)
                @php 
                    $total_all += $reimbursement->total;
                @endphp
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$reimbursement->karyawan->nama_lengkap}}</td>
                    <td>{{staticHelper()['status'][$reimbursement->status]['text']}}</td>
                    <td>{{$reimbursement->keterangan}}</td>
                    <td>{{$reimbursement->total}}</td>
                </tr>
            @endforeach
                <tr>
                    <td colspan="4" style="text-align: right"><b>TOTAL SEMUA</b></td>
                    <td>{{$total_all}}</td>
                </tr>
        </tbody>
    </table>
</body>
</html>