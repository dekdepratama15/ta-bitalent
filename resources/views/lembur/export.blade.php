<!DOCTYPE html>
<html>
<head>
    <title>Lembur</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Nama</b></th>
                <th><b>Jam Mulai</b></th>
                <th><b>Jam Selesai</b></th>
                <th><b>Status</b></th>
                <th><b>Biaya per Jam</b></th>
                <th><b>Total</b></th>
            </tr>
        </thead>
        <tbody>
            @php 
                $total_all = 0;
            @endphp
            @foreach($lemburs as $key => $lembur)
                @php 
                    $total_all += $lembur->total;
                @endphp
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$lembur->karyawan->nama_lengkap}}</td>
                    <td>{{date('Y-m-d, H:s', strtotime($lembur->jam_mulai))}}</td>
                    <td>{{date('Y-m-d, H:s', strtotime($lembur->jam_selesai))}}</td>
                    <td>{{staticHelper()['status'][$lembur->status]['text']}}</td>
                    <td>{{$lembur->bayar}}</td>
                    <td>{{$lembur->total}}</td>
                </tr>
            @endforeach
                <tr>
                    <td colspan="6" style="text-align: right"><b>TOTAL LEMBUR</b></td>
                    <td>{{$total_all}}</td>
                </tr>
        </tbody>
    </table>
</body>
</html>