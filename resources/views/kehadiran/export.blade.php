<!DOCTYPE html>
<html>
<head>
    <title>Cuti</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <td></td>
                <td style="background-color: #10b981"></td>
                <td colspan="5">On Time</td>
            </tr>
            <tr>
                <td></td>
                <td style="background-color: #ff5724"></td>
                <td colspan="5">Telat</td>
            </tr>
            <tr>
                <td></td>
                <td style="background-color: #f000b9"></td>
                <td colspan="5">Cuti</td>
            </tr>
            <tr>
                <th colspan="{{$numberDay + 1}}" style="text-align:center"><b>Absensi {{ staticHelper()['select_month'][$month-1] }} {{ $year }}</b></th>
            </tr>
            <tr>
                <td><b>Tanggal</b></td>
                @for($i = 1; $i <= $numberDay; $i++)
                    <td><b>{{$i}}</b></td>
                @endfor
            </tr>
        </thead>
        <tbody>
            @foreach($kehadirans as $key => $value)
                <tr>
                    <td>{{$value[0]->karyawan->nama_lengkap}}</td>
                    @for($i = 1; $i <= $numberDay; $i++)
                        @php 
                            $haveData = false;
                            $haveCuti = false;
                            $days = str_pad($i, 2, "0", STR_PAD_LEFT);
                            $months = str_pad($month, 2, "0", STR_PAD_LEFT);
                            if(count($value->where('tanggal', $year.'-'.$months.'-'.$days)) > 0) {
                                $haveData = true;
                                $data = $value->where('tanggal', $year.'-'.$months.'-'.$days)->values()[0];
                                if ($data != null) {
                                    if ($data->status == 'ontime') {
                                        $color = '#10b981';
                                    } else {
                                        $color = '#ff5724';
                                    }
                                }
                            } else {
                                $data = $value[0]->karyawan->cutis->where('tgl_mulai', '<=' , '2023-12-'.$i.' 00:00:00')->where('tgl_selesai', '>=' , '2023-12-'.$i.' 00:00:00')->whereIn('status', ['approve_admin', 'approve_lead']);
                                if (count($data) > 0) {
                                    $haveCuti = true;
                                    $color = '#f000b9';
                                }
                            }
                            @endphp
                            <td
                            @if($haveCuti || $haveData)
                                style="background-color: {{$color}}"
                            @endif
                            ></td>
                    @endfor
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>