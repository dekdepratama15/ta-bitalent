@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.time_management')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Detail Kehadiran
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('kehadiran.index') }}"
            >Manajemen Waktu</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('kehadiran.index') }}"
            >Kehadiran</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Detail Kehadiran</li>
        </ul>
    </div>

    <form method="post">
        @csrf
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            <div class="col-span-12 sm:col-span-12">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Kehadiran
                    </p>
                    <div class="mt-4 space-y-4">
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-4">
                            <label class="block">
                                <span>Nama Karyawan</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Nama Karyawan"
                                    type="text"
                                    name="nama_kehadiran"
                                    value="{{$kehadiran->karyawan->nama_lengkap}}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-regular fa-user"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Tanggal</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Tanggal"
                                    name="tgl"
                                    type="text"
                                    value="{{ date('d M Y', strtotime($kehadiran->tanggal)) }}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-regular fa-calendar"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Jam Masuk</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Jam Masuk"
                                    name="jam_masuk"
                                    type="text"
                                    value="{{ date('H:i', strtotime($kehadiran->time_in)) }}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-regular fa-clock"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Jam Keluar</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Jam Keluar"
                                    name="jam_keluar"
                                    type="text"
                                    value="{{ !empty($kehadiran->time_out) ? date('H:i', strtotime($kehadiran->time_out)) : '--:--'}}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-regular fa-clock"></i>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <div class="grid grid-cols-1 gap-3 sm:grid-cols-3">
                            <label class="block">
                                <span>Status</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Status"
                                    type="text"
                                    name="nama_kehadiran"
                                    value="{{staticHelper()['status'][$kehadiran->status]['text']}}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Lat Long In</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Status"
                                    type="text"
                                    name="nama_kehadiran"
                                    value="{{$kehadiran->latlong_in}}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Lat Long Out</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Status"
                                    type="text"
                                    name="nama_kehadiran"
                                    value="{{!empty($kehadiran->latlong_out) ? $kehadiran->latlong_out : '-'}}"
                                    disabled
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    </span>
                                </span>
                            </label>
                        </div>
                        <div class="grid grid-cols-1 gap-2 sm:grid-cols-2">
                            <label class="block">
                                <span>Alamat Masuk</span>
                                <textarea
                                rows="4"
                                placeholder="Alamat"
                                class="form-textarea mt-1.5 w-full rounded-lg border border-slate-300 bg-transparent p-2.5 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                name="alamat"
                                disabled
                                >{{$kehadiran->alamat_in}}</textarea>
                            </label>
                            <label class="block">
                                <span>Alamat Keluar</span>
                                <textarea
                                rows="4"
                                placeholder="Alamat"
                                class="form-textarea mt-1.5 w-full rounded-lg border border-slate-300 bg-transparent p-2.5 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                name="alamat"
                                disabled
                                >{{$kehadiran->alamat_out}}</textarea>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Report Harian
                    </p>
                    <div class="mt-4 space-y-4">
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                        <div class="flex items-center justify-between col-span-12">
                            <span
                            >
                            Report Harian 
                            </span>
                        </div>
                        <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                            <table class="is-zebra w-full text-left">
                            <thead>
                                <tr>
                                    <th
                                        class="whitespace-nowrap rounded-l-lg bg-slate-200 px-3 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        #
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Foto
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Keterangan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($kehadiran->kehadiran_details) > 0)
                                    @foreach($kehadiran->kehadiran_details as $key => $value)
                                        <tr>
                                            <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">{{$loop->iteration}}</td>
                                            <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center">
                                                <a href="{{ asset($value->file) }}" class="fancybox_file">
                                                    <div class="avatar flex">
                                                        <img class="rounded-full" src="{{ asset($value->file) }}" alt="avatar">
                                                    </div>
                                                </a>
                                            </td>
                                            <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">{{$value->keterangan}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="3">Tidak ada Report Harian</td>
                                    </tr>
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
    
    
            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
    
                    <div class="flex justify-end space-x-2">
                        <a 
                        type="button"
                        href="{{route('kehadiran.index')}}"
                        class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                        >
                        <span>Kembali</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
    </main>
@endsection



