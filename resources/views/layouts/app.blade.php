<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bitalent - Admin</title>
    <link rel="icon" type="image/png" href="{{ url('admin/images/favicon.png') }}" />

    <!-- CSS Assets -->
    <link href="{{ url('admin/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Javascript Assets -->
    <script src="{{ url('admin/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />
    <script>
      localStorage.getItem("_x_darkMode_on") === "true" &&
        document.documentElement.classList.add("dark");
    </script>
</head>
<body x-data class="is-header-blur" x-bind="$store.global.documentBody">
    <!-- App preloader-->
    <div
        class="app-preloader fixed z-50 grid h-full w-full place-content-center bg-slate-50 dark:bg-navy-900"
    >
        <div class="app-preloader-inner relative inline-block h-48 w-48"></div>
    </div>

    <!-- Page Wrapper -->
    <div
        id="root"
        class="min-h-100vh flex grow bg-slate-50 dark:bg-navy-900"
        x-cloak
    >
    @guest
    @else
        @include('layouts.sidenav')
    @endguest
    @yield('content')
    <div id="x-teleport-target"></div>
    <script>
      window.addEventListener("DOMContentLoaded", () => Alpine.start());
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $(document).ready(function () {
            $('.fancybox_file').fancybox();
            $('.delete-data').click(function (e) { 
                e.preventDefault();
                var id = $(this).data("id");
                var token = $(this).data("token");
                var route = $(this).data("route");
                Swal.fire({
                    title: 'Yakin?',
                    text: "Data akan dihapus!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4f46e5',
                    cancelButtonColor: '#ff5724',
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "delete",
                            url: route,
                            data: {
                                "id": id,
                                "_method": 'DELETE',
                                "_token": $('meta[name=csrf-token]').attr('content'),
                            },
                            success: function (response) {
                                if (response.error != undefined) {
                                    Swal.fire(
                                        'Gagal!',
                                        response.message,
                                        'error'
                                    )
                                } else {
                                    Swal.fire(
                                        'Berhasil!',
                                        response.message,
                                        'success'
                                    )
                                    .then((result) => {
                                        location.reload();
                                    });
                                }

                            }
                        });
                    }
                })
            });

            $('.change-status-data').on('click', function (e) {
                e.preventDefault();
                var token = $(this).data("token");
                var route = $(this).data("route");
                var status = $(this).data("status");
                Swal.fire({
                    title: 'Yakin?',
                    text: "Status akan diubah menjadi "+ ucWord(replaceUnderscore(status)) +"!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4f46e5',
                    cancelButtonColor: '#ff5724',
                    confirmButtonText: 'Ubah!',
                    cancelButtonText: 'Batal'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: route,
                            data: {
                                "status" : status,
                                "_method": 'POST',
                                "_token": $('meta[name=csrf-token]').attr('content'),
                            },
                            success: function (response) {
                                Swal.fire(
                                    'Berhasil!',
                                    response.message,
                                    'success'
                                )
                                .then((result) => {
                                    location.reload();
                                });

                            }
                        });
                    }
                })
            })
        });
    </script>

    <script>
        function replaceUnderscore(str) {
            return str.replace(/_/g, ' ');
        }

        function ucWord(str) {
            var str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            return str;
        }
        function numberWithCommas(str) {
            return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>

    @if(session('success'))
        <script>
            $(document).ready(function() {
                Swal.fire(
                    'Berhasil!',
                    '{{session("success")}}',
                    'success'
                );
            });
        </script>
    @endif

    @if(session('error'))
        <script>
            $(document).ready(function() {
                Swal.fire(
                    'Gagal!',
                    '{{session("error")}}',
                    'error'
                );
            });
        </script>
    @endif


    @yield('script')
</body>
</html>
