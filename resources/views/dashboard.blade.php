@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.dashboard')
@endsection

@section('content')
<main class="main-content w-full pb-8">
    <div
        class="mt-4 grid grid-cols-12 gap-4 px-[var(--margin-x)] transition-all duration-[.25s] sm:mt-5 sm:gap-5 lg:mt-6 lg:gap-6"
    >
        <div class="col-span-12 lg:col-span-8">
        <div>
              <div class="flex h-8 items-center justify-between">
                <h2
                  class="font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100"
                >
                  Kehadiran
                </h2>

                <select
                    class="form-select h-8 rounded-full border border-slate-300 bg-slate-50 px-2.5 pr-9 text-xs+ hover:border-slate-400 focus:border-primary dark:border-navy-600 dark:bg-navy-900 dark:hover:border-navy-400 dark:focus:border-accent"
                    id="select_month"
                >
                  @foreach(staticHelper()['select_month'] as $key => $month)
                    <option value="{{$key+1}}" {{ ($key + 1) == $month_select ? 'selected' : '' }}>{{$month}}</option>
                  @endforeach
                </select>
                <input type="hidden" value="{{ route('dashboard') }}" id="route-dashboard">
              </div>

              <div>
                
                <script>
                        var data_kehadiran = JSON.parse('{!! $data_kehadiran !!}');
                        var tanggal_kehadiran = JSON.parse('{!! $tanggal_kehadiran !!}');
                        var text_testing = {colors:["#a855f7"],series:[{name:"Jumlah",data:data_kehadiran}],chart:{height:268,type:"line",parentHeightOffset:0,toolbar:{show:!1},dropShadow:{enabled:!0,color:"#1E202C",top:18,left:6,blur:8,opacity:.1}},stroke:{width:5,curve:"smooth"},xaxis:{type:"datetime",categories: tanggal_kehadiran,tickAmount:10,labels:{formatter:function(e,t,i){return i.dateFormatter(new Date(t),"dd MMM")}}},yaxis:{labels:{offsetX:-12,offsetY:0}},fill:{type:"gradient",gradient:{shade:"dark",gradientToColors:["#86efac"],shadeIntensity:1,type:"horizontal",opacityFrom:1,opacityTo:.95,stops:[0,100,0,100]}},grid:{padding:{left:0,right:0}}};
                </script>
                <div
                  x-init="$nextTick(() => { $el._x_chart = new ApexCharts($el,text_testing); $el._x_chart.render() });"
                ></div>
              </div>
            </div>
        </div>
        <div class="col-span-12 lg:col-span-4">
        <div
            class="grid grid-cols-2 gap-3 sm:grid-cols-3 sm:gap-5 lg:grid-cols-2"
        >
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between space-x-1">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{ number_format($total_pinjaman, 0, '.', ',') }} 
                </p>
                <i class="fa-solid fa-rupiah-sign text-primary"></i>
            </div>
            <p class="mt-1 text-xs+">Pinjaman Karyawan</p>
            </div>
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{count($penggajian)}}
                </p>
                <i class="fa-regular fa-circle-check text-success"></i>
            </div>
            <p class="mt-1 text-xs+">Penggajian Bulan Ini</p>
            </div>
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{count($lembur)}}
                </p>
                <i class="fa-regular fa-clock text-warning"></i>
            </div>
            <p class="mt-1 text-xs+">Permintaan Lembur</p>
            </div>
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{ count($reimbursement) }}
                </p>
                <i class="fa-solid fa-money-bill-wave text-info"></i>
            </div>
            <p class="mt-1 text-xs+">Permintaan Rembes</p>
            </div>
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between space-x-1">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{count($pinjaman->where('status', 'pending'))}}
                </p>
                <i class="fa-solid fa-hand-holding-dollar text-secondary"></i>
            </div>
            <p class="mt-1 text-xs+">Permintaan Pinjaman</p>
            </div>
            <div class="rounded-lg bg-slate-150 p-4 dark:bg-navy-700">
            <div class="flex justify-between">
                <p
                class="text-xl font-semibold text-slate-700 dark:text-navy-100"
                >
                {{ count($karyawan) }}
                </p>
                <i class="fa-regular fa-user text-error"></i>
            </div>
            <p class="mt-1 text-xs+">Karyawan</p>
            </div>
        </div>
        </div>
        <div class="card col-span-12 lg:col-span-8">
            <div class="my-3 flex items-center justify-between px-4 sm:px-5">
                <h2
                class="font-medium tracking-wide text-slate-700 dark:text-navy-100"
                >
                Aktifitas Karyawan
                </h2>
            </div>
            <ol class="timeline line-space px-4 [--size:1.5rem] sm:px-5">
                @foreach($notifikasi as $notif)
                    <li class="timeline-item">
                        <div
                            class="timeline-item-point rounded-full border border-current bg-white text-{{ $notif->color }} dark:bg-navy-700 dark:text-{{ $notif->color }}-light"
                        >
                            <i class="{{ $notif->icon }} text-tiny"></i>
                        </div>
                        <div class="timeline-item-content flex-1 pl-4 sm:pl-8">
                            <div
                            class="flex flex-col justify-between pb-2 sm:flex-row sm:pb-0"
                            >
                            <p
                                class="pb-2 font-medium leading-none text-slate-600 dark:text-navy-100 sm:pb-0"
                            >
                                {{ $notif->judul }}
                            </p>
                            <span class="text-xs text-slate-400 dark:text-navy-300"
                                >{{ $notif->created_at->format('d M Y, H:i') }}</span
                            >
                            </div>
                            <p class="py-1">{{ $notif->keterangan }}</p>
                        </div>
                    </li>
                @endforeach
            </ol>
        </div>
        <div class="col-span-12 lg:col-span-4">
            <div
              :class="$store.breakpoints.smAndUp && 'via-purple-300'"
              class="card mt-12 bg-gradient-to-l from-pink-300 to-indigo-400 p-5 sm:mt-0 sm:flex-row"
            >
              <div class="flex justify-center sm:order-last">
                <img
                  class="-mt-16 h-40 sm:mt-0"
                  src="{{ asset('upload/asset/dashboard-check-dark.svg') }}"
                  alt=""
                />
              </div>
              <div
                class="mt-2 flex-1 pt-2 text-center text-white sm:mt-0 sm:text-left"
              >
                <h3 class="text-xl">
                  Selamat Datang, <span class="font-semibold">Admin</span>
                </h3>
                <p class="mt-2 leading-relaxed text-slate-200 text-xs">
                    <i>"<span class="font-semibold">Setiap hari</span> adalah kesempatan baru untuk menjadi lebih baik dalam <span class="font-semibold">pekerjaanmu</span>. Manfaatkan dengan penuh <span class="font-semibold">semangat!</span>"</i>
                </p>

              </div>
            </div>
            
            <div class="card px-4 pb-4 sm:px-5 my-5">
                <div class="my-3 flex h-8 items-center justify-between">
                    <h2
                    class="font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100"
                    >
                    Karyawan Cuti
                    </h2>
                </div>
                <div class="space-y-4">
                    @if(count($cuti) > 0)
                        @foreach($cuti as $key => $value)
                            <div
                            class="flex cursor-pointer items-center justify-between space-x-2"
                            >
                                <div class="flex items-center space-x-3">
                                    <a href="{{ !empty($value->karyawan->profile) ? asset($value->karyawan->profile) : generateUIAvatar($value->karyawan->nama_lengkap) }}" class="fancybox_file">
                                        <div class="avatar">
                                            <img
                                                class="rounded-full"
                                                src="{{ !empty($value->karyawan->profile) ? asset($value->karyawan->profile) : generateUIAvatar($value->karyawan->nama_lengkap) }}"
                                                alt="avatar"
                                            />
                                        </div>
                                    </a>
                                    <div>
                                    <p
                                        class="font-medium text-slate-700 dark:text-navy-100"
                                    >
                                        {{$value->karyawan->nama_lengkap}}
                                    </p>
                                    <p
                                        class="text-xs text-slate-400 line-clamp-1 dark:text-navy-300"
                                    >
                                        {{ date('d M Y', strtotime($value->tgl_mulai)) == date('d M Y', strtotime($value->tgl_selesai)) ? date('d M Y', strtotime($value->tgl_mulai)) : date('d M Y', strtotime($value->tgl_mulai)) .' - '. date('d M Y', strtotime($value->tgl_selesai)) }}
                                    </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p class="text-center">Tidak ada data karyawan cuti</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection


@section('script')
<script>
    $(document).ready(function () {
        $('#select_month').on('change', function (e) {
            window.location.href = $('#route-dashboard').val() + '?month=' + e.target.value
        });
    })
</script>
@endsection


