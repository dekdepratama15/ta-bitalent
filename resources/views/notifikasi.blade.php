@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.dashboard')
@endsection

@section('content')
<main class="main-content w-full pb-8">
    <div
        class="mt-4 grid grid-cols-12 gap-4 px-[var(--margin-x)] transition-all duration-[.25s] sm:mt-5 sm:gap-5 lg:mt-6 lg:gap-6"
    >
        <div class="card col-span-12 lg:col-span-12">
            <div class="my-3 flex items-center justify-between px-4 sm:px-5">
                <h2
                class="font-medium tracking-wide text-slate-700 dark:text-navy-100"
                >
                Aktifitas Karyawan
                </h2>
            </div>
            <ol class="timeline line-space px-4 [--size:1.5rem] sm:px-5">
                @foreach($notifikasi as $notif)
                    <li class="timeline-item">
                        <div
                            class="timeline-item-point rounded-full border border-current bg-white text-{{ $notif->color }} dark:bg-navy-700 dark:text-{{ $notif->color }}-light"
                        >
                            <i class="{{ $notif->icon }} text-tiny"></i>
                        </div>
                        <div class="timeline-item-content flex-1 pl-4 sm:pl-8">
                            <div
                            class="flex flex-col justify-between pb-2 sm:flex-row sm:pb-0"
                            >
                            <p
                                class="pb-2 font-medium leading-none text-slate-600 dark:text-navy-100 sm:pb-0"
                            >
                                {{ $notif->judul }}
                            </p>
                            <span class="text-xs text-slate-400 dark:text-navy-300"
                                >{{ $notif->created_at->format('d M Y, H:i') }}</span
                            >
                            </div>
                            <p class="py-1">{{ $notif->keterangan }}</p>
                        </div>
                    </li>
                @endforeach
            </ol>
        </div>
        </div>
    </div>
</main>
@endsection


