@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.users')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Karyawan
        </h2>
        <div class="hidden h-full py-1 sm:flex">
            <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
            <li class="flex items-center space-x-2">
                <a
                class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
                href="{{ url('/users/karyawan') }}"
                >Pengguna</a
                >
                <svg
                x-ignore
                xmlns="http://www.w3.org/2000/svg"
                class="h-4 w-4"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                >
                <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M9 5l7 7-7 7"
                />
                </svg>
            </li>
            <li>Karyawan</li>
        </ul>
    </div>
    <div class="grid grid-cols-1 gap-4 sm:gap-5 lg:gap-6">


        <!-- Table With Filter -->
        <div x-data="{isFilterExpanded:false}">
            <div class="flex items-center justify-between">
                <h2
                class="text-base font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100"
                >
                Daftar Karyawan
                </h2>
                <div class="flex">
                    <div class="flex items-center" x-data="{isInputActive:false}">
                        <form action="{{ route('karyawan.index') }}" method="get">
                            <label class="block">
                            <input
                                x-effect="isInputActive === true && $nextTick(() => { $el.focus()});"
                                :class="isInputActive ? 'w-32 lg:w-48' : 'w-0'"
                                class="form-input {{ !empty($search) ? 'w-32 lg:w-48' : 'w-0'}} bg-transparent px-1 text-right transition-all duration-100 placeholder:text-slate-500 dark:placeholder:text-navy-200"
                                placeholder="Search here..."
                                @if(!empty($search))  value="{{$search}}" @endif
                                type="text"
                                name="search"
                            />
                            </label>
                        </form>
                        <button
                        @click="isInputActive = !isInputActive"
                        class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-4.5 w-4.5"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="1.5"
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                            />
                        </svg>
                        </button>
                    </div>

                    <div
                    x-data="usePopper({placement:'bottom-end',offset:4})"
                    @click.outside="isShowPopper && (isShowPopper = false)"
                    class="inline-flex"
                    >
                    <button
                        x-ref="popperRef"
                        @click="isShowPopper = !isShowPopper"
                        class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                    >
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="h-4.5 w-4.5"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"
                        />
                        </svg>
                    </button>
                    <div
                        x-ref="popperRoot"
                        class="popper-root"
                        :class="isShowPopper && 'show'"
                    >
                        <div
                        class="popper-box rounded-md border border-slate-150 bg-white py-1.5 font-inter dark:border-navy-500 dark:bg-navy-700"
                        >
                            <ul>
                                <li>
                                <a
                                    href="{{ route('karyawan.create') }}"
                                    class="flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                    >Tambah Data</a
                                >
                                </li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
            <div x-show="isFilterExpanded" x-collapse>
                <div class="max-w-2xl py-3">
                <div
                    class="grid grid-cols-1 gap-4 sm:grid-cols-2 sm:gap-5 lg:gap-6"
                >
                    <label class="block">
                    <span>Employer name:</span>
                    <div class="relative mt-1.5 flex">
                        <input
                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                        placeholder="Enter Employer Name"
                        type="text"
                        />
                        <span
                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-4.5 w-4.5 transition-colors duration-200"
                            fill="none"
                            viewBox="0 0 24 24"
                        >
                            <path
                            stroke="currentColor"
                            stroke-width="1.5"
                            d="M5 19.111c0-2.413 1.697-4.468 4.004-4.848l.208-.035a17.134 17.134 0 015.576 0l.208.035c2.307.38 4.004 2.435 4.004 4.848C19 20.154 18.181 21 17.172 21H6.828C5.818 21 5 20.154 5 19.111zM16.083 6.938c0 2.174-1.828 3.937-4.083 3.937S7.917 9.112 7.917 6.937C7.917 4.764 9.745 3 12 3s4.083 1.763 4.083 3.938z"
                            />
                        </svg>
                        </span>
                    </div>
                    </label>
                    <label class="block">
                    <span>Project name:</span>
                    <div class="relative mt-1.5 flex">
                        <input
                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                        placeholder="Enter Project Name"
                        type="text"
                        />
                        <span
                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-4.5 w-4.5 transition-colors duration-200"
                            fill="none"
                            viewBox="0 0 24 24"
                        >
                            <path
                            stroke="currentColor"
                            stroke-width="1.5"
                            d="M3.082 13.944c-.529-.95-.793-1.425-.793-1.944 0-.519.264-.994.793-1.944L4.43 7.63l1.426-2.381c.559-.933.838-1.4 1.287-1.66.45-.259.993-.267 2.08-.285L12 3.26l2.775.044c1.088.018 1.631.026 2.08.286.45.26.73.726 1.288 1.659L19.57 7.63l1.35 2.426c.528.95.792 1.425.792 1.944 0 .519-.264.994-.793 1.944L19.57 16.37l-1.426 2.381c-.559.933-.838 1.4-1.287 1.66-.45.259-.993.267-2.08.285L12 20.74l-2.775-.044c-1.088-.018-1.631-.026-2.08-.286-.45-.26-.73-.726-1.288-1.659L4.43 16.37l-1.35-2.426z"
                            />
                            <circle
                            cx="12"
                            cy="12"
                            r="3"
                            stroke="currentColor"
                            stroke-width="1.5"
                            />
                        </svg>
                        </span>
                    </div>
                    </label>
                    <label class="block">
                    <span>From:</span>
                    <div class="relative mt-1.5 flex">
                        <input
                        x-init="$el._x_flatpickr = flatpickr($el)"
                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                        placeholder="Choose start date..."
                        type="text"
                        />
                        <span
                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-5 w-5 transition-colors duration-200"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            stroke-width="1.5"
                        >
                            <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                            />
                        </svg>
                        </span>
                    </div>
                    </label>
                    <label class="block">
                    <span>To:</span>
                    <div class="relative mt-1.5 flex">
                        <input
                        x-init="$el._x_flatpickr = flatpickr($el)"
                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                        placeholder="Choose start date..."
                        type="text"
                        />
                        <div
                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-5 w-5 transition-colors duration-200"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            stroke-width="1.5"
                        >
                            <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                            />
                        </svg>
                        </div>
                    </div>
                    </label>
                    <div class="sm:col-span-2">
                    <span>Project Status:</span>
                    <div
                        class="mt-2 grid grid-cols-1 gap-4 sm:grid-cols-4 sm:gap-5 lg:gap-6"
                    >
                        <label class="inline-flex items-center space-x-2">
                        <input
                            class="form-checkbox is-basic h-5 w-5 rounded border-slate-400/70 checked:border-secondary checked:bg-secondary hover:border-secondary focus:border-secondary dark:border-navy-400 dark:checked:border-secondary-light dark:checked:bg-secondary-light dark:hover:border-secondary-light dark:focus:border-secondary-light"
                            type="checkbox"
                        />
                        <span>Upcoming</span>
                        </label>
                        <label class="inline-flex items-center space-x-2">
                        <input
                            class="form-checkbox is-basic h-5 w-5 rounded border-slate-400/70 checked:border-primary checked:bg-primary hover:border-primary focus:border-primary dark:border-navy-400 dark:checked:border-accent dark:checked:bg-accent dark:hover:border-accent dark:focus:border-accent"
                            type="checkbox"
                        />
                        <span>In Progress</span>
                        </label>
                        <label class="inline-flex items-center space-x-2">
                        <input
                            checked
                            class="form-checkbox is-basic h-5 w-5 rounded border-slate-400/70 checked:!border-success checked:bg-success hover:!border-success focus:!border-success dark:border-navy-400"
                            type="checkbox"
                        />
                        <span>Complete</span>
                        </label>
                        <label class="inline-flex items-center space-x-2">
                        <input
                            checked
                            class="form-checkbox is-basic h-5 w-5 rounded border-slate-400/70 checked:!border-error checked:bg-error hover:!border-error focus:!border-error dark:border-navy-400"
                            type="checkbox"
                        />
                        <span>Cancelled</span>
                        </label>
                    </div>
                    </div>
                </div>
                <div class="mt-4 space-x-1 text-right">
                    <button
                    @click="isFilterExpanded = ! isFilterExpanded"
                    class="btn font-medium text-slate-700 hover:bg-slate-300/20 active:bg-slate-300/25 dark:text-navy-100 dark:hover:bg-navy-300/20 dark:active:bg-navy-300/25"
                    >
                    Cancel
                    </button>

                    <button
                    @click="isFilterExpanded = ! isFilterExpanded"
                    class="btn bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                    >
                    Apply
                    </button>
                </div>
                </div>
            </div>
            <div class="card mt-3">
                <div class="is-scrollbar-hidden min-w-full overflow-x-auto">
                <table class="is-hoverable w-full text-left">
                    <thead>
                        <tr>
                            <th
                                class="whitespace-nowrap rounded-tl-lg bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Profile
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Nama Karyawan
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Email
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Divisi
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Posisi
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Status
                            </th>
                            <th
                                class="whitespace-nowrap rounded-tr-lg bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($karyawans) > 0)
                            @foreach($karyawans as $karyawan)
                                <tr
                                    class="border-y border-transparent border-b-slate-200 dark:border-b-navy-500"
                                >
                                    <td class="whitespace-nowrap px-4 py-3 sm:px-5">
                                        <a href="{{ !empty($karyawan->profile) ? asset($karyawan->profile) : generateUIAvatar($karyawan->nama_lengkap) }}" class="fancybox_file">
                                            <div class="avatar flex">
                                                <img class="rounded-full" src="{{ !empty($karyawan->profile) ? asset($karyawan->profile) : generateUIAvatar($karyawan->nama_lengkap) }}" alt="avatar">
                                            </div>
                                        </a>
                                    </td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 font-medium text-slate-700 dark:text-navy-100 sm:px-5"
                                    >
                                        {{ $karyawan->nama_lengkap }}
                                    </td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 sm:px-5"
                                    >
                                        {{ $karyawan->user->email }}
                                    </td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 sm:px-5"
                                    >
                                        {{ $karyawan->posisi->divisi->nama_divisi }}
                                    </td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 sm:px-5"
                                    >
                                        {{ $karyawan->posisi_level->nama_posisi_level }} {{ $karyawan->posisi->nama_posisi }}
                                    </td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 sm:px-5"
                                    >
                                        <div
                                            class="badge space-x-2.5 rounded-full bg-{{staticHelper()['status'][$karyawan->status]['class']}}/10 text-{{staticHelper()['status'][$karyawan->status]['class']}} dark:bg-{{staticHelper()['status'][$karyawan->status]['class']}}/15"
                                        >
                                            <div class="h-2 w-2 rounded-full bg-current"></div>
                                            <span>{{staticHelper()['status'][$karyawan->status]['text']}}</span>
                                        </div>
                                    </td>
                                    <td class="whitespace-nowrap px-4 py-3 sm:px-5">
                                        <div
                                            x-data="usePopper({placement:'bottom-end',offset:4})"
                                            @click.outside="isShowPopper && (isShowPopper = false)"
                                            class="inline-flex"
                                        >
                                            <button
                                            x-ref="popperRef"
                                            @click="isShowPopper = !isShowPopper"
                                            class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                                            >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                class="h-5 w-5"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                stroke="currentColor"
                                                stroke-width="2"
                                            >
                                                <path
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"
                                                />
                                            </svg>
                                            </button>
                
                                            <div
                                            x-ref="popperRoot"
                                            class="popper-root"
                                            :class="isShowPopper && 'show'"
                                            >
                                                <div
                                                    class="popper-box rounded-md border border-slate-150 bg-white py-1.5 font-inter dark:border-navy-500 dark:bg-navy-700"
                                                >
                                                    <ul>
                                                        <li>
                                                            <a
                                                            href="{{ route('karyawan.edit', $karyawan->id) }}"
                                                            class="cursor-pointer flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                            >Ubah</a
                                                            >
                                                        </li>
                                                    </ul>
                                                    <div
                                                    class="my-1 h-px bg-slate-150 dark:bg-navy-500"
                                                    ></div>
                                                    <ul>
                                                        <li>
                                                            <a
                                                            href="{{ route('karyawan.show', $karyawan->id) }}"
                                                            class="cursor-pointer flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                            >Detail</a
                                                            >
                                                        </li>
                                                    </ul>
                                                    <div
                                                    class="my-1 h-px bg-slate-150 dark:bg-navy-500"
                                                    ></div>
                                                    <ul>
                                                        <li>
                                                            <div class="flex">
                                                                <div x-data="{showModal:false}">
                                                                    <a
                                                                    type="button"
                                                                    @click="showModal = true, isShowPopper = false"
                                                                    class="cursor-pointer flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                                    >Mutasi</a
                                                                    >
                                                                    <template x-teleport="#x-teleport-target">
                                                                        <div
                                                                            class="fixed inset-0 z-[100] flex flex-col items-center justify-center overflow-hidden px-4 py-6 sm:px-5"
                                                                            x-show="showModal"
                                                                            role="dialog"
                                                                            @keydown.window.escape="showModal = false"
                                                                        >
                                                                            <div
                                                                                class="absolute inset-0 bg-slate-900/60 backdrop-blur transition-opacity duration-300"
                                                                                @click="showModal = false"
                                                                                x-show="showModal"
                                                                                x-transition:enter="ease-out"
                                                                                x-transition:enter-start="opacity-0"
                                                                                x-transition:enter-end="opacity-100"
                                                                                x-transition:leave="ease-in"
                                                                                x-transition:leave-start="opacity-100"
                                                                                x-transition:leave-end="opacity-0"
                                                                            ></div>
                                                                            <div
                                                                                class="relative max-w-lg rounded-lg bg-white px-4 py-10 text-center transition-opacity duration-300 dark:bg-navy-700 sm:px-5"
                                                                                x-show="showModal"
                                                                                x-transition:enter="ease-out"
                                                                                x-transition:enter-start="opacity-0"
                                                                                x-transition:enter-end="opacity-100"
                                                                                x-transition:leave="ease-in"
                                                                                x-transition:leave-start="opacity-100"
                                                                                x-transition:leave-end="opacity-0"
                                                                            >
                                                                                <div class="mx-5">
                                                                                    <h2 class="text-2xl font-medium text-slate-700 dark:text-navy-100 mx-5">
                                                                                        Data Mutasi {{$karyawan->nama_lengkap}}
                                                                                    </h2>
                                                                                    <ol class="timeline line-space max-w-sm mt-5 pt-5">
                                                                                        @foreach($karyawan->mutasis as $key => $mutasi)
                                                                                            <li class="timeline-item">
                                                                                                <div
                                                                                                    class="timeline-item-point rounded-full bg-{{staticHelper()['type_mutasi'][$mutasi->type]['class']}} dark:bg-{{staticHelper()['type_mutasi'][$mutasi->type]['class']}}"
                                                                                                ></div>
                                                                                                <div class="timeline-item-content flex-1 pl-4 sm:pl-8">
                                                                                                    <a href="{{route('mutasi.show', $mutasi->id)}}">
                                                                                                        <div class="flex flex-col justify-between pb-2 sm:flex-row sm:pb-0">
                                                                                                        <p
                                                                                                            class="pb-2 font-medium leading-none text-slate-600 dark:text-navy-100 sm:pb-0"
                                                                                                        >
                                                                                                            {{staticHelper()['type_mutasi'][$mutasi->type]['text']}}
                                                                                                        </p>
                                                                                                        <span class="text-xs text-slate-400 dark:text-navy-300"
                                                                                                            >{{date('d M Y', strtotime($mutasi->created_at))}}</span
                                                                                                        >
                                                                                                        </div>
                                                                                                        <p class="py-1 text-left">
                                                                                                            {{staticHelper()['type_mutasi'][$mutasi->type]['text']}} dari {{$mutasi->posisi_awal}} ke {{$mutasi->posisi_sekarang}}
                                                                                                        </p>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </li>
                                                                                        @endforeach
                                                                                    </ol>
              
                                                                                    <button
                                                                                        @click="showModal = false"
                                                                                        class="btn mt-6 bg-error font-medium text-white hover:bg-error-focus focus:bg-error-focus active:bg-error-focus/90"
                                                                                    >
                                                                                        Tutup
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div
                                                    class="my-1 h-px bg-slate-150 dark:bg-navy-500"
                                                    ></div>
                                                    <ul>
                                                        <li>
                                                            <a
                                                            type="button"
                                                            data-status="{{$karyawan->status == 'aktif' ? 'nonaktif' : 'aktif'}}"
                                                            data-route="{{route('karyawan.change-status', ['karyawan' => $karyawan->id])}}"
                                                            class="cursor-pointer change-status-data flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                            >{{$karyawan->status == 'aktif' ? 'Nonaktif' : 'Aktif'}}</a
                                                            >
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr
                                class="border-y border-transparent border-b-slate-200 dark:border-b-navy-500"
                            >
                                <td class="whitespace-nowrap px-4 py-3 sm:px-5 text-center" colspan="4">
                                    Data tidak ditemukan
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                </div>

                <div
                class="flex flex-col justify-between space-y-4 px-4 py-4 sm:flex-row sm:items-center sm:space-y-0 sm:px-5"
                >
                <div class="text-xs+"></div>
                {{ $karyawans->links('vendor.pagination.tailwind') }}
                </div>
            </div>
        </div>

    </div>
</main>
@endsection
