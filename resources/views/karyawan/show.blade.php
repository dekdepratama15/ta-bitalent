@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.users')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Detail Karyawan
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
            href="{{ url('/users/karyawan') }}"
            >Pengguna</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
            href="{{ url('/users/karyawan') }}"
            >Karyawan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Detail Karyawan</li>
        </ul>
    </div>

    <form action="" method="post">
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            <div class="col-span-12 sm:col-span-12">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Pribadi
                    </p>
                    <div class="mt-4 space-y-4">
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                        <label class="block">
                            <span>Nama Lengkap</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Nama Lengkap"
                                type="text"
                                name="nama_lengkap"
                                value="{{$karyawan->nama_lengkap}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="far fa-user text-base"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Nama Panggilan</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Nama Panggilan"
                                type="text"
                                name="nama_panggilan"
                                value="{{$karyawan->nama_panggilan}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="far fa-user text-base"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Foto Profile</span><br>
                            <a
                                type="button"
                                target="_blank"
                                href="{{ asset($karyawan->profile) }}"
                                class="fancybox_file btn mt-1.5 btn-sm bg-secondary font-medium text-white hover:bg-secondary-focus focus:bg-secondary-focus active:bg-secondary-focus/90"
                            >
                                <i class="fa-regular fa-eye mr-2"></i> Lihat File
                            </a>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-12">
                        <label class="block sm:col-span-8">
                        <span>Email</span>
                        <div class="relative mt-1.5 flex">
                            <input
                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                            placeholder="Email"
                            type="email"
                            name="email"
                            value="{{$karyawan->user->email}}"
                            disabled
                            />
                            <span
                            class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                            >
                            <i class="fa-regular fa-envelope text-base"></i>
                            </span>
                        </div>
                        </label>
                        <label class="block sm:col-span-4">
                            <span>No Telp</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="08x-xxx-xxx-xxx"
                                type="text"
                                name="no_telp"
                                x-input-mask="{numericOnly: true, blocks: [0, 3, 3, 3, 4], delimiters: [' ', '-']}"
                                value="{{$karyawan->no_telp}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa fa-phone"></i>
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        <label class="block">
                            <span>Tempat Lahir</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Tempat Lahir"
                                type="text"
                                name="tempat_lahir"
                                value="{{$karyawan->tempat_lahir}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-solid fa-map-pin text-base"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Tanggal Lahir</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                x-init="$el._x_flatpickr = flatpickr($el)"
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Tanggal Lahir"
                                type="text"
                                name="tanggal_lahir"
                                value="{{$karyawan->tgl_lahir}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="h-5 w-5 transition-colors duration-200"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        stroke-width="1.5"
                                    >
                                        <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                        />
                                    </svg>
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        <label class="block">
                            <span>Password</span>
                            <span class="relative mt-1.5 flex">
                            <input
                                class="form-input peer w-full rounded-lg border border-slate-300 @error('password') border-error @enderror bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Masukan Password"
                                type="password"
                                name="password"
                                autocomplete="current-password"
                                disabled
                            />
                            <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                            >
                                <i class="fa-solid fa-key"></i>
                            </span>
                            </span>
                            @error('password')
                                <span class="text-tiny+ text-error">{{ $message }}</span>
                            @else
                                <span class="text-tiny+ text-slate-400">*isikan jika ingin mengubah password</span>
                            @enderror
                        </label>
                        <label class="block">
                            <span>Konfirmasi Password</span>
                            <span class="relative mt-1.5 flex">
                            <input
                                class="form-input peer w-full rounded-lg border border-slate-300 @error('password') border-error @enderror bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Masukan Konfirmasi Password"
                                type="password"
                                name="password_confirmation"
                                autocomplete="current-password"
                                disabled
                            />
                            <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                            >
                                <i class="fa-solid fa-key"></i>
                            </span>
                            </span>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-4">
                        <label class="block sm:col-span-3">
                            <span>Jenis Kelamin</span>
                            <select
                                class="mt-1.5 w-full bg-transparent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="jenis_kelamin"
                                disabled
                            >
                                <option selected disabled>Pilih Jenis Kelamin</option>
                                @foreach(staticHelper()['jenis_kelamin'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->jenis_kelamin ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Status Pernikahan</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="status_pernikahan"
                                disabled
                            >
                                <option selected disabled>Pilih Status Pernikahan</option>
                                @foreach(staticHelper()['status_pernikahan'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->status_perkawinan ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Golongan Darah</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="golongan_darah"
                                disabled
                            >
                                <option selected disabled>Pilih Golongan Darah</option>
                                @foreach(staticHelper()['golongan_darah'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->golongan_darah ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Agama</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="agama"
                                disabled
                            >
                                <option selected disabled>Pilih Agama</option>
                                @foreach(staticHelper()['agama'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->agama ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        <label class="block sm:col-span-3">
                            <span>Tipe Identitas</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="tipe_identitas"
                                disabled
                            >
                                <option selected disabled>Pilih Tipe Identitas</option>
                                @foreach(staticHelper()['tipe_identitas'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->jenis_identitas ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block">
                            <span>No Identitas</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="No Identitas"
                                    type="number"
                                    name="no_identitas"
                                    value="{{$karyawan->no_identitas}}"
                                    disabled
                                />
                                <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                    <i class="fa-regular fa-address-card"></i>
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-12">
                        <label class="block sm:col-span-8">
                            <span>Tanggal Identitas Kadaluarsa</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                x-init="$el._x_flatpickr = flatpickr($el)"
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Tanggal Identitas Kadaluarsa"
                                type="text"
                                name="tgl_identitas_kadaluarsa"
                                id="tgl_identitas_kadaluarsa"
                                @if(!$karyawan->is_identitas_permanent)
                                    value="{{$karyawan->tgl_identitas_kadaluarsa}}"
                                    disabled
                                @else
                                    disabled
                                @endif
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="h-5 w-5 transition-colors duration-200"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        stroke-width="1.5"
                                    >
                                        <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                        />
                                    </svg>
                                </span>
                            </span>

                            <label class="inline-flex items-center space-x-2 mt-2">
                                <input
                                class="form-checkbox is-basic h-5 w-5 rounded border-slate-400/70 checked:border-primary checked:bg-primary hover:border-primary focus:border-primary dark:border-navy-400 dark:checked:border-accent dark:checked:bg-accent dark:hover:border-accent dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                type="checkbox"
                                name="is_permanent"
                                id="is_permanent"
                                @if($karyawan->is_identitas_permanent)
                                    checked
                                @endif
                                disabled
                                />
                                <span>Permanent</span>
                            </label>
                        </label>
                        <label class="block sm:col-span-4">
                            <span>Kode Pos</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                type="number"
                                placeholder="Kode Pos"
                                name="kode_pos"
                                value="{{$karyawan->kode_pos}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-brands fa-usps"></i>
                                </span>
                            </span>
                        </label>
                    </div>
                    <label class="block">
                        <span>Alamat</span>
                        <textarea
                        rows="4"
                        placeholder="Alamat"
                        class="form-textarea mt-1.5 w-full rounded-lg border border-slate-300 bg-transparent p-2.5 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                        name="alamat"
                        disabled
                        >{{$karyawan->alamat}}</textarea>
                    </label>
                    </div>
                </div>
            </div>

            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Karyawan
                    </p>
                    <div class="mt-4 space-y-4">
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        <label class="block">
                            <span>ID Karyawan</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                    disabled
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                    value="{{$karyawan->kode_karyawan}}"
                                    type="text"
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-regular fa-id-badge"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Divisi</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                id="divisi"
                                name="divisi"
                                disabled
                            >
                                <option selected disabled>Pilih Divisi</option>
                                @foreach($divisis as $divisi)
                                    <option value="{{$divisi->id}}" {{$divisi->id == $karyawan->posisi->divisi_id ? 'selected' : ''}}>{{$divisi->nama_divisi}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                        
                        <label class="block sm:col-span-3">
                            <span>Posisi</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                id="posisi"
                                name="posisi"
                                disabled
                            >
                                <option selected disabled>Pilih divisi terlebih dahulu</option>
                            </select>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Level Posisi</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="posisi_level"
                                disabled
                            >
                                <option selected disabled>Pilih Level Posisi</option>
                                @foreach($posisi_levels as $posisi_level)
                                    <option value="{{$posisi_level->id}}" {{$posisi_level->id == $karyawan->posisi_level_id ? 'selected' : ''}}>{{$posisi_level->nama_posisi_level}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block sm:col-span-3">
                            <span>Penanggung Jawab</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="penanggung_jawab"
                                disabled
                            >
                                <option selected disabled>Pilih Penanggung Jawab</option>
                                <option value="0" {{0 == $karyawan->penanggung_jawab_id || empty($karyawan->penanggung_jawab_id) ? 'selected' : ''}}>Tidak Ada Penanggung Jawab</option>
                                @foreach($karyawan_all as $k)
                                    <option value="{{$k->user->id}}" {{$k->user->id == $karyawan->penanggung_jawab_id ? 'selected' : ''}}>{{$k->nama_lengkap}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                        <label class="block">
                            <span>Tanggal Masuk</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                x-init="$el._x_flatpickr = flatpickr($el)"
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Tanggal Lahir"
                                type="text"
                                name="tgl_masuk"
                                value="{{$karyawan->tgl_bergabung}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="h-5 w-5 transition-colors duration-200"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        stroke-width="1.5"
                                    >
                                        <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                        />
                                    </svg>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Tanggal Keluar</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                x-init="$el._x_flatpickr = flatpickr($el)"
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Tanggal Lahir"
                                type="text"
                                name="tgl_keluar"
                                value="{{$karyawan->tgl_selesai}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        class="h-5 w-5 transition-colors duration-200"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        stroke-width="1.5"
                                    >
                                        <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                        />
                                    </svg>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Status Karyawan</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="status_karyawan"
                                disabled
                            >
                                <option selected disabled>Pilih Status Karyawan</option>
                                @foreach(staticHelper()['status_karyawan'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->status_karyawan ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    </div>
                </div>
            </div>

            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Keuangan Dan Waktu
                    </p>
                    <div class="mt-4 space-y-4">
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                        <label class="block">
                            <span>Jumlah Cuti</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                type="number"
                                placeholder="Jumlah Cuti"
                                name="jumlah_cuti"
                                x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                value="{{$karyawan->jumlah_cuti}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-solid fa-umbrella-beach"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Gaji Pokok</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Gaji Pokok"
                                x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                type="text"
                                name="gaji"
                                value="{{$karyawan->gaji_pokok}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-solid fa-rupiah-sign"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>Tipe Penggajian</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="tipe_penggajian"
                                disabled
                            >
                                <option selected disabled>Pilih Tipe Penggajian</option>
                                @foreach(staticHelper()['tipe_penggajian'] as $key => $value)
                                    <option value="{{$key}}" {{$key == $karyawan->tipe_penggajian ? 'selected' : ''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                        <label class="block">
                            <span>Bank</span>
                            <select
                                class="mt-1.5 w-full disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                x-init="$el._x_tom = new Tom($el,{create: false})"
                                name="bank"
                                disabled
                            >
                                <option selected disabled>Pilih Bank</option>
                                @foreach($banks as $bank)
                                    <option value="{{$bank->id}}" {{$bank->id == $karyawan->bank_id ? 'selected' : ''}}>{{$bank->nama_bank}}</option>
                                @endforeach
                            </select>
                        </label>
                        <label class="block">
                            <span>Nama Pemilik Rekening</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                placeholder="Nama Pemilik Rekening"
                                type="text"
                                name="pemilik_rekening"
                                value="{{$karyawan->nama_akun_bank}}"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-solid fa-money-check"></i>
                                </span>
                            </span>
                        </label>
                        <label class="block">
                            <span>No Rekening</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                type="number"
                                placeholder="No Rekening"
                                value="{{$karyawan->no_akun_bank}}"
                                name="no_rekening"
                                disabled
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                >
                                <i class="fa-solid fa-money-check"></i>
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                        <div class="flex items-center justify-between col-span-12">
                            <span
                            >
                            Tunjangan Karyawan
                            </span>
                        </div>
                        @php
                            $tunjangan = '';
                            foreach ($karyawan->tunjangan_karyawans as $tunjangan_karyawan) {
                                if ($tunjangan != '') {
                                    $tunjangan .= ',';
                                }
                                $tunjangan .= $tunjangan_karyawan->tunjangan_id;
                            }
                        @endphp
                        <input type="hidden" name="tunjangan" id="tunjangan" value="{{$tunjangan}}">
                        <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                            <table class="is-zebra w-full text-left">
                            <thead>
                                <tr>
                                    <th
                                        class="whitespace-nowrap rounded-l-lg bg-slate-200 px-3 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        #
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Nama Tunjangan
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Tipe Tunjangan
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Jenis Tunjangan
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Diambil dari
                                    </th>
                                    <th
                                        class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                    >
                                        Tanggal Pemberian
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="body-table-tunjangan">
                                <tr>
                                    <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="7">Tidak ada Tunjangan</td>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>


            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">

                    <div class="flex justify-end space-x-2">
                        <a 
                        type="button"
                        href="{{route('karyawan.index')}}"
                        class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                        >
                        <span>Kembali</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
</main>


@endsection

@section('script')
<script>
    $(document).ready(function () {
        var select_posisi = new Tom('#posisi', {   
                plugins: ['dropdown_input'],
        });
        getPosisi('{{$karyawan->posisi->divisi_id}}', select_posisi, '{{$karyawan->posisi_id}}');
        getTunjanganTable()

        $('#is_permanent').on('change', function () {
            if (this.checked) {
                $('#tgl_identitas_kadaluarsa').removeAttr('required')
                $('#tgl_identitas_kadaluarsa').attr('disabled', true)
            } else {
                $('#tgl_identitas_kadaluarsa').removeAttr('disabled')
                $('#tgl_identitas_kadaluarsa').attr('required', true)
            }
        });
    });

    function getTunjanganTable() {
        var tunjangan_id = $('#tunjangan').val();

        var row = '';
        if (tunjangan_id != '') {
            $.each(tunjangan_id.split(','), function (key, id) {
                var tunjangan = $.grep(JSON.parse('{!! $tunjangans !!}'), function( n, i ) {
                    return n.id==id;
                });

                row += `
                    <tr>
                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">${key + 1}</td>
                        <td class="whitespace-nowrap px-4 py-3 sm:px-5">${tunjangan[0].nama_tunjangan}</td>
                        <td class="whitespace-nowrap px-4 py-3 sm:px-5">${ucWord(tunjangan[0].type)}</td>
                        <td class="whitespace-nowrap rounded-r-lg px-4 py-3 sm:px-5">${ucWord(replaceUnderscore(tunjangan[0].jenis_tunjangan))}</td>
                        <td class="whitespace-nowrap rounded-r-lg px-4 py-3 sm:px-5">${ucWord(tunjangan[0].diambil_dari)}</td>
                        <td class="whitespace-nowrap rounded-r-lg px-4 py-3 sm:px-5">${ucWord(tunjangan[0].tanggal_pemberian)}</td>
                    </tr>
                `;
            });
        } else {
            row += `
                <tr>
                    <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="7">Tidak ada Tunjangan</td>
                </tr>
            `;
        }
        $('#body-table-tunjangan').html(row)
    }

    function deleteTunjangan(delete_id) {
        var tunjangan_id = $('#tunjangan').val();
        var tunjangan_id = $.grep(tunjangan_id.split(','), function( id ) {
            return id!=delete_id;
        });
        $('#tunjangan').val(tunjangan_id.join());
        getTunjanganTable();
    }

    function getTunjanganOption(select_tambah_tunjangan) {
        var tunjangans = JSON.parse('{!! $tunjangans !!}');

        var option = '<option selected disabled>Pilih Tunjangan</option>';
        $.each(tunjangans, function (key, val) {
            option += `
                <option value="${val.id}">${val.nama_tunjangan}</option>
            `;
        });

        
        select_tambah_tunjangan.clear(); 
        select_tambah_tunjangan.clearOptions();
        $('#tambah_tunjangan').html(option); 
        select_tambah_tunjangan.sync();
    }

    function getPosisi(id, select_posisi, posisi_id = null) {
        var posisis = $.grep(JSON.parse('{!! $posisis !!}'), function( n, i ) {
            return n.divisi_id==id;
        });

        var option = '<option selected disabled>Pilih Posisi</option>';
        $.each(posisis, function (key, val) {
            option += `
                <option value="${val.id}" ${val.id == posisi_id ? 'selected' : ''}>${val.nama_posisi}</option>
            `;
        });

        
        select_posisi.clear(); 
        select_posisi.clearOptions();
        $('#posisi').html(option); 
        select_posisi.sync();
    }
</script>
@endsection
