@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.finance')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Tambah Tunjangan
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('tunjangan.index') }}"
            >Keuangan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('tunjangan.index') }}"
            >Tunjangan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Tambah Tunjangan</li>
        </ul>
    </div>

    <form action="{{ route('tunjangan.store') }}" method="post">
        @csrf
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            <div class="col-span-12 sm:col-span-12">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Tunjangan
                    </p>
                    <div class="mt-4 space-y-4">
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                            <label class="block">
                                <span>Nama Tunjangan</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Nama Tunjangan"
                                    type="text"
                                    name="nama_tunjangan"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-solid fa-plus-minus"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Tipe Tunjangan</span>
                                <select
                                    class="mt-1.5 w-full"
                                    x-init="$el._x_tom = new Tom($el,{create: false})"
                                    id="tipe-tunjangan"
                                    name="tipe_tunjangan"
                                    required
                                >
                                    <option selected disabled>Pilih Tipe Tunjangan</option>
                                    @foreach(staticHelper()['tipe_tunjangan'] as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-4">
                            <label class="block">
                                <span>Jenis Tunjangan</span>
                                <select
                                    class="mt-1.5 w-full"
                                    x-init="$el._x_tom = new Tom($el,{create: false})"
                                    name="jenis"
                                    required
                                >
                                    <option selected disabled>Pilih Jenis Tunjangan</option>
                                    @foreach(staticHelper()['jenis_tunjangan'] as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </label>
                            <label class="block">
                                <span>Diambil Dari</span>
                                <select
                                    class="mt-1.5 w-full"
                                    x-init="$el._x_tom = new Tom($el,{create: false})"
                                    id="diambil-dari"
                                    name="diambil_dari"
                                    required
                                >
                                    <option selected disabled>Pilih Diambil Dari</option>
                                    @foreach(staticHelper()['diambil_dari'] as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </label>
                            <label class="block diambil-dari" id="gaji">
                                <span>Persentase</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input input-diambil-dari peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Persentase"
                                    type="number"
                                    name="persentase"
                                    id="input-gaji"
                                    x-input-mask="{numericOnly: true}"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-solid fa-percent"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block diambil-dari hidden" id="pendapatan">
                                <span>Jumlah</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input input-diambil-dari peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Persentase"
                                    type="text"
                                    name="jumlah"
                                    id="input-pendapatan"
                                    x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    <i class="fa-solid fa-rupiah-sign"></i>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Tanggal Pemberian</span>
                                <span class="relative mt-1.5 flex" id="sekali">
                                    <input
                                    x-init="$el._x_flatpickr = flatpickr($el)"
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Tanggal Pemberian"
                                    name="tanggal_pemberian_sekali"
                                    type="text"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="h-5 w-5 transition-colors duration-200"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            stroke-width="1.5"
                                        >
                                            <path
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                            />
                                        </svg>
                                    </span>
                                </span>
                                <select
                                    class="mt-1.5 w-full hidden berkala"
                                    x-init="$el._x_tom = new Tom($el,{create: false})"
                                    id="berkala"
                                    name="tanggal_pemberian_berkala"
                                >
                                    <option selected disabled>Pilih Tanggal Pemberian</option>
                                    @for($i = 1;$i <= 31; $i++)
                                        <option value="{{$i}}">Tanggal {{$i}}</option>
                                    @endfor
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
    
    
            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
    
                    <div class="flex justify-end space-x-2">
                        <a 
                        type="button"
                        href="{{route('tunjangan.index')}}"
                        class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                        >
                        <span>Kembali</span>
                        </a>
                        <button
                        class="btn space-x-2 bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                        >
                        <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
    </main>
@endsection

@section('script')
<script>
    $('#tipe-tunjangan').on('change', function (e) {
        if (e.target.value == 'sekali') {
            $('#berkala').hide()
            $('.berkala').hide();
            $('#berkala').addClass('hidden')
            $('.berkala').addClass('hidden')
            $('#berkala').removeAttr('required');

            $('#sekali').removeClass('hidden');
            $('#sekali').show();
            $('#sekali').attr('required', true);
        } else {
            $('#sekali').hide()
            $('#sekali').addClass('hidden')
            $('#sekali').removeAttr('required');

            $('#berkala').removeClass('hidden');
            $('.berkala').removeClass('hidden');
            $('#berkala').show();
            $('.berkala').show();
            $('#berkala').attr('required', true);
        }
    });
    $('#diambil-dari').on('change', function (e) {
        $('.diambil-dari').hide()
        $('.input-diambil-dari').removeAttr('required')
        $('#'+e.target.value).show()
        $('#input-'+e.target.value).attr('required', true);
    });
</script>
@endsection
