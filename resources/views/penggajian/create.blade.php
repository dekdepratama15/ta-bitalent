@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.finance')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Tambah Penggajian
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('penggajian.index') }}"
            >Keuangan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('penggajian.index') }}"
            >Penggajian</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Tambah Penggajian</li>
        </ul>
    </div>

    <form action="{{route('penggajian.store')}}" method="post" enctype="multipart/form-data">
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            @csrf

            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Pilih Karyawan
                    </p>
                    <div class="mt-4 space-y-4">
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                            <label class="block">
                                <span>Karyawan</span>
                                <input type="hidden" id="url_create" value="{{route('penggajian.create')}}">
                                <select
                                    class="mt-1.5 w-full"
                                    x-init="$el._x_tom = new Tom($el,{create: false})"
                                    name="karyawan"
                                    id="select_karyawan"
                                    required
                                >
                                    <option selected disabled>Pilih Karyawan</option>
                                    @foreach($karyawans as $key => $value)
                                        <option value="{{$value->id}}" {{ !empty($karyawan) ? ($karyawan->id == $value->id ? 'selected' : ''):'' }}>{{$value->nama_lengkap}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div> 
            @if($karyawan != null)
                @php
                    $lembur_total = 0;
                    $reimbursement_total = 0;
                    $tunjangan_karyawan_total = 0;
                @endphp
                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <p
                        class="text-base font-medium text-slate-700 dark:text-navy-100"
                        >
                        Data Karyawan
                        </p>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-4">
                                <label class="block">
                                    <span>ID Karyawan</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ $karyawan->kode_karyawan }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        <i class="fa-regular fa-id-badge"></i>
                                        </span>
                                    </span>
                                </label>
                                <label class="block">
                                    <span>Nama Panggilan</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ $karyawan->nama_panggilan }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        <i class="fa-regular fa-user"></i>
                                        </span>
                                    </span>
                                </label>
                                <label class="block sm:col-span-3">
                                    <span>Divisi</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ $karyawan->posisi->divisi->nama_divisi }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        </span>
                                    </span>
                                </label>
                                <label class="block sm:col-span-3">
                                    <span>Posisi</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ $karyawan->posisi->nama_posisi }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                                
                                <label class="block sm:col-span-3">
                                    <span>Level Posisi</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ $karyawan->posisi_level->nama_posisi_level }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        </span>
                                    </span>
                                </label>
                                <label class="block sm:col-span-3">
                                    <span>Penanggung Jawab</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                            disabled
                                            class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary disabled:pointer-events-none disabled:select-none disabled:border-gray disabled:bg-zinc-100 dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent dark:disabled:bg-navy-600"
                                            value="{{ !empty($karyawan->penanggung_jawab) ? $karyawan->penanggung_jawab->karyawan->nama_lengkap : '-' }}"
                                            type="text"
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                        >
                                        </span>
                                    </span>
                                </label>
                                <label class="block sm:col-span-3">
                                    <span>Gaji Pokok</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        placeholder="Gaji Pokok"
                                        x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                        type="text"
                                        name="gaji"
                                        value="{{$karyawan->gaji_pokok}}"
                                        disabled
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        >
                                        <i class="fa-solid fa-rupiah-sign"></i>
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <p
                        class="text-base font-medium text-slate-700 dark:text-navy-100"
                        >
                        Lembur
                        </p>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                                <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                                    <table class="is-zebra w-full text-left">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="whitespace-nowrap rounded-l-lg bg-slate-200 px-3 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    #
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Tanggal
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Biaya per Jam
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Total
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                Aksi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($karyawan->lemburs) > 0)
                                                @foreach($karyawan->lemburs as $key => $lembur)
                                                    @php
                                                        $lembur_total += $lembur->total;
                                                    @endphp
                                                    <tr>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{$loop->iteration}} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{date('Y-m-d', strtotime($lembur->jam_mulai)) == date('Y-m-d', strtotime($lembur->jam_selesai)) ? date('d M Y, H:i', strtotime($lembur->jam_mulai)).' - '.date('H:i', strtotime($lembur->jam_selesai)) : date('d M Y, H:i', strtotime($lembur->jam_mulai)).' - '.date('d M Y, H:i', strtotime($lembur->jam_selesai)) }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        Rp. {{ number_format($lembur->bayar, 0, '.', ',') }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        Rp. {{ number_format($lembur->total, 0, '.', ',') }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                            <label class="inline-flex items-center space-x-2">
                                                                <input
                                                                    class="form-switch h-5 w-10 rounded-full bg-slate-300 before:rounded-full before:bg-slate-50 checked:!bg-success checked:before:bg-white dark:bg-navy-900 dark:before:bg-navy-300 dark:checked:before:bg-white checked-switch-input"
                                                                    type="checkbox"
                                                                    name="lembur_id[]"
                                                                    value="{{ $lembur->id }}"
                                                                    data-nominal="{{ $lembur->total }}"
                                                                    data-fitur="lembur_total"
                                                                    checked
                                                                />
                                                            </label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-right" colspan="4">TOTAL LEMBUR</th>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" id="row_lembur_total">Rp. {{ number_format($lembur_total, 0, '.', ',') }}</th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="5">Tidak ada data Lembur</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <p
                        class="text-base font-medium text-slate-700 dark:text-navy-100"
                        >
                        Rembes
                        </p>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                                <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                                    <table class="is-zebra w-full text-left">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="whitespace-nowrap rounded-l-lg bg-slate-200 px-3 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    #
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Keterangan
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Tanggal
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Total
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    File
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                Aksi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($karyawan->reimbursements) > 0)
                                                @foreach($karyawan->reimbursements as $key => $reimbursement)
                                                    @php
                                                        $reimbursement_total += $reimbursement->total;
                                                    @endphp
                                                    <tr>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{$loop->iteration}} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{ $reimbursement->keterangan }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{date('Y-m-d', strtotime($reimbursement->created_at))}} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        Rp. {{ number_format($reimbursement->total, 0, '.', ',') }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                            <a
                                                                type="button"
                                                                target="_blank"
                                                                href="{{ asset($reimbursement->file) }}"
                                                                class="fancybox_file btn mt-1.5 btn-sm bg-secondary font-medium text-white hover:bg-secondary-focus focus:bg-secondary-focus active:bg-secondary-focus/90"
                                                            >
                                                                <i class="fa-regular fa-eye mr-2"></i> Lihat File
                                                            </a>
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                            <label class="inline-flex items-center space-x-2">
                                                                <input
                                                                    class="form-switch h-5 w-10 rounded-full bg-slate-300 before:rounded-full before:bg-slate-50 checked:!bg-success checked:before:bg-white dark:bg-navy-900 dark:before:bg-navy-300 dark:checked:before:bg-white checked-switch-input"
                                                                    type="checkbox"
                                                                    name="reimbursement_id[]"
                                                                    value="{{ $reimbursement->id }}"
                                                                    data-nominal="{{ $reimbursement->total }}"
                                                                    data-fitur="reimbursement_total"
                                                                    checked
                                                                />
                                                            </label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-right" colspan="5">TOTAL Rembes</th>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" id="row_reimbursement_total">Rp. {{ number_format($reimbursement_total, 0, '.', ',') }}</th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="5">Tidak ada data Rembes</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <p
                        class="text-base font-medium text-slate-700 dark:text-navy-100"
                        >
                        Tunjangan Karyawan
                        </p>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                                <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                                    <table class="is-zebra w-full text-left">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="whitespace-nowrap rounded-l-lg bg-slate-200 px-3 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    #
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Nama Tunjangan
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Jenis & Tipe
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Diambil dari
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                    Total
                                                </th>
                                                <th
                                                    class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                                                >
                                                Aksi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($karyawan->tunjangan_karyawans) > 0)
                                                @foreach($karyawan->tunjangan_karyawans as $key => $tunjangan_karyawan)
                                                    <tr>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{$loop->iteration}} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{ $tunjangan_karyawan->tunjangan->nama_tunjangan }} 
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{ ucfirst(str_replace('_', ' ', $tunjangan_karyawan->tunjangan->jenis_tunjangan)) }} {{ucfirst($tunjangan_karyawan->tunjangan->type)}}
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                        {{ ucfirst($tunjangan_karyawan->tunjangan->diambil_dari) }} {{$tunjangan_karyawan->tunjangan->diambil_dari == 'gaji' ? '('.$tunjangan_karyawan->tunjangan->persentase.'%)' : '' }}
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                            @php 
                                                                if ($tunjangan_karyawan->tunjangan->diambil_dari == 'pendapatan') {
                                                                    $jumlah_tunjangan = $tunjangan_karyawan->tunjangan->jumlah;
                                                                } else {
                                                                    $jumlah_tunjangan = ($karyawan->gaji_pokok * $tunjangan_karyawan->tunjangan->persentase) / 100;
                                                                }
                                                                if ($tunjangan_karyawan->tunjangan->jenis_tunjangan == 'potongan_pajak') {
                                                                    $jumlah_tunjangan = $jumlah_tunjangan * -1;
                                                                }
                                                                $tunjangan_karyawan_total += $jumlah_tunjangan;
                                                            @endphp
                                                            Rp. {{ number_format($jumlah_tunjangan, 0, '.', ',') }}
                                                        </td>
                                                        <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5">
                                                            <label class="inline-flex items-center space-x-2">
                                                                <input
                                                                    class="form-switch h-5 w-10 rounded-full bg-slate-300 before:rounded-full before:bg-slate-50 checked:!bg-success checked:before:bg-white dark:bg-navy-900 dark:before:bg-navy-300 dark:checked:before:bg-white checked-switch-input"
                                                                    type="checkbox"
                                                                    name="tunjangan_karyawan_id[]"
                                                                    value="{{ $tunjangan_karyawan->id }}"
                                                                    data-nominal="{{ $jumlah_tunjangan }}"
                                                                    data-fitur="tunjangan_karyawan_total"
                                                                    checked
                                                                />
                                                            </label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-right" colspan="5">TOTAL TUNJANGAN KARYAWAN</th>
                                                    <th class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" id="row_tunjangan_karyawan_total">Rp. {{ number_format($tunjangan_karyawan_total, 0, '.', ',') }}</th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="whitespace-nowrap rounded-l-lg px-4 py-3 sm:px-5 text-center" colspan="5">Tidak ada data Tunjangan Karyawan</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <div class="flex h-8 items-center justify-between">
                            <h2 class="font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100 lg:text-base">
                                Bonus
                            </h2>
                            <label class="inline-flex items-center space-x-2">
                                <span class="text-xs text-slate-400 dark:text-navy-300">Tidak Ada</span>
                                <input class="form-switch h-5 w-10 rounded-full bg-slate-300 before:rounded-full before:bg-slate-50 checked:bg-primary checked:before:bg-white dark:bg-navy-900 dark:before:bg-navy-300 dark:checked:bg-accent dark:checked:before:bg-white" type="checkbox" id="adaBonus" checked>
                            </label>
                        </div>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-1 sm:grid-cols-1">
                                <label class="block">
                                    <span>Bonus</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        placeholder="Bonus"
                                        x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                        type="text"
                                        id="bonus"
                                        name="bonus"
                                        value="0"
                                        disabled
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        >
                                        <i class="fa-solid fa-rupiah-sign"></i>
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <div class="flex h-8 items-center justify-between">
                            <h2 class="font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100 lg:text-base">
                                Pemotongan Gaji
                            </h2>
                            <label class="inline-flex items-center space-x-2">
                                <span class="text-xs text-slate-400 dark:text-navy-300">Tidak Ada</span>
                                <input class="form-switch h-5 w-10 rounded-full bg-slate-300 before:rounded-full before:bg-slate-50 checked:bg-primary checked:before:bg-white dark:bg-navy-900 dark:before:bg-navy-300 dark:checked:bg-accent dark:checked:before:bg-white" type="checkbox" id="adaPemotonganPajak" checked>
                            </label>
                        </div>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-1 sm:grid-cols-1">
                                <label class="block">
                                    <span>Pemotongan Gaji</span>
                                    <span class="relative mt-1.5 flex">
                                        <input
                                        class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        placeholder="Pemotongan Gaji"
                                        x-input-mask="{numeral: true,numeralThousandsGroupStyle: 'thousand'}"
                                        type="text"
                                        id="pemotongan_gaji"
                                        name="pemotongan_gaji"
                                        value="0"
                                        disabled
                                        />
                                        <span
                                        class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                        >
                                        <i class="fa-solid fa-rupiah-sign"></i>
                                        </span>
                                    </span>
                                </label>
                                <label class="block">
                                    <span>Alasan Pemotongan</span>
                                    <textarea
                                    rows="4"
                                    placeholder="Alasan Pemotongan"
                                    class="form-textarea mt-1.5 w-full rounded-lg border border-slate-300 bg-transparent p-2.5 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    name="alasan"
                                    id="alasan"
                                    disabled
                                    ></textarea>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">
                        <p
                        class="text-base font-medium text-slate-700 dark:text-navy-100"
                        >
                        Grand Total
                        </p>
                        <div class="mt-4 space-y-4">
                            <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                                <div class="is-scrollbar-hidden min-w-full overflow-x-auto col-span-12">
                                    <table class="w-full text-left">
                                        <tbody>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                TOTAL LEMBUR
                                                </th>
                                                <td class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_lembur_total">
                                                Rp. {{ number_format($lembur_total, 0, '.', ',') }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                TOTAL REMBES
                                                </th>
                                                <td class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_reimbursement_total">
                                                Rp. {{ number_format($reimbursement_total, 0, '.', ',') }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                TOTAL TUNJANGAN
                                                </th>
                                                <td class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_tunjangan_karyawan_total">
                                                Rp. {{ number_format($tunjangan_karyawan_total, 0, '.', ',') }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                BONUS
                                                </th>
                                                <td class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_bonus_total">
                                                Rp. {{ number_format(0, 0, '.', ',') }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                PEMOTONGAN GAJI
                                                </th>
                                                <td class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_pemotongan_gaji_total">
                                                Rp. {{ number_format(0, 0, '.', ',') }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right">
                                                GRAND TOTAL
                                                </th>
                                                <th class="whitespace-nowrap border border-slate-200 px-3 py-3 dark:border-navy-500 lg:px-5 text-right" id="grand_grand_total">
                                                @php
                                                    $grand_total = $karyawan->gaji_pokok + $lembur_total + $reimbursement_total + $tunjangan_karyawan_total
                                                @endphp
                                                Rp. {{ number_format($grand_total, 0, '.', ',') }} 
                                                </th>
                                            </tr>
                                            <input type="hidden" name="lembur_total" id="lembur_total" value="{{ $lembur_total }}">
                                            <input type="hidden" name="reimbursement_total" id="reimbursement_total" value="{{ $reimbursement_total }}">
                                            <input type="hidden" name="tunjangan_karyawan_total" id="tunjangan_karyawan_total" value="{{ $tunjangan_karyawan_total }}">
                                            <input type="hidden" name="grand_total" id="grand_total" value="{{ $grand_total }}">
                                            <input type="hidden" name="gaji_pokok" id="gaji_pokok" value="{{ $karyawan->gaji_pokok }}">
                                            <input type="hidden" name="pemotongan_gaji_total" id="pemotongan_gaji_total" value="0">
                                            <input type="hidden" name="bonus_total" id="bonus_total" value="0">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                


                <div class="col-span-12 sm:col-span-12 mt-5">
                    <div class="card p-4 sm:p-5">

                        <div class="flex justify-end space-x-2">
                            <a 
                            type="button"
                            href="{{route('penggajian.index')}}"
                            class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                            >
                            <span>Kembali</span>
                            </a>
                            <button
                            type="submit"
                            class="btn space-x-2 bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                            >
                            <span>Simpan</span>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </form>

    </div>
</main>


@endsection

@section('script')
<script>
    $('#select_karyawan').on('change', function(e) {
        window.location.href = $('#url_create').val() + '?karyawan=' + e.target.value;
    });

    $('#adaPemotonganPajak').on('change', function(e) {
        if ($(this).is(':checked')) {
            $('#pemotongan_gaji').val(0);
            $('#pemotongan_gaji_total').val(0);
            $('#alasan').val('');

            $('#pemotongan_gaji').attr('disabled', true);
            $('#alasan').attr('disabled', true);

            $('#pemotongan_gaji').removeAttr('required');
            $('#alasan').removeAttr('required');
            getGrandTotal();
        } else {
            $('#pemotongan_gaji').attr('required', true);
            $('#alasan').attr('required', true);

            $('#pemotongan_gaji').removeAttr('disabled');
            $('#alasan').removeAttr('disabled');
        }
    });

    $('#pemotongan_gaji').on('keyup', function(e) {
        $('#pemotongan_gaji_total').val($(this).val().replace(/,/g, ""))
        $('#grand_pemotongan_gaji_total').html('Rp. ' + numberWithCommas($(this).val().replace(/,/g, "")))
        getGrandTotal();
    });

    $('#adaBonus').on('change', function(e) {
        if ($(this).is(':checked')) {
            $('#bonus').val(0);
            $('#bonus_total').val(0);
            $('#bonus').attr('disabled', true);
            $('#bonus').removeAttr('required');
            getGrandTotal();
        } else {
            $('#bonus').attr('required', true);
            $('#bonus').removeAttr('disabled');
        }
    });

    $('#bonus').on('keyup', function(e) {
        $('#bonus_total').val($(this).val().replace(/,/g, ""))
        $('#grand_bonus_total').html('Rp. ' + numberWithCommas($(this).val().replace(/,/g, "")))
        getGrandTotal();
    });

    $('.checked-switch-input').on('change', function () {
        var fitur = $(this).data('fitur');
        var nominal = parseFloat($(this).data('nominal'));
        var fitur_total = parseFloat($('#' + fitur).val());
        console.log(fitur, nominal, fitur_total);
        var total;
        if ($(this).is(':checked')) {
            total = fitur_total + nominal;
        } else {
            total = fitur_total - nominal;
        }
        console.log(total);
        $('#row_'+fitur).html('Rp. ' + numberWithCommas(total))
        $('#grand_'+fitur).html('Rp. ' + numberWithCommas(total))
        $('#'+fitur).val(parseFloat(total))
        getGrandTotal();
    });

    function getGrandTotal() {
        var gaji_pokok = parseFloat($('#gaji_pokok').val());
        var lembur_total = parseFloat($('#lembur_total').val());
        var reimbursement_total = parseFloat($('#reimbursement_total').val());
        var tunjangan_karyawan_total = parseFloat($('#tunjangan_karyawan_total').val());
        var pemotongan_gaji_total = parseFloat($('#pemotongan_gaji_total').val());
        var bonus_total = parseFloat($('#bonus_total').val());
        var grand_total = gaji_pokok + lembur_total + reimbursement_total + tunjangan_karyawan_total + bonus_total - pemotongan_gaji_total;
        $('#grand_total').val(grand_total);
        $('#grand_grand_total').html('Rp. ' + numberWithCommas(grand_total));
    }
</script>
@endsection
