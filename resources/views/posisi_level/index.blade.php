@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.users')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Posisi Level
        </h2>
        <div class="hidden h-full py-1 sm:flex">
            <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
            <li class="flex items-center space-x-2">
                <a
                class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
                href="{{ route('posisi-level.index') }}"
                >Pengguna</a
                >
                <svg
                x-ignore
                xmlns="http://www.w3.org/2000/svg"
                class="h-4 w-4"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                >
                <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M9 5l7 7-7 7"
                />
                </svg>
            </li>
            <li>Posisi Level</li>
        </ul>
    </div>
    <div class="grid grid-cols-1 gap-4 sm:gap-5 lg:gap-6">


        <!-- Table With Filter -->
        <div x-data="{isFilterExpanded:false}">
            <div class="flex items-center justify-between">
                <h2
                class="text-base font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100"
                >
                Daftar Posisi Level
                </h2>
                <div class="flex">
                    <div class="flex items-center" x-data="{isInputActive:false}">
                        <form action="{{ route('posisi-level.index') }}" method="get">
                            <label class="block">
                            <input
                                x-effect="isInputActive === true && $nextTick(() => { $el.focus()});"
                                :class="isInputActive ? 'w-32 lg:w-48' : 'w-0'"
                                class="form-input {{ !empty($search) ? 'w-32 lg:w-48' : 'w-0'}} bg-transparent px-1 text-right transition-all duration-100 placeholder:text-slate-500 dark:placeholder:text-navy-200"
                                placeholder="Search here..."
                                @if(!empty($search))  value="{{$search}}" @endif
                                type="text"
                                name="search"
                            />
                            </label>
                        </form>
                        <button
                        @click="isInputActive = !isInputActive"
                        class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="h-4.5 w-4.5"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="1.5"
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                            />
                        </svg>
                        </button>
                    </div>

                    <div
                    x-data="usePopper({placement:'bottom-end',offset:4})"
                    @click.outside="isShowPopper && (isShowPopper = false)"
                    class="inline-flex"
                    >
                    <button
                        x-ref="popperRef"
                        @click="isShowPopper = !isShowPopper"
                        class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                    >
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="h-4.5 w-4.5"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"
                        />
                        </svg>
                    </button>
                    <div
                        x-ref="popperRoot"
                        class="popper-root"
                        :class="isShowPopper && 'show'"
                    >
                        <div
                        class="popper-box rounded-md border border-slate-150 bg-white py-1.5 font-inter dark:border-navy-500 dark:bg-navy-700"
                        >
                        <ul>
                            <li>
                            <a
                                href="{{ route('posisi-level.create') }}"
                                class="flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                >Tambah Data</a
                            >
                            </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
            <div class="card mt-3">
                <div class="is-scrollbar-hidden min-w-full overflow-x-auto">
                <table class="is-hoverable w-full text-left">
                    <thead>
                        <tr>
                            <th
                                class="whitespace-nowrap rounded-tl-lg bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                #
                            </th>
                            <th
                                class="whitespace-nowrap bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Nama Posisi Level
                            </th>
                            <th
                                class="whitespace-nowrap text-center bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                                Jumlah Karyawan
                            </th>
                            <th
                                class="whitespace-nowrap rounded-tr-lg bg-slate-200 px-4 py-3 font-semibold uppercase text-slate-800 dark:bg-navy-800 dark:text-navy-100 lg:px-5"
                            >
                            Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($posisi_levels) > 0)
                            @foreach($posisi_levels as $posisi_level)
                                <tr
                                    class="border-y border-transparent border-b-slate-200 dark:border-b-navy-500"
                                >
                                    <td class="whitespace-nowrap px-4 py-3 sm:px-5">{{ $loop->iteration }}</td>
                                    <td
                                        class="whitespace-nowrap px-4 py-3 font-medium text-slate-700 dark:text-navy-100 sm:px-5"
                                    >
                                        {{ $posisi_level->nama_posisi_level }}
                                    </td>
                                    <td class="whitespace-nowrap px-4 py-3 sm:px-5 text-center">
                                        {{ count($posisi_level->karyawans) }}
                                    </td>
                                    <td class="whitespace-nowrap px-4 py-3 sm:px-5">
                                    <div
                                        x-data="usePopper({placement:'bottom-end',offset:4})"
                                        @click.outside="isShowPopper && (isShowPopper = false)"
                                        class="inline-flex"
                                    >
                                        <button
                                        x-ref="popperRef"
                                        @click="isShowPopper = !isShowPopper"
                                        class="btn h-8 w-8 rounded-full p-0 hover:bg-slate-300/20 focus:bg-slate-300/20 active:bg-slate-300/25 dark:hover:bg-navy-300/20 dark:focus:bg-navy-300/20 dark:active:bg-navy-300/25"
                                        >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="h-5 w-5"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            stroke-width="2"
                                        >
                                            <path
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"
                                            />
                                        </svg>
                                        </button>
            
                                        <div
                                        x-ref="popperRoot"
                                        class="popper-root"
                                        :class="isShowPopper && 'show'"
                                        >
                                        <div
                                            class="popper-box rounded-md border border-slate-150 bg-white py-1.5 font-inter dark:border-navy-500 dark:bg-navy-700"
                                        >
                                            <ul>
                                                <li>
                                                    <a
                                                    href="{{ route('posisi-level.edit', $posisi_level->id) }}"
                                                    class="cursor-pointer flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                    >Ubah</a
                                                    >
                                                </li>
                                            </ul>
                                            <div
                                            class="my-1 h-px bg-slate-150 dark:bg-navy-500"
                                            ></div>
                                            <ul>
                                                <li>
                                                    <a
                                                    type="button"
                                                    data-route="{{ route('posisi-level.destroy', $posisi_level->id) }}"
                                                    data-token="{{ csrf_token() }}"
                                                    class="delete-data cursor-pointer flex h-8 items-center px-3 pr-8 font-medium tracking-wide outline-none transition-all hover:bg-slate-100 hover:text-slate-800 focus:bg-slate-100 focus:text-slate-800 dark:hover:bg-navy-600 dark:hover:text-navy-100 dark:focus:bg-navy-600 dark:focus:text-navy-100"
                                                    >Hapus</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr
                                class="border-y border-transparent border-b-slate-200 dark:border-b-navy-500"
                            >
                                <td class="whitespace-nowrap px-4 py-3 sm:px-5 text-center" colspan="4">
                                    Data tidak ditemukan
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                </div>

                <div
                class="flex flex-col justify-between space-y-4 px-4 py-4 sm:flex-row sm:items-center sm:space-y-0 sm:px-5"
                >
                    <div class="text-xs+"></div>
                    {{ $posisi_levels->links('vendor.pagination.tailwind') }}
                </div> 


            </div>
        </div>

    </div>
</main>
@endsection
