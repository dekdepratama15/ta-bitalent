<!DOCTYPE html>
<html>
<head>
    <title>Lembur</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Nama</b></th>
                <th><b>Akan dibayar</b></th>
                <th><b>Status</b></th>
                <th><b>Alasan Pinjam</b></th>
                <th><b>Total</b></th>
            </tr>
        </thead>
        <tbody>
            @php 
                $total_all = 0;
                $total_belum = 0;
                $total_sudah = 0;
            @endphp
            @foreach($pinjamans as $key => $pinjaman)
                @php 
                    $total_all += $pinjaman->total;
                    if ($pinjaman->status == 'done') {
                        $total_sudah += $pinjaman->total;
                    } else {
                        $total_belum += $pinjaman->total;
                    }
                @endphp
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$pinjaman->karyawan->nama_lengkap}}</td>
                    <td>{{date('Y-m-d, H:s', strtotime($pinjaman->akan_dibayar_tanggal))}}</td>
                    <td>{{staticHelper()['status'][$pinjaman->status]['text']}}</td>
                    <td>{{$pinjaman->alasan}}</td>
                    <td>{{$pinjaman->total}}</td>
                </tr>
            @endforeach
                <tr>
                    <td colspan="5" style="text-align: right"><b>TOTAL BELUM BAYAR</b></td>
                    <td>{{$total_belum}}</td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right"><b>TOTAL SUDAH BAYAR</b></td>
                    <td>{{$total_sudah}}</td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right"><b>TOTAL SEMUA</b></td>
                    <td>{{$total_all}}</td>
                </tr>
        </tbody>
    </table>
</body>
</html>