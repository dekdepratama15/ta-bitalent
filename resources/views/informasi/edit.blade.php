@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.information')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Ubah Informasi & Kegiatan
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('informasi.index') }}"
            >Informasi</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('informasi.index') }}"
            >Informasi & Kegiatan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Ubah Informasi & Kegiatan</li>
        </ul>
    </div>

    <form action="{{ route('informasi.update', $informasi->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            <div class="col-span-12 sm:col-span-12">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Informasi & Kegiatan
                    </p>
                    <div class="mt-4 space-y-4">
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                            <label class="block">
                                <span>Judul Informasi & Kegiatan</span>
                                <span class="relative mt-1.5 flex">
                                    <input
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Judul Informasi & Kegiatan"
                                    type="text"
                                    name="judul"
                                    value="{{$informasi->judul_informasi}}"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Tanggal Informasi & Kegiatan</span>
                                <span class="relative mt-1.5 flex" id="sekali">
                                    <input
                                    x-init="$el._x_flatpickr = flatpickr($el)"
                                    class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                    placeholder="Tanggal Informasi & Kegiatan"
                                    name="tanggal"
                                    type="text"
                                    value="{{date('Y-m-d', strtotime($informasi->tanggal))}}"
                                    required
                                    />
                                    <span
                                    class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                    >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="h-5 w-5 transition-colors duration-200"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            stroke-width="1.5"
                                        >
                                            <path
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                            />
                                        </svg>
                                    </span>
                                </span>
                            </label>
                            <label class="block">
                                <span>Gambar</span>
                                
                                <span class="relative mt-1.5 flex -space-x-px">
                                    <input
                                    class="form-input cursor-pointer peer w-full @if(!empty($informasi->file)) rounded-l-lg @else rounded-lg @endif border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent disabled:bg-zinc-100 dark:disabled:bg-navy-600"
                                    placeholder="Nama Level Posisi"
                                    type="file"
                                    name="file_kegiatan"
                                    />
                                    @if(!empty($informasi->file))
                                    <a
                                        type="button"
                                        target="_blank"
                                        href="{{ asset($informasi->file) }}"
                                        class="fancybox_file btn rounded-l-none bg-secondary font-medium text-white hover:bg-secondary-focus focus:bg-secondary-focus active:bg-secondary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                                    >
                                        Lihat
                                    </a>
                                    @endif
                                </span>
                                @error('file_kegiatan')
                                    <span class="text-tiny+ text-error">{{ $message }}</span>
                                @enderror
                            </label>
                        </div>
                        <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                            <label class="block">
                                <span>Keterangan</span>
                                <textarea
                                rows="4"
                                placeholder="Keterangan"
                                class="form-textarea mt-1.5 w-full rounded-lg border border-slate-300 bg-transparent p-2.5 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                name="keterangan"
                                required
                                >{{$informasi->keterangan}}</textarea>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
    
    
            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
    
                    <div class="flex justify-end space-x-2">
                        <a 
                        type="button"
                        href="{{route('informasi.index')}}"
                        class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                        >
                        <span>Kembali</span>
                        </a>
                        <button
                        class="btn space-x-2 bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                        >
                        <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
    </main>
@endsection

