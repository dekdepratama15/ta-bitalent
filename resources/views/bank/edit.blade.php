@extends('layouts.app')

@section('sidebar')
    @include('layouts.sidebar.finance')
@endsection

@section('content')
<!-- Main Content Wrapper -->
<main class="main-content w-full px-[var(--margin-x)] pb-8">
    <div class="flex items-center space-x-4 py-5 lg:py-6">
        <h2
        class="text-xl font-medium text-slate-800 dark:text-navy-50 lg:text-2xl"
        >
        Ubah Bank
        </h2>
        <div class="hidden h-full py-1 sm:flex">
        <div class="h-full w-px bg-slate-300 dark:bg-navy-600"></div>
        </div>
        <ul class="hidden flex-wrap items-center space-x-2 sm:flex">
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('bank.index') }}"
            >Keuangan</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li class="flex items-center space-x-2">
            <a
            class="text-primary transition-colors hover:text-primary-focus dark:text-accent-light dark:hover:text-accent"
            href="{{ route('bank.index') }}"
            >Bank</a
            >
            <svg
            x-ignore
            xmlns="http://www.w3.org/2000/svg"
            class="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M9 5l7 7-7 7"
            />
            </svg>
        </li>
        <li>Ubah Bank</li>
        </ul>
    </div>

    <form action="{{ route('bank.update', $bank->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="grid grid-cols-12 gap-4 sm:gap-5 lg:gap-6">
            <div class="col-span-12 sm:col-span-12">
                <div class="card p-4 sm:p-5">
                    <p
                    class="text-base font-medium text-slate-700 dark:text-navy-100"
                    >
                    Data Bank
                    </p>
                    <div class="mt-4 space-y-4">
                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-1">
                        <label class="block">
                            <span>Nama Bank</span>
                            <span class="relative mt-1.5 flex">
                                <input
                                class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 pl-9 placeholder:text-slate-400/70 hover:border-slate-400 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                placeholder="Nama Bank"
                                type="text"
                                name="nama_bank"
                                value="{{ $bank->nama_bank }}"
                                required
                                />
                                <span
                                class="pointer-events-none absolute flex h-full w-10 items-center justify-center text-slate-400 peer-focus:text-primary dark:text-navy-300 dark:peer-focus:text-accent"
                                >
                                <i class="fa-solid fa-building-columns"></i>
                                </span>
                            </span>
                        </label>
                    </div>
                    </div>
                </div>
            </div>
    
    
            <div class="col-span-12 sm:col-span-12 mt-5">
                <div class="card p-4 sm:p-5">
    
                    <div class="flex justify-end space-x-2">
                        <a 
                        type="button"
                        href="{{route('bank.index')}}"
                        class="btn space-x-2 bg-slate-150 font-medium text-slate-800 hover:bg-slate-200 focus:bg-slate-200 active:bg-slate-200/80 dark:bg-navy-500 dark:text-navy-50 dark:hover:bg-navy-450 dark:focus:bg-navy-450 dark:active:bg-navy-450/90"
                        >
                        <span>Kembali</span>
                        </a>
                        <button
                        class="btn space-x-2 bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                        >
                        <span>Ubah</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
    </main>
@endsection
