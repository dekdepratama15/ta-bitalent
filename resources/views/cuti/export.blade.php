<!DOCTYPE html>
<html>
<head>
    <title>Cuti</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Nama</b></th>
                <th><b>Tanggal Mulai</b></th>
                <th><b>Tanggal Selesai</b></th>
                <th><b>Status</b></th>
                <th><b>Keterangan</b></th>
            </tr>
        </thead>
        <tbody>
            @foreach($cutis as $key => $cuti)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cuti->karyawan->nama_lengkap}}</td>
                    <td>{{date('Y-m-d', strtotime($cuti->tgl_mulai))}}</td>
                    <td>{{date('Y-m-d', strtotime($cuti->tgl_selesai))}}</td>
                    <td>{{staticHelper()['status'][$cuti->status]['text']}}</td>
                    <td>{{$cuti->keterangan}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>