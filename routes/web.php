<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
Route::group(['middleware' => ['auth','admin']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/notifikasi', 'HomeController@notifikasi')->name('notifikasi');

    Route::prefix('users')->group(function () {
        Route::resource('/divisi', DivisiController::class);
        Route::resource('/posisi', PosisiController::class);
        Route::resource('/posisi-level', PosisiLevelController::class);
        Route::resource('/mutasi', MutasiController::class);

        Route::resource('/karyawan', KaryawanController::class);
        Route::post('/karyawan/change-status/{karyawan}', 'KaryawanController@changeStatus')->name('karyawan.change-status');
    });
 
    Route::prefix('time-management')->group(function () {
        Route::resource('/kehadiran', KehadiranController::class);
        Route::resource('/lembur', LemburController::class);
        Route::resource('/libur', LiburController::class);
        Route::resource('/cuti', CutiController::class);
        Route::post('/cuti/change-status/{cuti}', 'CutiController@changeStatus')->name('cuti.change-status');

        Route::get('/cuti-export', 'CutiController@exportExcel')->name('cuti.export');
        Route::get('/lembur-export', 'LemburController@exportExcel')->name('lembur.export');
        Route::get('/kehadiran-export', 'KehadiranController@exportExcel')->name('kehadiran.export');
        Route::get('/kehadiran/report/create/{id}', 'KehadiranController@createReport')->name('kehadiran.report.create');
        Route::post('/kehadiran/report/store/{id}', 'KehadiranController@storeReport')->name('kehadiran.report.store');
        Route::delete('/kehadiran/report/destroy/{id}', 'KehadiranController@destroyReport')->name('kehadiran.report.destroy');
    });

    Route::prefix('finance')->group(function () {
        Route::resource('/bank', BankController::class);
        Route::resource('/tunjangan', TunjanganController::class);
        Route::resource('/tunjangan-karyawan', TunjanganKaryawanController::class);
        Route::resource('/penggajian', PenggajianController::class);

        Route::resource('/pinjaman', PinjamanController::class);
        Route::post('/pinjaman/change-status/{pinjaman}', 'PinjamanController@changeStatus')->name('pinjaman.change-status');
        Route::get('/pinjaman-export', 'PinjamanController@exportExcel')->name('pinjaman.export');

        Route::resource('/reimbursement', ReimbursementController::class);
        Route::post('/reimbursement/change-status/{reimbursement}', 'ReimbursementController@changeStatus')->name('reimbursement.change-status');
        Route::get('/reimbursement-export', 'ReimbursementController@exportExcel')->name('reimbursement.export');
    });

    Route::prefix('information')->group(function () {
        Route::resource('/informasi', InformasiController::class);
    });
 });

