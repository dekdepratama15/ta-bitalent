<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'ApiController@login')->name('api.login');
Route::middleware('auth:api')->group( function () {

    Route::prefix('notifikasi')->group(function () {
        Route::get('/', 'ApiController@listNotifikasi')->name('api.notifikasi.list');
    });

    Route::prefix('reimbursement')->group(function () {
        Route::get('/', 'ApiController@listReimbursement')->name('api.reimbursement.list');
        Route::get('/{id}', 'ApiController@showReimbursement')->name('api.reimbursement.show');
        Route::post('/store', 'ApiController@storeReimbursement')->name('api.reimbursement.store');
        Route::post('/{id}/update', 'ApiController@updateReimbursement')->name('api.reimbursement.update');
        Route::delete('/{id}/destroy', 'ApiController@deleteReimbursement')->name('api.reimbursement.delete');
    });

    Route::prefix('pinjaman')->group(function () {
        Route::get('/', 'ApiController@listPinjaman')->name('api.pinjaman.list');
        Route::get('/{id}', 'ApiController@showPinjaman')->name('api.pinjaman.show');
        Route::post('/store', 'ApiController@storePinjaman')->name('api.pinjaman.store');
        Route::post('/{id}/update', 'ApiController@updatePinjaman')->name('api.pinjaman.update');
        Route::delete('/{id}/destroy', 'ApiController@deletePinjaman')->name('api.pinjaman.delete');
    });

    Route::prefix('lembur')->group(function () {
        Route::get('/', 'ApiController@listLembur')->name('api.lembur.list');
        Route::get('/{id}', 'ApiController@showLembur')->name('api.lembur.show');
        Route::post('/store', 'ApiController@storeLembur')->name('api.lembur.store');
        Route::post('/{id}/update', 'ApiController@updateLembur')->name('api.lembur.update');
        Route::delete('/{id}/destroy', 'ApiController@deleteLembur')->name('api.lembur.delete');
    });

    Route::prefix('cuti')->group(function () {
        Route::get('/', 'ApiController@listCuti')->name('api.cuti.list');
        Route::get('/{id}', 'ApiController@showCuti')->name('api.cuti.show');
        Route::post('/store', 'ApiController@storeCuti')->name('api.cuti.store');
        Route::post('/{id}/update', 'ApiController@updateCuti')->name('api.cuti.update');
        Route::delete('/{id}/destroy', 'ApiController@deleteCuti')->name('api.cuti.delete');
    });

    Route::prefix('tunjangan-karyawan')->group(function () {
        Route::get('/', 'ApiController@listTunjanganKaryawan')->name('api.tunjangan_karyawan.list');
        Route::get('/{id}', 'ApiController@showTunjanganKaryawan')->name('api.tunjangan_karyawan.show');
    });

    Route::prefix('karyawan')->group(function () {
        Route::get('/', 'ApiController@listKaryawan')->name('api.karyawan.list');
        Route::get('/{id}', 'ApiController@showKaryawan')->name('api.karyawan.show');
        Route::get('/getby/user', 'ApiController@showKaryawanByUser')->name('api.karyawan.showByUser');
        Route::post('/update-profile', 'ApiController@updateProfileKaryawan')->name('api.karyawan.updateProfile');
        Route::post('/update-password', 'ApiController@updatePasswordKaryawan')->name('api.karyawan.updatePassword');
    });

    Route::prefix('informasi')->group(function () {
        Route::get('/', 'ApiController@listInformasi')->name('api.informasi.list');
        Route::get('/{id}', 'ApiController@showInformasi')->name('api.informasi.show');
    });

    Route::prefix('kehadiran')->group(function () {
        Route::get('/', 'ApiController@listKehadiran')->name('api.kehadiran.list');
        Route::get('/day/now', 'ApiController@dayNowKehadiran')->name('api.kehadiran.now');
        Route::get('/{id}', 'ApiController@showKehadiran')->name('api.kehadiran.show');
        Route::post('/store', 'ApiController@storeKehadiran')->name('api.kehadiran.store');
        Route::post('/{id}/update', 'ApiController@updateKehadiran')->name('api.kehadiran.update');
    });

    Route::prefix('report')->group(function () {
        Route::get('/{id}', 'ApiController@listReportHarian')->name('api.report.show');
        Route::post('/{id}/store', 'ApiController@storeReportHarian')->name('api.report.store');
    });

    Route::prefix('penggajian')->group(function () {
        Route::get('/', 'ApiController@listPenggajian')->name('api.penggajian.list');
        Route::get('/{id}', 'ApiController@showPenggajian')->name('api.penggajian.show');
    });

    Route::prefix('pengajuan')->group(function () {
        Route::get('/', 'ApiController@listPengajuan')->name('api.pengajuan.list');
    });

});
