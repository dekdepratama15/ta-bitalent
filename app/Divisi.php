<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    public function posisis()
    {
        return $this->hasMany(Posisi::class);
    }
}
