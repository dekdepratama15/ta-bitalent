<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenggajianLembur extends Model
{
    public function penggajian()
    {
        return $this->belongsTo(Penggajian::class);
    }
}
