<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TunjanganKaryawan extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function tunjangan()
    {
        return $this->belongsTo(Tunjangan::class);
    }
}
