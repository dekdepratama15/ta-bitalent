<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenggajianReimbursement extends Model
{
    public function penggajian()
    {
        return $this->belongsTo(Penggajian::class);
    }
}
