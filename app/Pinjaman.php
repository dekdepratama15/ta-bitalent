<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'pinjamans';

    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }
}
