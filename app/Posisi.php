<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    public function divisi()
    {
        return $this->belongsTo(Divisi::class);
    }

    public function karyawans()
    {
        return $this->hasMany(Karyawan::class);
    }
}
