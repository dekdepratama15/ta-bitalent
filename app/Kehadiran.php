<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kehadiran extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function kehadiran_details()
    {
        return $this->hasMany(KehadiranDetail::class);
    }
}
