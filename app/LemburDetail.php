<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LemburDetail extends Model
{
    public function lembur()
    {
        return $this->belongsTo(Lembur::class);
    }
}
