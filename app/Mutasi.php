<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function posisi_sebelum()
    {
        return $this->belongsTo(Posisi::class, 'posisi_sebelum_id');
    }

    public function level_posisi_sebelum()
    {
        return $this->belongsTo(PosisiLevel::class, 'posisi_level_sebelum_id');
    }

}
