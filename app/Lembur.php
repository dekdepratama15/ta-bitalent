<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lembur extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function lembur_details()
    {
        return $this->hasMany(LemburDetail::class);
    }
}
