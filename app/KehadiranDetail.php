<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KehadiranDetail extends Model
{
    public function kehadiran()
    {
        return $this->belongsTo(Kehadiran::class);
    }
}
