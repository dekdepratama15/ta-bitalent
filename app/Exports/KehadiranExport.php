<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;

class KehadiranExport implements FromView, ShouldAutoSize
{
    protected $data;
    protected $numberDay;
    protected $month;
    protected $year;

    public function __construct($data)
    {
        $this->data = $data[0];
        $this->numberDay = $data[1];
        $this->month = $data[2];
        $this->year = $data[3];
    }

    public function view(): View
    {
        return view('kehadiran.export')->with('kehadirans', $this->data)->with('numberDay', $this->numberDay)->with('month', $this->month)->with('year', $this->year);
    }
}
