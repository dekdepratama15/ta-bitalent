<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reimbursement extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }
}
