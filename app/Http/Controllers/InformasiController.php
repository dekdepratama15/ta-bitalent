<?php

namespace App\Http\Controllers;

use App\Informasi;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $informasis  = Informasi::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $informasis->where('judul_informasi', 'like', '%' . $search . '%');
        }
        $informasis = $informasis->paginate(10);

        return view('informasi.index',[
            'informasis' => $informasis,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('informasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file_kegiatan' => 'mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);

        $informasi = new Informasi();
        $informasi->judul_informasi = $request->judul;
        $informasi->keterangan = $request->keterangan;
        $informasi->tanggal = $request->tanggal;
        if (isset($request->file_kegiatan) && !empty($request->file_kegiatan)) {
            $informasi->file = uploadFile($request->file_kegiatan, 'informasi');
        }
        $informasi->save();

        $this->createNotifikasi([
            'judul' => 'Informasi Baru',
            'keterangan' => 'Informasi '. $request->judul .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);
        session()->flash('success', 'Data Informasi & Kegiatan Berhasil Ditambah');
        return redirect()->route('informasi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function show(Informasi $informasi)
    {
        return view('informasi.show', [
            'informasi' => $informasi
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Informasi $informasi)
    {
        return view('informasi.edit', [
            'informasi' => $informasi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informasi $informasi)
    {
        $request->validate([
            'file_kegiatan' => 'mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);
        $informasi->judul_informasi = $request->judul;
        $informasi->keterangan = $request->keterangan;
        $informasi->tanggal = $request->tanggal;
        if (isset($request->file_kegiatan) && !empty($request->file_kegiatan)) {
            $informasi->file = uploadFile($request->file_kegiatan, 'informasi');
        }
        $informasi->save();

        $this->createNotifikasi([
            'judul' => 'Informasi Diubah',
            'keterangan' => 'Terdapat perubahan data pada Informasi '.$request->judul,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);
        session()->flash('success', 'Data Informasi & Kegiatan Berhasil Diubah');
        return redirect()->route('informasi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informasi $informasi)
    {

        $this->createNotifikasi([
            'judul' => 'Informasi Dihapus',
            'keterangan' => 'Informasi '.$informasi->judul_informasi.' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        Informasi::destroy($informasi->id);
        return response()->json([
            'message' => 'Data Informasi & Kegiatan Berhasil Dihapus'
        ]);
    }
}
