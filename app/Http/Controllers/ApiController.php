<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\Cuti;
use App\Libur;
use App\Lembur;
use App\Pinjaman;
use App\Notifikasi;
use App\LemburDetail;
use App\Reimbursement;
use App\Karyawan;
use App\Informasi;
use App\Penggajian;
use App\Tunjangan;
use App\Kehadiran;
use App\KehadiranDetail;
use App\TunjanganKaryawan;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    public function login(Request $request) {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string',
            'password' => 'required|string|min:8',
        ]);

        if (!$validator->fails()) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                
                $user  = User::find(auth()->user()->id);
                $token = $user->createToken('bitalent'.auth()->user()->id)->accessToken;

                $check_tanggung_jawab = Karyawan::where('penanggung_jawab_id', $user->id)->get();
                $user->karyawan->haveTanggungJawab = count($check_tanggung_jawab) > 0 ? true : false;

                if ($user->karyawan->status != 'aktif') {
                    return response()->json([
                        'error' => true,
                        'message' => 'Karyawan telah dinonaktifkan',
                    ], 401);
                }

                $this->createNotifikasi([
                    'judul' => 'Karyawan Login',
                    'keterangan' => 'Karyawan '.$user->karyawan->nama_lengkap.' telah melakukan login pada aplikasi mobile',
                    'icon' => 'fa-solid fa-arrow-right-to-bracket',
                    'color' => 'success',
                    'karyawan_id' => $user->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'email' => $request->email, 
                    'password' => $request->password,
                    'user' => $user,
                    'karyawan' => $user->karyawan,
                    'token' => $token,
                ], 200);
            }
            return response()->json([
                'error' => true,
                'message' => 'Email atau Password Salah',
            ], 401);
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'error_message' => $validator->errors(),
        ], 401);
    }

    public function listNotifikasi() {
        try {
            
            $notifikasi = Notifikasi::where('karyawan_id', auth()->user()->karyawan->id)->orderBy('id', 'DESC')->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data notifikasi',
                'data' => $notifikasi,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API notifikasi list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function listReimbursement() {
        try {
            
            $reimbursement = Reimbursement::with(['karyawan'])->where('karyawan_id', auth()->user()->karyawan->id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data reimbursement',
                'data' => $reimbursement,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API reimbursement list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showReimbursement($id) {
        try {

            $reimbursement = Reimbursement::with(['karyawan'])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data reimbursement',
                'data' => $reimbursement,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API reimbursement show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function storeReimbursement(Request $request) {
        $validator = Validator::make($request->all(),[
            'keterangan' => 'required',
            'total' => 'required',
            'file' => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);
        
        if (!$validator->fails()) {
            try {

                $reimbursement = new Reimbursement();
                $reimbursement->karyawan_id = auth()->user()->karyawan->id;
                $reimbursement->file = uploadFile($request->file, 'reimbursement');
                $reimbursement->keterangan = $request->keterangan;
                $reimbursement->total = $request->total;
                $reimbursement->status = 'pending';
                $reimbursement->save();

                $this->createNotifikasi([
                    'judul' => 'Rembes Baru',
                    'keterangan' => 'Karyawan '.auth()->user()->karyawan->nama_lengkap.' telah menambahkan data rembes baru',
                    'icon' => 'fa-solid fa-money-bill-transfer',
                    'color' => 'warning',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data reimbursement',
                    'data' => $reimbursement,
                    'url' => url('/')
                ], 200);

            } catch (\Exception $e) {
                Log::error('Error API reimbursement store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updateReimbursement(Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'status' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                $reimbursement = Reimbursement::find($id);
                $reimbursement->status = $request->status;
                $reimbursement->save();

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah data reimbursement',
                    'data' => $reimbursement,
                    'url' => url('/')
                ], 200);

            } catch (\Exception $e) {
                Log::error('Error API reimbursement store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function deleteReimbursement($id) {
        try {
            $reimbursement = Reimbursement::find($id);
            if ($reimbursement == null) {
                return response()->json([
                    'error' => true,
                    'message' => 'Data reimbursement tidak ditemukan',
                ], 404);
            }


            $this->createNotifikasi([
                'judul' => 'Rembes Dihapus',
                'keterangan' => 'Karyawan '.$reimbursement->karyawan->nama_lengkap.' telah menghapus data rembes',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
                'karyawan_id' => $reimbursement->karyawan->id
            ]);

            $reimbursement->delete();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus data reimbursement',
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API reimbursement delete : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam menghapus data',
            ], 500);
        }
    }

    public function listPinjaman() {
        try {

            $pinjaman = Pinjaman::with(['karyawan'])->where('karyawan_id', auth()->user()->karyawan->id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data pinjaman',
                'data' => $pinjaman,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API pinjaman list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showPinjaman($id) {
        try {

            $pinjaman = Pinjaman::with(['karyawan'])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data pinjaman',
                'data' => $pinjaman,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API pinjaman show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function storePinjaman(Request $request) {
        $validator = Validator::make($request->all(),[
            'keterangan' => 'required',
            'total' => 'required',
            'alasan' => 'required',
            'akan_dibayar_tanggal' => 'required',
            'file' => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);
        
        if (!$validator->fails()) {
            try {

                $pinjaman = new Pinjaman();
                $pinjaman->karyawan_id = auth()->user()->karyawan->id;
                $pinjaman->file = uploadFile($request->file, 'pinjaman');
                $pinjaman->keterangan = $request->keterangan;
                $pinjaman->alasan = $request->alasan;
                $pinjaman->total = $request->total;
                $pinjaman->akan_dibayar_tanggal = $request->akan_dibayar_tanggal;
                $pinjaman->status = 'pending';
                $pinjaman->save();
                
                $this->createNotifikasi([
                    'judul' => 'Pinjaman Baru',
                    'keterangan' => 'Karyawan '.auth()->user()->karyawan->nama_lengkap.' telah menambahkan data pinjaman baru',
                    'icon' => 'fa-solid fa-hand-holding-dollar',
                    'color' => 'warning',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data pinjaman',
                    'data' => $pinjaman,
                    'url' => url('/')
                ], 200);

            } catch (\Exception $e) {
                Log::error('Error API pinjaman store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updatePinjaman(Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'status' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                $pinjaman = Pinjaman::find($id);
                $pinjaman->status = $request->status;
                $pinjaman->save();

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah data pinjaman',
                    'data' => $pinjaman,
                    'url' => url('/')
                ], 200);

            } catch (\Exception $e) {
                Log::error('Error API pinjaman store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function deletePinjaman($id) {
        try {
            $pinjaman = Pinjaman::find($id);
            if ($pinjaman == null) {
                return response()->json([
                    'error' => false,
                    'message' => 'Data pinjaman tidak ditemukan',
                ], 404);
            }

            $this->createNotifikasi([
                'judul' => 'Pinjaman Dihapus',
                'keterangan' => 'Karyawan '.$pinjaman->karyawan->nama_lengkap.' telah menghapus data pinjaman',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
                'karyawan_id' => $pinjaman->karyawan->id
            ]);

            $pinjaman->delete();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus data pinjaman',
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API pinjaman delete : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam menghapus data',
            ], 500);
        }
    }

    public function listLembur() {
        try {
            $lembur = Lembur::with(['karyawan', 'lembur_details'])->where('karyawan_id', auth()->user()->karyawan->id)->orderBy('created_at', 'DESC')->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data lembur',
                'data' => $lembur,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API lembur list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showLembur($id) {
        try {

            $lembur = Lembur::with(['karyawan', 'lembur_details'])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data lembur',
                'data' => $lembur,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API lembur show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function storeLembur(Request $request) {
        $validator = Validator::make($request->all(),[
            'keterangan' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
            // 'keterangan_file.*' => 'required',
            // 'file.*' => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);

        
        if (!$validator->fails()) {
            try {

                DB::beginTransaction();
                $lembur = new Lembur();
                $lembur->karyawan_id = auth()->user()->karyawan->id;
                $lembur->keterangan = $request->keterangan;
                $lembur->jam_mulai = $request->jam_mulai;
                $lembur->jam_selesai = $request->jam_selesai;

                $jam_mulai = strtotime($request->jam_mulai); 
                $jam_selesai = strtotime($request->jam_selesai);
                
                $selisih_detik = $jam_selesai - $jam_mulai;
                $selisih_jam = floor($selisih_detik / 3600); 
                $selisih_menit = ($selisih_detik % 3600) / 60; 
                $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100);
                $karyawan = Karyawan::find(auth()->user()->karyawan->id);
                $status = !empty($karyawan->penanggung_jawab_id) ? 'pending' : 'approve_lead';

                $lembur->jumlah_jam = $jumlah;
                $lembur->status = $status;
                $lembur->save();

                if (isset($request->file)) {
                    foreach ($request->file as $key => $file) {
                        $lembur_detail = new LemburDetail();
                        $lembur_detail->lembur_id = $lembur->id;
                        $lembur_detail->file = uploadFile($file, 'lembur');
                        $keterangan_file = isset($request->keterangan_file[$key]) && !empty($request->keterangan_file[$key]) ? $request->keterangan_file[$key] : '-';
                        $lembur_detail->keterangan = $keterangan_file;
                        $lembur_detail->save();
                    }
                }

                DB::commit();

                $this->createNotifikasi([
                    'judul' => 'Lembur Baru',
                    'keterangan' => 'Karyawan '.$lembur->karyawan->nama_lengkap.' telah menambahkan data lembur baru',
                    'icon' => 'fa-regular fa-clock',
                    'color' => 'warning',
                    'karyawan_id' => $lembur->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data lembur',
                    'data' => $lembur,
                    'url' => url('/')
                ], 200);
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API lembur store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updateLembur(Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'status' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                $lembur = Lembur::find($id);
                $lembur->status = $request->status;
                $lembur->save();

                if ($request->status == 'approve_lead') {
                    $this->createNotifikasi([
                        'judul' => 'Lembur Disetujui',
                        'keterangan' => auth()->user()->karyawan->nama_lengkap.' telah menyetujui lembur dari Karyawan '.$lembur->karyawan->nama_lengkap,
                        'icon' => 'fa-regular fa-thumbs-up',
                        'color' => 'primary',
                        'karyawan_id' => $lembur->karyawan->id
                    ]);    
                } else {
                    $this->createNotifikasi([
                        'judul' => 'Lembur Ditolak',
                        'keterangan' => auth()->user()->karyawan->nama_lengkap.' telah menolak lembur dari Karyawan '.$lembur->karyawan->nama_lengkap,
                        'icon' => 'fa-regular fa-thumbs-down',
                        'color' => 'danger',
                        'karyawan_id' => $lembur->karyawan->id
                    ]);
    
                }
                
                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah data lembur',
                    'data' => $lembur,
                    'url' => url('/')
                ], 200);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API lembur store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function deleteLembur($id) {
        try {
            $lembur = Lembur::find($id);
            if ($lembur == null) {
                return response()->json([
                    'error' => true,
                    'message' => 'Data lembur tidak ditemukan',
                ], 404);
            }

            $this->createNotifikasi([
                'judul' => 'Lembur Dihapus',
                'keterangan' => 'Karyawan '.$lembur->karyawan->nama_lengkap.' telah menghapus data lembur',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
                'karyawan_id' => $lembur->karyawan->id
            ]);

            $lembur->lembur_details()->delete();
            $lembur->delete();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus data lembur',
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API lembur delete : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam menghapus data',
            ], 500);
        }
    }

    public function listCuti() {
        try {
            $cuti = Cuti::with(['karyawan'])->where('karyawan_id', auth()->user()->karyawan->id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data cuti',
                'data' => $cuti,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API cuti list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showCuti($id) {
        try {

            $cuti = Cuti::with(['karyawan'])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data cuti',
                'data' => $cuti,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API cuti show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function storeCuti(Request $request) {
        $validator = Validator::make($request->all(),[
            'keterangan' => 'required',
            'tgl_mulai' => 'required',
            'tgl_selesai' => 'required',
        ]);

        
        if (!$validator->fails()) {
            try {

                DB::beginTransaction();
                $cuti = new Cuti();
                $cuti->karyawan_id = auth()->user()->karyawan->id;
                $cuti->keterangan = $request->keterangan;
                $cuti->tgl_mulai = $request->tgl_mulai;
                $cuti->tgl_selesai = $request->tgl_selesai;

                $tgl_mulai = Carbon::createFromDate($request->tgl_mulai); 
                $tgl_selesai = Carbon::createFromDate($request->tgl_selesai);
                
                $selisih = $tgl_mulai->diffInDays($tgl_selesai) + 1;
                $karyawan = Karyawan::find(auth()->user()->karyawan->id);
                $status = !empty($karyawan->penanggung_jawab_id) ? 'pending' : 'approve_lead';
                $cuti->jumlah_cuti = $selisih;
                $cuti->status = $status;
                $cuti->save();

                $jumlah_cuti = $karyawan->jumlah_cuti - $selisih;
                if ($jumlah_cuti < 0) {
                    return response()->json([
                        'error' => true,
                        'message' => 'Jumlah Cuti hanya tersisa '.$karyawan->jumlah_cuti.' hari',
                    ], 401);
                } else {
                    // $karyawan->jumlah_cuti = $jumlah_cuti;
                    // $karyawan->save();
                }

                DB::commit();

                $this->createNotifikasi([
                    'judul' => 'Cuti Baru',
                    'keterangan' => 'Karyawan '.$cuti->karyawan->nama_lengkap.' telah menambahkan data cuti baru',
                    'icon' => 'fa-solid fa-umbrella-beach',
                    'color' => 'warning',
                    'karyawan_id' => $cuti->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data cuti',
                    'data' => $cuti,
                    'url' => url('/')
                ], 200);
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API cuti store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updateCuti(Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'status' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                $cuti = Cuti::find($id);

                if ($request->status == 'approve_lead') {
                    $sisa = $cuti->karyawan->jumlah_cuti - $cuti->jumlah_cuti;

                    if ($sisa < 0) {
                        return response()->json([
                            'error' => true,
                            'message' => 'Karyawan tidak memiliki sisa jumlah cuti',
                        ], 401);
                    }
                    $cuti->status = $request->status;
                    $cuti->save();
                    $cuti->karyawan->jumlah_cuti = $sisa;
                    $cuti->karyawan->save();
                    $this->createNotifikasi([
                        'judul' => 'Cuti Disetujui',
                        'keterangan' => auth()->user()->karyawan->nama_lengkap.' telah menyetujui cuti dari Karyawan '.$cuti->karyawan->nama_lengkap,
                        'icon' => 'fa-regular fa-thumbs-up',
                        'color' => 'primary',
                        'karyawan_id' => $cuti->karyawan->id
                    ]);
                } else {
                    $cuti->status = $request->status;
                    $cuti->save();
                    $this->createNotifikasi([
                        'judul' => 'Cuti Ditolak',
                        'keterangan' => auth()->user()->karyawan->nama_lengkap.' telah menolak cuti dari Karyawan '.$cuti->karyawan->nama_lengkap,
                        'icon' => 'fa-regular fa-thumbs-down',
                        'color' => 'danger',
                        'karyawan_id' => $cuti->karyawan->id
                    ]);

                }

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah data cuti',
                    'data' => $cuti,
                    'url' => url('/')
                ], 200);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API cuti store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function deleteCuti($id) {
        try {
            $cuti = Cuti::find($id);
            if ($cuti == null) {
                return response()->json([
                    'error' => true,
                    'message' => 'Data cuti tidak ditemukan',
                ], 404);
            }

            $this->createNotifikasi([
                'judul' => 'Cuti Dihapus',
                'keterangan' => 'Karyawan '.$cuti->karyawan->nama_lengkap.' telah menghapus data cuti',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
                'karyawan_id' => $cuti->karyawan->id
            ]);

            $cuti->delete();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus data cuti',
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API cuti delete : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam menghapus data',
            ], 500);
        }
    }

    public function listTunjanganKaryawan() {
        try {
            $tunjangan_karyawan = TunjanganKaryawan::with(['karyawan', 'tunjangan'])->where('karyawan_id', auth()->user()->karyawan->id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data tunjangan karyawan',
                'data' => $tunjangan_karyawan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API tunjangan karyawan list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showTunjanganKaryawan($id) {
        try {

            $tunjangan_karyawan = TunjanganKaryawan::with(['karyawan', 'tunjangan'])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data tunjangan karyawan',
                'data' => $tunjangan_karyawan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API tunjangan karyawan show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function updateProfileKaryawan(Request $request) {
        $validator = Validator::make($request->all(),[
            'foto_profile' => 'mimes:jpg,jpeg,png'
        ]);
        
        if (!$validator->fails()) {
            try {

                $karyawan = Karyawan::find(auth()->user()->karyawan->id);
                $karyawan->nama_lengkap = $request->nama_lengkap;
                $karyawan->nama_panggilan = $request->nama_panggilan;
                $karyawan->no_telp = $request->no_telp;
                if (isset($request->foto_profile) && !empty($request->foto_profile)) {
                    $karyawan->profile = uploadFile($request->foto_profile, 'karyawan');
                }
                $karyawan->save();

                $this->createNotifikasi([
                    'judul' => 'Update Profile',
                    'keterangan' => 'Karyawan '.$karyawan->nama_lengkap.' telah melakukan update profile',
                    'icon' => 'fa-solid fa-user-pen',
                    'color' => 'secondary',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah profile',
                    'data' => $karyawan,
                    'url' => url('/')
                ], 200);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API karyawan update profile : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updatePasswordKaryawan(Request $request) {
        $validator = Validator::make($request->all(),[
            'current_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        if (!$validator->fails()) {
            try {
                if (!Hash::check($request->current_password, auth()->user()->password)) {
                    return response()->json([
                        'error' => true,
                        'message' => 'Current password salah, silahkan hubungi admin',
                    ], 401);
                }
                $user = User::find(auth()->user()->id);
                $user->password = Hash::make($request->password);
                $user->save();

                $this->createNotifikasi([
                    'judul' => 'Update Password',
                    'keterangan' => 'Karyawan '.$user->karyawan->nama_lengkap.' telah melakukan update password',
                    'icon' => 'fa-solid fa-user-gear',
                    'color' => 'secondary',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah password',
                ], 200);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API karyawan update password : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function listKaryawan() {
        try {
            $karyawan = Karyawan::with([
                'penanggung_jawab', 
                'penanggung_jawab.karyawan', 
                'posisi', 
                'posisi.divisi', 
                'posisi.divisi', 
                'posisi_level',
                'bank',
                'cutis',
                'kehadirans',
                'lemburs',
                'lemburs.lembur_details',
                'mutasis',
                'penggajians',
                'pinjamans',
                'reimbursements',
                'tunjangan_karyawans',
                'tunjangan_karyawans.tunjangan',
                ])->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data karyawan',
                'data' => $karyawan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API karyawan list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showKaryawan($id) {
        try {

            $karyawan = Karyawan::with([
                'penanggung_jawab', 
                'penanggung_jawab.karyawan', 
                'posisi', 
                'posisi.divisi', 
                'posisi.divisi', 
                'posisi_level',
                'bank',
                'cutis',
                'kehadirans',
                'lemburs',
                'lemburs.lembur_details',
                'mutasis',
                'penggajians',
                'pinjamans',
                'reimbursements',
                'tunjangan_karyawans',
                'tunjangan_karyawans.tunjangan',
                ])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data karyawan',
                'data' => $karyawan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API karyawan show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showKaryawanByUser() {
        try {

            $karyawan = Karyawan::with([
                'penanggung_jawab', 
                'penanggung_jawab.karyawan', 
                'posisi', 
                'posisi.divisi', 
                'posisi.divisi', 
                'posisi_level',
                'bank',
                'cutis',
                'kehadirans',
                'lemburs',
                'lemburs.lembur_details',
                'mutasis',
                'penggajians',
                'pinjamans',
                'reimbursements',
                'tunjangan_karyawans',
                'tunjangan_karyawans.tunjangan',
            ])->find(auth()->user()->karyawan->id);
            $check_tanggung_jawab = Karyawan::where('penanggung_jawab_id', auth()->user()->id)->get();
            $karyawan['haveTanggungJawab'] = count($check_tanggung_jawab) > 0 ? true : false;

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data karyawan',
                'data' => $karyawan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API karyawan show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function listInformasi(Request $param) {
        try {
            $informasi = Informasi::orderBy('id', 'DESC');

            if (isset($param->limit) && $param->limit > 0) {
                $informasi = $informasi->limit($param->limit);
            }
            $informasi = $informasi->get();

            return response()->json([
                    'error' => false,
                'message' => 'Berhasil mendapatkan list data informasi',
                'data' => $informasi,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API informasi list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showInformasi($id) {
        try {

            $informasi = Informasi::find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data informasi',
                'data' => $informasi,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API informasi show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function listKehadiran() {
        try {
            $kehadiran = Kehadiran::with(['karyawan', 'kehadiran_details'])->where('karyawan_id', auth()->user()->karyawan->id)->orderBy('id', 'DESC')->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data kehadiran karyawan',
                'data' => $kehadiran,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API kehadiran karyawan list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        } 
    }

    public function dayNowKehadiran() {
        try {
            $today = date('Y-m-d H:i:s');
            $cuti = Cuti::whereDate('tgl_mulai', '<=', $today)->whereDate('tgl_selesai', '>=', $today)->whereIn('status', ['approve_admin', 'approve_lead'])->where('karyawan_id', auth()->user()->karyawan->id)->get();
            $libur = Libur::whereDate('tgl_mulai', '<=', $today)->whereDate('tgl_selesai', '>=', $today)->first();
            
            $kehadiran = Kehadiran::with(['karyawan', 'kehadiran_details'])->where('karyawan_id', auth()->user()->karyawan->id)->where('tanggal', date('Y-m-d'))->first();
            $kehadiran['haveCutiNow'] = count($cuti) > 0 ? true: false;
            $kehadiran['haveLiburNow'] = $libur;
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan data kehadiran now karyawan',
                'data' => $kehadiran,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API kehadiran karyawan now : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        } 
    }

    public function storeKehadiran(Request $request) {
        $validator = Validator::make($request->all(),[
            'time_in' => 'required',
            'radius_in' => 'required',
            'alamat_in' => 'required',
            'latlong_in' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                DB::beginTransaction();
                $kehadiran = new Kehadiran();
                $kehadiran->karyawan_id = auth()->user()->karyawan->id;

                $jam_mulai = strtotime('09:00:00'); 
                $time_in = strtotime($request->time_in);
                $selisih_detik = $time_in - $jam_mulai;
                $selisih_jam = floor($selisih_detik / 3600); 
                $selisih_menit = ($selisih_detik % 3600) / 60; 
                $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100);
                $status = $jumlah <= 0 ? 'ontime' : 'late';
                $jumlah = $jumlah <= 0 ? '00:00:00' : date('H:i:s', strtotime(str_replace('.', ':', $jumlah)));

                $kehadiran->time_in = date('H:i:s', $time_in);
                $kehadiran->radius_in = $request->radius_in;
                $kehadiran->radius_out = 0;
                $kehadiran->alamat_in = $request->alamat_in;
                $kehadiran->latlong_in = $request->latlong_in;
                $kehadiran->late_time = $jumlah;
                $kehadiran->status = $status;
                $kehadiran->tanggal = date('Y-m-d');
                $kehadiran->save();


                DB::commit();
                $this->createNotifikasi([
                    'judul' => 'Absensi Check In',
                    'keterangan' => 'Karyawan '.auth()->user()->karyawan->nama_lengkap.' telah melakukan absensi check in',
                    'icon' => 'fa-solid fa-user-clock',
                    'color' => 'success',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);
                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data kehadiran',
                    'data' => $kehadiran,
                    'url' => url('/')
                ], 200);
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API kehadiran store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function updateKehadiran(Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'time_out' => 'required',
            'radius_out' => 'required',
            'alamat_out' => 'required',
            'latlong_out' => 'required',
        ]);
        
        if (!$validator->fails()) {
            try {

                $kehadiran = Kehadiran::find($id);
                $kehadiran->time_out = date('H:i:s', strtotime($request->time_out));
                $kehadiran->radius_out = $request->radius_out;
                $kehadiran->alamat_out = $request->alamat_out;
                $kehadiran->latlong_out = $request->latlong_out;
                $kehadiran->save();

                $this->createNotifikasi([
                    'judul' => 'Absensi Check Out',
                    'keterangan' => 'Karyawan '.auth()->user()->karyawan->nama_lengkap.' telah melakukan absensi check out',
                    'icon' => 'fa-solid fa-user-clock',
                    'color' => 'secondary',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);
                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil mengubah data kehadiran',
                    'data' => $kehadiran,
                    'url' => url('/')
                ], 200);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Error API kehadiran update : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

    public function listPengajuan() {
        try {
            $pengajuan = [];
            $penanggung_jawab_id = auth()->user()->id;
            $lembur = Lembur::with(['karyawan'])->whereHas('karyawan', function ($query) use ($penanggung_jawab_id) {
                $query->where('penanggung_jawab_id', $penanggung_jawab_id);
            })->get();

            $cuti = Cuti::whereHas('karyawan', function ($query) use ($penanggung_jawab_id) {
                $query->where('penanggung_jawab_id', $penanggung_jawab_id);
            })->get();

            foreach ($lembur as $key => $value) {
                $pengajuan[] = [
                    'type' => 'Lembur',
                    'status' => $value->status,
                    'tanggal' => $value->created_at->format('Y-m-d H:i:s'),
                    'id_data' => $value->id,
                    'karyawan' => $value->karyawan,
                ];
            }

            foreach ($cuti as $key => $value) {
                $pengajuan[] = [
                    'type' => 'Cuti',
                    'status' => $value->status,
                    'tanggal' => $value->created_at->format('Y-m-d H:i:s'),
                    'id_data' => $value->id,
                    'karyawan' => $value->karyawan,
                ];
            }
            usort($pengajuan, function($a, $b) {
                $statusA = strtolower($a['status']);
                $statusB = strtolower($b['status']);
                if ($statusA === 'pending' && $statusB === 'pending') {
                    return 0;
                }

                if ($statusA === 'pending') {
                    return -1;
                } elseif ($statusB === 'pending') {
                    return 1;
                }
            
                return 0;
            });
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data pengajuan karyawan',
                'data' => $pengajuan,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API pengajuan karyawan list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        } 
    }

    public function listPenggajian(Request $param) {
        try {
            $penggajian = Penggajian::orderBy('id', 'DESC')->where('karyawan_id', auth()->user()->karyawan->id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data penggajian',
                'data' => $penggajian,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API penggajian list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function showPenggajian($id) {
        try {
            $penggajian = Penggajian::with([
                'penggajian_lemburs',
                'penggajian_reimbursements',
                'penggajian_tunjangans',
            ])->find($id);

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan detail data penggajian',
                'data' => $penggajian,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API penggajian show : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function listReportHarian($id, Request $param) {
        try {
            $kehadiran_detail = KehadiranDetail::orderBy('id', 'DESC')->where('kehadiran_id', $id)->get();

            return response()->json([
                'error' => false,
                'message' => 'Berhasil mendapatkan list data kehadiran detail',
                'data' => $kehadiran_detail,
                'url' => url('/')
            ], 200);

        } catch (\Exception $e) {
            Log::error('Error API kehadiran detail list : ' . $e->getMessage());
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan dalam mengambil data',
            ], 500);
        }
    }

    public function storeReportHarian($id, Request $request) {
        date_default_timezone_set('Asia/Makassar');
        $validator = Validator::make($request->all(),[
            'keterangan' => 'required',
            'file' => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,csv'
        ]);
        
        if (!$validator->fails()) {
            try {

                $kehadiran = Kehadiran::find($id);
                $kehadiran_detail = new KehadiranDetail();
                $kehadiran_detail->kehadiran_id = $kehadiran->id;
                $kehadiran_detail->file = uploadFile($request->file, 'kehadiran_detail');
                $kehadiran_detail->keterangan = $request->keterangan;
                $kehadiran_detail->save();
                
                $this->createNotifikasi([
                    'judul' => 'Report Kehadiran',
                    'keterangan' => 'Karyawan '.auth()->user()->karyawan->nama_lengkap.' telah menambahkan data report kehadiran baru',
                    'icon' => 'fa-regular fa-clipboard',
                    'color' => 'primary',
                    'karyawan_id' => auth()->user()->karyawan->id
                ]);

                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil menambahkan data report',
                    'data' => $kehadiran_detail,
                    'url' => url('/')
                ], 200);

            } catch (\Exception $e) {
                Log::error('Error API report store : ' . $e->getMessage());
                return response()->json([
                    'error' => true,
                    'message' => 'Terjadi kesalahan dalam menyimpan data',
                ], 500);
            }
        }
        return response()->json([
            'error' => true,
            'message' => 'Data yang diberikan tidak valid',
            'errors' => $validator->errors(),
        ], 401);
    }

}
