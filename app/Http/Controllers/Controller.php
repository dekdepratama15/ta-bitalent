<?php

namespace App\Http\Controllers;

use App\Notifikasi;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createNotifikasi($param) {
        $notifikasi = new Notifikasi();
        $notifikasi->judul = $param['judul'];
        $notifikasi->keterangan = $param['keterangan'];
        $notifikasi->icon = $param['icon'];
        $notifikasi->color = $param['color'];
        if (isset($param['karyawan_id']) && !empty($param['karyawan_id'])) {
            $notifikasi->karyawan_id = $param['karyawan_id'];
        }
        $notifikasi->save();
    }
}
