<?php

namespace App\Http\Controllers;

use DB;
use App\Bank;
use App\User;
use App\Divisi;
use App\Posisi;
use App\Karyawan;
use App\Tunjangan;
use App\PosisiLevel;
use App\TunjanganKaryawan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $karyawans  = Karyawan::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $karyawans->where('nama_lengkap', 'like', '%' . $search . '%');
        }
        $karyawans = $karyawans->paginate(10);
        return view('karyawan.index',[
            'karyawans' => $karyawans,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks         = Bank::all();
        $divisis       = Divisi::all();
        $posisis       = Posisi::all();
        $posisi_levels = PosisiLevel::all();
        $karyawans     = Karyawan::where('status', 'aktif')->get();
        $tunjangans    = Tunjangan::all();

        return view('karyawan.create', [
            'banks' => $banks,
            'divisis' => $divisis,
            'posisis' => $posisis,
            'karyawans' => $karyawans,
            'tunjangans' => $tunjangans,
            'posisi_levels' => $posisi_levels,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'password' => 'confirmed',
            'foto_profile' => 'mimes:jpg,jpeg,png'
        ]);

        try {
            DB::beginTransaction();
            $user           = new User();
            $user->name     = $request->nama_lengkap;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $user->role     = 'user';
            $user->save();

            $karyawan = new Karyawan();
            $karyawan->user_id = $user->id;
            $karyawan->posisi_id = $request->posisi;
            $karyawan->posisi_level_id = $request->posisi_level;
            $karyawan->bank_id = $request->bank;
            if ($request->penanggung_jawab != 0) {
                $karyawan->penanggung_jawab_id = $request->penanggung_jawab;
            }
            if (isset($request->foto_profile) && !empty($request->foto_profile)) {
                $karyawan->profile = uploadFile($request->foto_profile, 'karyawan');
            }
            $karyawan->nama_lengkap = $request->nama_lengkap;
            $karyawan->nama_panggilan = $request->nama_panggilan;
            $karyawan->status = 'aktif';
            $karyawan->jenis_kelamin = $request->jenis_kelamin;
            $karyawan->golongan_darah = $request->golongan_darah;
            $karyawan->agama = $request->agama;
            $karyawan->status_perkawinan = $request->status_pernikahan;
            $karyawan->status_karyawan = $request->status_karyawan;
            $karyawan->jenis_identitas = $request->tipe_identitas;
            $karyawan->tipe_penggajian = $request->tipe_penggajian;
            $karyawan->no_identitas = $request->no_identitas;
            $karyawan->no_telp = str_replace('-', '', $request->no_telp);
            $karyawan->tempat_lahir = $request->tempat_lahir;
            $karyawan->tgl_lahir = $request->tanggal_lahir;
            $karyawan->alamat = $request->alamat;
            $karyawan->tgl_bergabung = $request->tgl_masuk;
            $karyawan->tgl_selesai = $request->tgl_keluar;
            $karyawan->gaji_pokok = str_replace(',', '', $request->gaji);
            $karyawan->jumlah_cuti = $request->jumlah_cuti;
            $karyawan->no_akun_bank = $request->no_rekening;
            $karyawan->nama_akun_bank = $request->pemilik_rekening;
            $karyawan->kode_pos = $request->kode_pos;
            $karyawan->kode_karyawan = generateCodeBitalent(staticHelper()['code']['karyawan'], Karyawan::count());
            if (isset($request->is_permanent) && $request->is_permanent == 'on') {
                $karyawan->is_identitas_permanent = true;
            } else {
                $karyawan->tgl_identitas_kadaluarsa = $request->tgl_identitas_kadaluarsa;
                $karyawan->is_identitas_permanent = true;
            }
            $karyawan->save();

            if (isset($request->tunjangan) && !empty($request->tunjangan)) {
                $tunjangan_id = explode(',', $request->tunjangan);

                foreach ($tunjangan_id as $id) {
                    $tunjangan_karyawan = new TunjanganKaryawan();
                    $tunjangan_karyawan->karyawan_id = $karyawan->id;
                    $tunjangan_karyawan->tunjangan_id = $id;
                    $tunjangan_karyawan->save();
                }
            }

            session()->flash('success', 'Data Karyawan Berhasil Ditambah');

            $this->createNotifikasi([
                'judul' => 'Karyawan Baru',
                'keterangan' => 'Karyawan '. $request->nama_lengkap .' telah ditambahkan',
                'icon' => 'fa-solid fa-plus',
                'color' => 'success',
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Error karyawan store : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Karyawan');
        }
        return redirect()->route('karyawan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show(Karyawan $karyawan)
    {
        $banks         = Bank::all();
        $divisis       = Divisi::all();
        $posisis       = Posisi::all();
        $posisi_levels = PosisiLevel::all();
        $karyawan_all  = Karyawan::where('status', 'aktif')->get();
        $tunjangans    = Tunjangan::all();

        return view('karyawan.show', [
            'banks' => $banks,
            'divisis' => $divisis,
            'posisis' => $posisis,
            'karyawan' => $karyawan,
            'karyawan_all' => $karyawan_all,
            'tunjangans' => $tunjangans,
            'posisi_levels' => $posisi_levels,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function edit(Karyawan $karyawan)
    {
        $banks         = Bank::all();
        $divisis       = Divisi::all();
        $posisis       = Posisi::all();
        $posisi_levels = PosisiLevel::all();
        $karyawan_all  = Karyawan::where('status', 'aktif')->get();
        $tunjangans    = Tunjangan::all();

        return view('karyawan.edit', [
            'banks' => $banks,
            'divisis' => $divisis,
            'posisis' => $posisis,
            'karyawan' => $karyawan,
            'karyawan_all' => $karyawan_all,
            'tunjangans' => $tunjangans,
            'posisi_levels' => $posisi_levels,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Karyawan $karyawan)
    {
        $request->validate([
            'password' => 'confirmed',
            'foto_profile' => 'mimes:jpg,jpeg,png'
        ]);
        try {
            DB::beginTransaction();
            $user           = $karyawan->user;
            $user->name     = $request->nama_lengkap;
            if (isset($request->password) && !empty($request->password)) {
                $user->password = Hash::make($request->password);
            }
            $user->save();

            $karyawan->user_id = $user->id;
            $karyawan->posisi_id = $request->posisi;
            $karyawan->posisi_level_id = $request->posisi_level;
            $karyawan->bank_id = $request->bank;
            if ($request->penanggung_jawab != 0) {
                $karyawan->penanggung_jawab_id = $request->penanggung_jawab;
            }
            if (isset($request->foto_profile) && !empty($request->foto_profile)) {
                $karyawan->profile = uploadFile($request->foto_profile, 'karyawan');
            }
            $karyawan->nama_lengkap = $request->nama_lengkap;
            $karyawan->nama_panggilan = $request->nama_panggilan;
            $karyawan->jenis_kelamin = $request->jenis_kelamin;
            $karyawan->golongan_darah = $request->golongan_darah;
            $karyawan->agama = $request->agama;
            $karyawan->status_perkawinan = $request->status_pernikahan;
            $karyawan->status_karyawan = $request->status_karyawan;
            $karyawan->jenis_identitas = $request->tipe_identitas;
            $karyawan->tipe_penggajian = $request->tipe_penggajian;
            $karyawan->no_identitas = $request->no_identitas;
            $karyawan->no_telp = str_replace('-', '', $request->no_telp);
            $karyawan->tempat_lahir = $request->tempat_lahir;
            $karyawan->tgl_lahir = $request->tanggal_lahir;
            $karyawan->alamat = $request->alamat;
            $karyawan->tgl_bergabung = $request->tgl_masuk;
            $karyawan->tgl_selesai = $request->tgl_keluar;
            $karyawan->gaji_pokok = str_replace(',', '', $request->gaji);
            $karyawan->jumlah_cuti = $request->jumlah_cuti;
            $karyawan->no_akun_bank = $request->no_rekening;
            $karyawan->nama_akun_bank = $request->pemilik_rekening;
            $karyawan->kode_pos = $request->kode_pos;

            if (isset($request->is_permanent) && $request->is_permanent == 'on') {
                $karyawan->is_identitas_permanent = true;
            } else {
                $karyawan->tgl_identitas_kadaluarsa = $request->tgl_identitas_kadaluarsa;
                $karyawan->is_identitas_permanent = false;
            }
            $karyawan->save();

            if (isset($request->tunjangan) && !empty($request->tunjangan)) {
                $karyawan->tunjangan_karyawans()->delete();
                $tunjangan_id = explode(',', $request->tunjangan);

                foreach ($tunjangan_id as $id) {
                    $tunjangan_karyawan = new TunjanganKaryawan();
                    $tunjangan_karyawan->karyawan_id = $karyawan->id;
                    $tunjangan_karyawan->tunjangan_id = $id;
                    $tunjangan_karyawan->save();
                }
            }

            $this->createNotifikasi([
                'judul' => 'Karyawan Diubah',
                'keterangan' => 'Terdapat perubahan data pada Karyawan '.$request->nama_lengkap,
                'icon' => 'fa-regular fa-pen-to-square',
                'color' => 'secondary',
            ]);
            session()->flash('success', 'Data Karyawan Berhasil Diubah');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Error karyawan update : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Karyawan');
        }
        return redirect()->route('karyawan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Karyawan $karyawan)
    {
        //
    }

    public function changeStatus(Request $request, Karyawan $karyawan) {
        $karyawan->status = $request->status;
        $karyawan->save();
        $this->createNotifikasi([
            'judul' => 'Karyawan di'.$request->status.'kan',
            'keterangan' => 'Karyawan '.$karyawan->nama_lengkap.' telah di'.$request->status.'kan',
            'icon' => 'fa-solid fa-rotate',
            'color' => 'secondary',
        ]);
        return response()->json([
            'message' => $karyawan->nama_lengkap.' berhasil di'.$request->status.'kan'
        ]);
    }
}
