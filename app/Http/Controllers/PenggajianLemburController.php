<?php

namespace App\Http\Controllers;

use App\PenggajianLembur;
use Illuminate\Http\Request;

class PenggajianLemburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PenggajianLembur  $penggajianLembur
     * @return \Illuminate\Http\Response
     */
    public function show(PenggajianLembur $penggajianLembur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PenggajianLembur  $penggajianLembur
     * @return \Illuminate\Http\Response
     */
    public function edit(PenggajianLembur $penggajianLembur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PenggajianLembur  $penggajianLembur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenggajianLembur $penggajianLembur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PenggajianLembur  $penggajianLembur
     * @return \Illuminate\Http\Response
     */
    public function destroy(PenggajianLembur $penggajianLembur)
    {
        //
    }
}
