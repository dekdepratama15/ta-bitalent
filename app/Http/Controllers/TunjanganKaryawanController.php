<?php

namespace App\Http\Controllers;

use App\TunjanganKaryawan;
use Illuminate\Http\Request;

class TunjanganKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tunjangan_karyawan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function show(TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function edit(TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function destroy(TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }
}
