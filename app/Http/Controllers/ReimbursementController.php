<?php

namespace App\Http\Controllers;

use DB;
use App\Karyawan;
use App\Reimbursement;
use App\Exports\ReimbursementExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ReimbursementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $reimbursements  = Reimbursement::orderBy('id', 'DESC');
        if (isset($search) && !empty($search)) {
            $reimbursements->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $reimbursements = $reimbursements->paginate(10);
        return view('reimbursement.index',[
            'reimbursements' => $reimbursements,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        return view('reimbursement.create', [
            'karyawans' => $karyawans
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $karyawan = Karyawan::find($request->karyawan);
            $reimbursement = new Reimbursement();
            $reimbursement->karyawan_id = $request->karyawan;
            $reimbursement->status = $request->status;
            $reimbursement->total = str_replace(',', '', $request->total);
            $reimbursement->tanggal_bayar = $request->tanggal_bayar;
            $reimbursement->keterangan = $request->keterangan;
            if (isset($request->file) && !empty($request->file)) {
                $reimbursement->file = uploadFile($request->file, 'reimbursement');
            }
            $reimbursement->save();

            $this->createNotifikasi([
                'judul' => 'Rembes Karyawan Dibuat',
                'keterangan' => 'Admin membuat Rembes Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-plus',
                'color' => 'success',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Rembes Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error reimbursement generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Reimbursement');
        }
        return redirect()->route('reimbursement.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function show(Reimbursement $reimbursement)
    {
        return view('reimbursement.show',[
            'reimbursement' => $reimbursement,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function edit(Reimbursement $reimbursement)
    {
        return view('reimbursement.edit',[
            'reimbursement' => $reimbursement,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reimbursement $reimbursement)
    {
        try {
            DB::beginTransaction();
            $karyawan = $reimbursement->karyawan;
            $reimbursement->status = $request->status;
            $reimbursement->total = str_replace(',', '', $request->total);
            $reimbursement->tanggal_bayar = $request->tanggal_bayar;
            $reimbursement->keterangan = $request->keterangan;
            if (isset($request->file) && !empty($request->file)) {
                $reimbursement->file = uploadFile($request->file, 'reimbursement');
            }
            $reimbursement->save();

            $this->createNotifikasi([
                'judul' => 'Rembes Karyawan Diubah',
                'keterangan' => 'Admin mengubah Rembes Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-pen-to-square',
                'color' => 'secondary',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Rembes Berhasil Diubah');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error reimbursement generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Reimbursement');
        }
        return redirect()->route('reimbursement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reimbursement $reimbursement)
    {
        $this->createNotifikasi([
            'judul' => 'Rembes Dihapus',
            'keterangan' => 'Rembes Karyawan '.$reimbursement->karyawan->nama_lengkap.' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        Reimbursement::destroy($reimbursement->id);
        return response()->json([
            'message' => 'Data Rembes Berhasil Dihapus'
        ]);
    }

    public function changeStatus(Request $request, Reimbursement $reimbursement) {
        $reimbursement->status = $request->status;
        $reimbursement->save();
        $this->createNotifikasi([
            'judul' => 'Status Rembes Diubah',
            'keterangan' => 'Status Rembes Karyawan '.$reimbursement->karyawan->nama_lengkap.' telah diubah',
            'icon' => 'fa-solid fa-rotate',
            'color' => 'primary',
            'karyawan_id' => $reimbursement->karyawan->id
        ]);
        return response()->json([
            'message' => 'Status Rembes berhasil diubah'
        ]);
    }

    public function exportExcel(Request $request) {
        $search = $request->search;
        $reimbursements  = Reimbursement::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $reimbursements->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $reimbursements = $reimbursements->get();

        return Excel::download(new ReimbursementExport($reimbursements), 'reimbursement.xlsx');
    }
}
