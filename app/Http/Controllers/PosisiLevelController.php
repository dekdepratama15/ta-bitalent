<?php

namespace App\Http\Controllers;

use App\Karyawan;
use App\PosisiLevel;
use Illuminate\Http\Request;

class PosisiLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $posisi_levels  = PosisiLevel::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $posisi_levels->where('nama_posisi_level', 'like', '%' . $search . '%');
        }
        $posisi_levels = $posisi_levels->paginate(10);
        return view('posisi_level.index',[
            'posisi_levels' => $posisi_levels,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posisi_level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posisi_level = new PosisiLevel();
        $posisi_level->nama_posisi_level = $request->nama_posisi_level;
        $posisi_level->save();

        $this->createNotifikasi([
            'judul' => 'Posisi Level Baru',
            'keterangan' => 'Posisi Level '. $request->nama_posisi_level .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);

        session()->flash('success', 'Data Posisi Level Berhasil Ditambah');
        return redirect()->route('posisi-level.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PosisiLevel  $posisiLevel
     * @return \Illuminate\Http\Response
     */
    public function show(PosisiLevel $posisiLevel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PosisiLevel  $posisiLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(PosisiLevel $posisiLevel)
    {
        return view('posisi_level.edit', [
            'posisi_level' => $posisiLevel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PosisiLevel  $posisiLevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosisiLevel $posisiLevel)
    {
        $posisiLevel->nama_posisi_level = $request->nama_posisi_level;
        $posisiLevel->save();

        $this->createNotifikasi([
            'judul' => 'Posisi Level Diubah',
            'keterangan' => 'Terdapat perubahan data pada Posisi Level '.$request->nama_posisi_level,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Posisi Level Berhasil Diubah');
        return redirect()->route('posisi-level.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PosisiLevel  $posisiLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosisiLevel $posisiLevel)
    {
        $karyawan = Karyawan::where('posisi_level_id', $posisiLevel->id)->get();
        if (count($karyawan) == 0) {

            $this->createNotifikasi([
                'judul' => 'Posisi Level Dihapus',
                'keterangan' => 'Posisi Level '.$posisiLevel->nama_posisi_level.' telah dihapus',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
            ]);
            PosisiLevel::destroy($posisiLevel->id);
            $response = [
                'message' => 'Data Posisi Level Berhasil Dihapus'
            ];
        } else {
            $response = [
                'error' => true,
                'message' => 'Terdapat Data Karyawan didalam Posisi Level ini'
            ];
        }
        return response()->json($response);
    }
}
