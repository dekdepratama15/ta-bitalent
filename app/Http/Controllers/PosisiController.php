<?php

namespace App\Http\Controllers;

use App\Posisi;
use App\Divisi;
use App\Karyawan;
use Illuminate\Http\Request;

class PosisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $posisis  = Posisi::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $posisis->where('nama_posisi', 'like', '%' . $search . '%');
        }
        $posisis = $posisis->paginate(10);
        return view('posisi.index',[
            'posisis' => $posisis,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisis = Divisi::all();
        return view('posisi.create',[
            'divisis' => $divisis,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posisi = new Posisi();
        $posisi->nama_posisi = $request->nama_posisi;
        $posisi->divisi_id = $request->divisi;
        $posisi->save();

        $this->createNotifikasi([
            'judul' => 'Posisi Baru',
            'keterangan' => 'Posisi '. $request->nama_posisi .' telah ditambahkan pada divisi '.$posisi->divisi->nama_divisi,
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);

        session()->flash('success', 'Data Posisi Berhasil Ditambah');
        return redirect()->route('posisi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Posisi  $posisi
     * @return \Illuminate\Http\Response
     */
    public function show(Posisi $posisi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Posisi  $posisi
     * @return \Illuminate\Http\Response
     */
    public function edit(Posisi $posisi)
    {
        $divisis = Divisi::all();
        return view('posisi.edit',[
            'divisis' => $divisis,
            'posisi' => $posisi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Posisi  $posisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posisi $posisi)
    {
        $posisi->nama_posisi = $request->nama_posisi;
        $posisi->divisi_id = $request->divisi;
        $posisi->save();


        $this->createNotifikasi([
            'judul' => 'Posisi Diubah',
            'keterangan' => 'Terdapat perubahan data pada Posisi '.$request->nama_posisi,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Posisi Berhasil Diubah');
        return redirect()->route('posisi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Posisi  $posisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posisi $posisi)
    {
        $karyawan = Karyawan::where('posisi_id', $posisi->id)->get();
        if (count($karyawan) == 0) {
            $this->createNotifikasi([
                'judul' => 'Posisi Dihapus',
                'keterangan' => 'Posisi '.$posisi->nama_posisi.' telah dihapus',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
            ]);
            Posisi::destroy($posisi->id);
            $response = [
                'message' => 'Data Posisi Berhasil Dihapus'
            ];
        } else {
            $response = [
                'error' => true,
                'message' => 'Terdapat Data Karyawan didalam Posisi ini'
            ];
        }
        return response()->json($response);
    }
}
