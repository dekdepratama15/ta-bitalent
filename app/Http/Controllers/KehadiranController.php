<?php

namespace App\Http\Controllers;

use DB;
use App\Kehadiran;
use App\KehadiranDetail;
use App\Karyawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\KehadiranExport;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class KehadiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $kehadirans  = Kehadiran::orderBy('tanggal', 'DESC');
        if (isset($search) && !empty($search)) {
            $kehadirans->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $kehadirans->whereMonth('tanggal', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->month != 'all') {
            $kehadirans->whereYear('tanggal', $request->tahun);
        }
        $kehadirans = $kehadirans->paginate(10);
        return view('kehadiran.index', [
            'kehadirans' => $kehadirans,
            'search' => $search,
            'search_bulan' => $request->month ?? 'all',
            'tahun' => $request->tahun ?? 'all',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        return view('kehadiran.create', [
            'karyawans' => $karyawans
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $kehadiran = new Kehadiran();
            $karyawan = Karyawan::find($request->karyawan);
            $kehadiran->karyawan_id = $request->karyawan;
    
            $jam_mulai = strtotime('09:00:00'); 
            $time_in = strtotime($request->jam_masuk.':00');
            $time_out = strtotime($request->jam_keluar.':00');
            $selisih_detik = $time_in - $jam_mulai;
            $selisih_jam = floor($selisih_detik / 3600); 
            $selisih_menit = ($selisih_detik % 3600) / 60; 
            $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100);
            $status = $jumlah <= 0 ? 'ontime' : 'late';
            $jumlah = $jumlah <= 0 ? '00:00:00' : date('H:i:s', strtotime(str_replace('.', ':', $jumlah)));
    
            $kehadiran->time_in = date('H:i:s', $time_in);
            $kehadiran->time_out = date('H:i:s', $time_out);
            $kehadiran->radius_in = '000';
            $kehadiran->radius_out = '000';
            $kehadiran->alamat_in = $request->alamat_masuk;
            $kehadiran->alamat_out = $request->alamat_keluar;
            $kehadiran->latlong_in = $request->latlongin;
            $kehadiran->latlong_out = $request->latlongout;
            $kehadiran->late_time = $jumlah;
            $kehadiran->status = $status;
            $kehadiran->tanggal = $request->tgl;
            $kehadiran->save();

            $this->createNotifikasi([
                'judul' => 'Kehadiran Karyawan',
                'keterangan' => 'Admin menambah Kehadiran Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-solid fa-plus',
                'color' => 'success',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Kehadiran Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error kehadiran generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Kehadiran');
        }
        return redirect()->route('kehadiran.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kehadiran  $kehadiran
     * @return \Illuminate\Http\Response
     */
    public function show(Kehadiran $kehadiran)
    {
        return view('kehadiran.show', [
            'kehadiran' => $kehadiran
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kehadiran  $kehadiran
     * @return \Illuminate\Http\Response
     */
    public function edit(Kehadiran $kehadiran)
    {
        return view('kehadiran.edit', [
            'kehadiran' => $kehadiran
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kehadiran  $kehadiran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kehadiran $kehadiran)
    {
        try {
            DB::beginTransaction();
            $karyawan = $kehadiran->karyawan;
    
            $jam_mulai = strtotime('09:00:00'); 
            $time_in = strtotime($request->jam_masuk.':00');
            $time_out = strtotime($request->jam_keluar.':00');
            $selisih_detik = $time_in - $jam_mulai;
            $selisih_jam = floor($selisih_detik / 3600); 
            $selisih_menit = ($selisih_detik % 3600) / 60; 
            $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100);
            $status = $jumlah <= 0 ? 'ontime' : 'late';
            $jumlah = $jumlah <= 0 ? '00:00:00' : date('H:i:s', strtotime(str_replace('.', ':', $jumlah)));
    
            $kehadiran->time_in = date('H:i:s', $time_in);
            $kehadiran->time_out = date('H:i:s', $time_out);
            $kehadiran->radius_in = '000';
            $kehadiran->radius_out = '000';
            $kehadiran->alamat_in = $request->alamat_masuk;
            $kehadiran->alamat_out = $request->alamat_keluar;
            $kehadiran->latlong_in = $request->latlongin;
            $kehadiran->latlong_out = $request->latlongout;
            $kehadiran->late_time = $jumlah;
            $kehadiran->status = $status;
            $kehadiran->save();

            $this->createNotifikasi([
                'judul' => 'Kehadiran Karyawan Diubah',
                'keterangan' => 'Admin mengubah Kehadiran Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-pen-to-square',
                'color' => 'secondary',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Kehadiran Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error kehadiran generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Kehadiran');
        }
        return redirect()->route('kehadiran.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kehadiran  $kehadiran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kehadiran $kehadiran)
    {
        $this->createNotifikasi([
            'judul' => 'Kehadiran Dihapus',
            'keterangan' => 'Kehadiran '.$kehadiran->karyawan->nama_lengkap.' pada tanggal '.date('d M Y', strtotime($kehadiran->tanggal)).' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        $kehadiran->kehadiran_details()->delete();
        $kehadiran->delete();
        return response()->json([
            'message' => 'Data Kehadiran Berhasil Dihapus'
        ]);
    }

    public function exportExcel(Request $request) {
        $search = $request->search;
        $kehadirans  = Kehadiran::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $kehadirans->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $kehadirans->whereMonth('tanggal', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->year != 'all') {
            $kehadirans->whereYear('tanggal', $request->tahun);
        }
        $kehadirans = $kehadirans->get();

        $date = Carbon::createFromFormat('Y-m-d', $request->tahun.'-'.$request->month.'-01');
        $numberOfDays = $date->daysInMonth;

        $send = [$kehadirans->groupBy('karyawan_id'), $numberOfDays, $request->month, $request->tahun];
        return Excel::download(new KehadiranExport($send), 'kehadiran.xlsx');
    }

    public function createReport($id) {
        return view('kehadiran.create_report', [
            'id' => $id
        ]);
    }

    public function storeReport(Request $request, $id) {
        try {
            DB::beginTransaction();
            $kehadiran = Kehadiran::find($id);
            $kehadiran_detail = new KehadiranDetail();
            $kehadiran_detail->kehadiran_id = $id;
            $kehadiran_detail->keterangan = $request->kegiatan;
            $kehadiran_detail->created_at = date('Y-m-d H:i:s', strtotime($kehadiran->tanggal.' '.$request->jam_kegiatan.':00'));

            if (isset($request->upload) && !empty($request->upload)) {
                $kehadiran_detail->file = uploadFile($request->upload, 'kehadiran_detail');
            }

            $kehadiran_detail->save();

    

            $this->createNotifikasi([
                'judul' => 'Report Kehadiran Karyawan Ditambah',
                'keterangan' => 'Admin menambah Report Kehadiran Karyawan '. $kehadiran->karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-plus',
                'color' => 'success',
                'karyawan_id' => $kehadiran->karyawan->id
            ]);

            session()->flash('success', 'Data Report Kehadiran Detail Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Error kehadiran generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Report Kehadiran');
        }
        return redirect()->route('kehadiran.edit', $id);
    }

    public function destroyReport($id) {
        KehadiranDetail::destroy($id);
        return response()->json([
            'message' => 'Data Report Kehadiran Berhasil Dihapus'
        ]);
    }
}
