<?php

namespace App\Http\Controllers;

use App\KehadiranDetail;
use Illuminate\Http\Request;

class KehadiranDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KehadiranDetail  $kehadiranDetail
     * @return \Illuminate\Http\Response
     */
    public function show(KehadiranDetail $kehadiranDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KehadiranDetail  $kehadiranDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(KehadiranDetail $kehadiranDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KehadiranDetail  $kehadiranDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KehadiranDetail $kehadiranDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KehadiranDetail  $kehadiranDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(KehadiranDetail $kehadiranDetail)
    {
        //
    }
}
