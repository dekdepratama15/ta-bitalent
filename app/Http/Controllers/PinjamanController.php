<?php

namespace App\Http\Controllers;

use DB;
use App\Karyawan;
use App\Pinjaman;
use App\Exports\PinjamanExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $pinjamans  = Pinjaman::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $pinjamans->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $pinjamans = $pinjamans->paginate(10);
        return view('pinjaman.index',[
            'pinjamans' => $pinjamans,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        return view('pinjaman.create', [
            'karyawans' => $karyawans
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $karyawan = Karyawan::find($request->karyawan);
            $pinjaman = new Pinjaman();
            $pinjaman->karyawan_id = $request->karyawan;
            $pinjaman->status = $request->status;
            $pinjaman->total = str_replace(',', '', $request->total);
            $pinjaman->akan_dibayar_tanggal = $request->tanggal_bayar;
            $pinjaman->keterangan = $request->keterangan;
            $pinjaman->alasan = $request->alasan;
            if (isset($request->file) && !empty($request->file)) {
                $pinjaman->file = uploadFile($request->file, 'pinjaman');
            }
            $pinjaman->save();

            $this->createNotifikasi([
                'judul' => 'Pinjaman Karyawan Dibuat',
                'keterangan' => 'Admin mengubah Pinjaman Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-plus',
                'color' => 'success',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Pinjaman Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error pinjaman generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Pinjaman');
        }
        return redirect()->route('pinjaman.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjaman $pinjaman)
    {
        return view('pinjaman.show',[
            'pinjaman' => $pinjaman,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function edit(Pinjaman $pinjaman)
    {
        return view('pinjaman.edit',[
            'pinjaman' => $pinjaman,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pinjaman $pinjaman)
    {
        try {
            DB::beginTransaction();
            $karyawan = $pinjaman->karyawan;
            $pinjaman->status = $request->status;
            $pinjaman->total = str_replace(',', '', $request->total);
            $pinjaman->akan_dibayar_tanggal = $request->tanggal_bayar;
            $pinjaman->keterangan = $request->keterangan;
            $pinjaman->alasan = $request->alasan;
            if (isset($request->file) && !empty($request->file)) {
                $pinjaman->file = uploadFile($request->file, 'pinjaman');
            }
            $pinjaman->save();

            $this->createNotifikasi([
                'judul' => 'Pinjaman Karyawan Diubah',
                'keterangan' => 'Admin mengubah Pinjaman Karyawan '. $karyawan->nama_lengkap,
                'icon' => 'fa-regular fa-pen-to-square',
                'color' => 'secondary',
                'karyawan_id' => $karyawan->id
            ]);

            session()->flash('success', 'Data Pinjaman Berhasil Diubah');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error pinjaman generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Pinjaman');
        }
        return redirect()->route('pinjaman.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pinjaman $pinjaman)
    {
        Pinjaman::destroy($pinjaman->id);
        return response()->json([
            'message' => 'Data Pinjaman Berhasil Dihapus'
        ]);
    }

    public function changeStatus(Request $request, Pinjaman $pinjaman) {
        $pinjaman->status = $request->status;
        $pinjaman->save();
        $this->createNotifikasi([
            'judul' => 'Status Pinjaman Diubah',
            'keterangan' => 'Status Pinjaman Karyawan '.$pinjaman->karyawan->nama_lengkap.' telah diubah',
            'icon' => 'fa-solid fa-rotate',
            'color' => 'primary',
            'karyawan_id' => $pinjaman->karyawan->id
        ]);
        return response()->json([
            'message' => 'Status Pinjaman berhasil diubah'
        ]);
    }

    public function exportExcel(Request $request) {
        $search = $request->search;
        $pinjamans  = Pinjaman::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $pinjamans->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $pinjamans = $pinjamans->get();

        return Excel::download(new PinjamanExport($pinjamans), 'pinjaman.xlsx');
    }
}
