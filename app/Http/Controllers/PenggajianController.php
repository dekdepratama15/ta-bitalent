<?php

namespace App\Http\Controllers;

use DB;
use App\Lembur;
use App\Karyawan;
use App\Reimbursement;
use App\TunjanganKaryawan;
use App\Penggajian;
use App\PenggajianTunjangan;
use App\PenggajianLembur;
use App\PenggajianReimbursement;
use Illuminate\Http\Request;

class PenggajianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $penggajians  = Penggajian::orderBy('created_at', 'DESC');
        if (isset($search) && !empty($search)) {
            $penggajians->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $penggajians = $penggajians->paginate(10);
        return view('penggajian.index', [
            'penggajians' => $penggajians,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $karyawan = null;
        $currentDate = date('Y-m');
        $karyawanAll = Karyawan::whereDoesntHave('penggajians', function ($query) use ($currentDate) {
            $query->where('tanggal', 'like', $currentDate.'%');
        })->where('status', 'aktif')->get();

        if (isset($request->karyawan) && !empty($request->karyawan)) {
            $karyawan = Karyawan::find($request->karyawan);
        }

        return view('penggajian.create', [
            'karyawan' => $karyawan,
            'karyawans' => $karyawanAll,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $penggajian = new Penggajian();
            $penggajian->karyawan_id = $request->karyawan;
            $penggajian->tanggal = date('Y-m-d');
            $penggajian->gaji_pokok = $request->gaji_pokok;
            $penggajian->pemotongan_gaji = $request->pemotongan_gaji_total;
            $penggajian->lembur = $request->lembur_total;
            $penggajian->bonus = $request->bonus_total;
            $penggajian->tunjangan = $request->tunjangan_karyawan_total;
            $penggajian->reimbursement = $request->reimbursement_total;
            $penggajian->total = $request->grand_total;
            $penggajian->alasan_potong = $request->alasan;
            $penggajian->save();

            if (isset($request->lembur_id) && count($request->lembur_id) > 0) {
                foreach ($request->lembur_id as $key => $value) {
                    $lembur = Lembur::find($value);
                    $penggajian_lembur = new PenggajianLembur();
                    $penggajian_lembur->penggajian_id = $penggajian->id;
                    $penggajian_lembur->tanggal = date('Y-m-d', strtotime($lembur->jam_mulai));
                    $penggajian_lembur->keterangan = $lembur->keterangan;
                    $penggajian_lembur->jam_mulai = $lembur->jam_mulai;
                    $penggajian_lembur->jam_selesai = $lembur->jam_selesai;
                    $penggajian_lembur->jumlah_jam = $lembur->jumlah_jam;
                    $penggajian_lembur->status = $lembur->status;
                    $penggajian_lembur->bayar = $lembur->bayar;
                    $penggajian_lembur->total = $lembur->total;
                    $penggajian_lembur->save();
                }
            }

            if (isset($request->reimbursement_id) && count($request->reimbursement_id) > 0) {
                foreach ($request->reimbursement_id as $key => $value) {
                    $rembes = Reimbursement::find($value);
                    $penggajian_rembes = new PenggajianReimbursement();
                    $penggajian_rembes->penggajian_id = $penggajian->id;
                    $penggajian_rembes->keterangan = $rembes->keterangan;
                    $penggajian_rembes->file = $rembes->file;
                    $penggajian_rembes->total = $rembes->total;
                    $penggajian_rembes->status = $rembes->status;
                    $penggajian_rembes->save();
                }
            }

            if (isset($request->tunjangan_karyawan_id) && count($request->tunjangan_karyawan_id) > 0) {
                foreach ($request->tunjangan_karyawan_id as $key => $value) {
                    $tunjangan_karyawan = TunjanganKaryawan::find($value);

                    if ($tunjangan_karyawan->tunjangan->diambil_dari == 'pendapatan') {
                        $jumlah_tunjangan = $tunjangan_karyawan->tunjangan->jumlah;
                    } else {
                        $jumlah_tunjangan = ($tunjangan_karyawan->karyawan->gaji_pokok * $tunjangan_karyawan->tunjangan->persentase) / 100;
                    }
                    if ($tunjangan_karyawan->tunjangan->jenis_tunjangan == 'potongan_pajak') {
                        $jumlah_tunjangan = $jumlah_tunjangan * -1;
                    }

                    $penggajian_tunjangan = new PenggajianTunjangan();
                    $penggajian_tunjangan->penggajian_id = $penggajian->id;
                    $penggajian_tunjangan->nama_tunjangan = $tunjangan_karyawan->tunjangan->nama_tunjangan;
                    $penggajian_tunjangan->gaji_pokok = $tunjangan_karyawan->karyawan->gaji_pokok;
                    $penggajian_tunjangan->total = $jumlah_tunjangan;
                    $penggajian_tunjangan->type = $tunjangan_karyawan->tunjangan->type;
                    $penggajian_tunjangan->jenis_tunjangan = $tunjangan_karyawan->tunjangan->jenis_tunjangan;
                    $penggajian_tunjangan->diambil_dari = $tunjangan_karyawan->tunjangan->diambil_dari;
                    $penggajian_tunjangan->tanggal_pemberian = $tunjangan_karyawan->tunjangan->tanggal_pemberian;
                    $penggajian_tunjangan->persentase = empty($tunjangan_karyawan->tunjangan->persentase) ? 0 : $tunjangan_karyawan->tunjangan->persentase;
                    $penggajian_tunjangan->jumlah = empty($tunjangan_karyawan->tunjangan->jumlah) ? 0 : $tunjangan_karyawan->tunjangan->jumlah;
                    $penggajian_tunjangan->save();
                }
            }

            $this->createNotifikasi([
                'judul' => 'Penggajian Karyawan',
                'keterangan' => 'Penggajian Karyawan '. $penggajian->karyawan->nama_lengkap .' telah digenerate',
                'icon' => 'fa-solid fa-dollar-sign',
                'color' => 'success',
                'karyawan_id' => $penggajian->karyawan->id
            ]);

            session()->flash('success', 'Data Penggajian Berhasil Dibuat');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('Error penggajian generate : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Penggajian');
        }
        return redirect()->route('penggajian.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penggajian  $penggajian
     * @return \Illuminate\Http\Response
     */
    public function show(Penggajian $penggajian)
    {
        return view('penggajian.show', [
            'penggajian' => $penggajian,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penggajian  $penggajian
     * @return \Illuminate\Http\Response
     */
    public function edit(Penggajian $penggajian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penggajian  $penggajian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penggajian $penggajian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penggajian  $penggajian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penggajian $penggajian)
    {
        $this->createNotifikasi([
            'judul' => 'Penggajian Dihapus',
            'keterangan' => 'Penggajian '.$penggajian->karyawan->nama_lengkap.' pada tanggal '.date('d M Y', strtotime($penggajian->tanggal)).' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        $penggajian->penggajian_lemburs()->delete();
        $penggajian->penggajian_reimbursements()->delete();
        $penggajian->penggajian_tunjangans()->delete();
        $penggajian->delete();
        return response()->json([
            'message' => 'Data Kehadiran Berhasil Dihapus'
        ]);
    }
}
