<?php

namespace App\Http\Controllers;

use DB;
use App\Karyawan;
use App\Lembur;
use App\Exports\LemburExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class LemburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $lemburs  = Lembur::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $lemburs->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $lemburs->whereMonth('jam_mulai', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->year != 'all') {
            $lemburs->whereYear('jam_mulai', $request->tahun);
        }
        $lemburs = $lemburs->paginate(10);
        return view('lembur.index',[
            'lemburs' => $lemburs,
            'search' => $search,
            'search_bulan' => $request->month ?? 'all',
            'tahun' => $request->tahun ?? 'all',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        return view('lembur.create', [
            'karyawans' => $karyawans
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jam_mulai = strtotime($request->tgl_mulai); 
        $jam_selesai = strtotime($request->tgl_selesai);
        
        $selisih_detik = $jam_selesai - $jam_mulai;
        $selisih_jam = floor($selisih_detik / 3600); 
        $selisih_menit = ($selisih_detik % 3600) / 60; 
        $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100) * 10;
        
        $lembur = new Lembur();
        $lembur->karyawan_id = $request->karyawan;
        $lembur->jam_mulai = $request->tgl_mulai;
        $lembur->jam_selesai = $request->tgl_selesai;
        $lembur->bayar = str_replace(',', '', $request->bayar);
        $lembur->total = $jumlah * str_replace(',', '', $request->bayar);
        $lembur->status = $request->status;
        $lembur->keterangan = $request->keterangan;
        $lembur->jumlah_jam = $jumlah;
        $lembur->save();

        $this->createNotifikasi([
            'judul' => 'Lembur Diubah',
            'keterangan' => 'Terdapat perubahan data pada Lembur Karyawan '.$lembur->karyawan->nama_lengkap,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);
        session()->flash('success', 'Data Lembur Berhasil Diubah');
        return redirect()->route('lembur.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function show(Lembur $lembur)
    {
        return view('lembur.show', [
            'lembur' => $lembur
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function edit(Lembur $lembur)
    {
        return view('lembur.edit', [
            'lembur' => $lembur
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lembur $lembur)
    {
        $jam_mulai = strtotime($request->tgl_mulai); 
        $jam_selesai = strtotime($request->tgl_selesai);
        
        $selisih_detik = $jam_selesai - $jam_mulai;
        $selisih_jam = floor($selisih_detik / 3600); 
        $selisih_menit = ($selisih_detik % 3600) / 60; 
        $jumlah = floatval(intval($selisih_jam.$selisih_menit)/100);
        
        $lembur->jam_mulai = $request->tgl_mulai;
        $lembur->jam_selesai = $request->tgl_selesai;
        $lembur->bayar = str_replace(',', '', $request->bayar);
        $lembur->total = str_replace(',', '', $request->total);
        $lembur->status = $request->status;
        $lembur->keterangan = $request->keterangan;
        $lembur->jumlah_jam = $jumlah;
        $lembur->save();

        $this->createNotifikasi([
            'judul' => 'Lembur Diubah',
            'keterangan' => 'Terdapat perubahan data pada Lembur Karyawan '.$lembur->karyawan->nama_lengkap,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);
        session()->flash('success', 'Data Lembur Berhasil Diubah');
        return redirect()->route('lembur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lembur $lembur)
    {
        $this->createNotifikasi([
            'judul' => 'Lembur Dihapus',
            'keterangan' => 'Lembur Karyawan '.$lembur->karyawan->nama_lengkap.' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        Lembur::destroy($lembur->id);
        return response()->json([
            'message' => 'Data Lembur Berhasil Dihapus'
        ]);
    }

    public function exportExcel(Request $request) {
        
        $search = $request->search;
        $lemburs  = Lembur::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $lemburs->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $lemburs->whereMonth('jam_mulai', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->year != 'all') {
            $lemburs->whereYear('jam_mulai', $request->tahun);
        }
        $lemburs = $lemburs->get();

        return Excel::download(new LemburExport($lemburs), 'lembur.xlsx');
    }
}
