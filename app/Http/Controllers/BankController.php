<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $banks  = Bank::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $banks->where('nama_bank', 'like', '%' . $search . '%');
        }
        $banks = $banks->paginate(10);
        // dd($banks);
        return view('bank.index',[
            'banks' => $banks,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank = new Bank();
        $bank->nama_bank = $request->nama_bank;
        $bank->save();

        $this->createNotifikasi([
            'judul' => 'Bank Baru',
            'keterangan' => 'Bank '. $request->nama_bank .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);

        session()->flash('success', 'Data Bank Berhasil Ditambah');
        return redirect()->route('bank.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        return view('bank.edit', [
            'bank' => $bank
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        $bank->nama_bank = $request->nama_bank;
        $bank->save();

        $this->createNotifikasi([
            'judul' => 'Bank Diubah',
            'keterangan' => 'Terdapat perubahan data pada Bank '.$request->nama_bank,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Bank Berhasil Diubah');
        return redirect()->route('bank.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $this->createNotifikasi([
            'judul' => 'Bank Dihapus',
            'keterangan' => 'Bank '.$bank->nama_bank.' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        
        Bank::destroy($bank->id);
        return response()->json([
            'message' => 'Data Bank Berhasil Dihapus'
        ]);
    }
}
