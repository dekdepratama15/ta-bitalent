<?php

namespace App\Http\Controllers;

use App\LemburDetail;
use Illuminate\Http\Request;

class LemburDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LemburDetail  $lemburDetail
     * @return \Illuminate\Http\Response
     */
    public function show(LemburDetail $lemburDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LemburDetail  $lemburDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(LemburDetail $lemburDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LemburDetail  $lemburDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LemburDetail $lemburDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LemburDetail  $lemburDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(LemburDetail $lemburDetail)
    {
        //
    }
}
