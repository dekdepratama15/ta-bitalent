<?php

namespace App\Http\Controllers;

use App\Libur;
use Illuminate\Http\Request;

class LiburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $liburs  = Libur::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $liburs->where('nama_libur', 'like', '%' . $search . '%');
        }
        $liburs = $liburs->paginate(10);
        return view('libur.index',[
            'liburs' => $liburs,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libur = new Libur();
        $libur->nama_libur = $request->nama_libur;
        $libur->tgl_mulai = $request->tgl_mulai;
        $libur->tgl_selesai = $request->tgl_selesai;
        $libur->keterangan = $request->keterangan;
        $libur->save();

        $this->createNotifikasi([
            'judul' => 'Libur Baru',
            'keterangan' => 'Libur '. $request->nama_libur .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);

        session()->flash('success', 'Data Libur Berhasil Ditambah');
        return redirect()->route('libur.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libur  $libur
     * @return \Illuminate\Http\Response
     */
    public function show(Libur $libur)
    {
        return view('libur.show', [
            'libur' => $libur
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libur  $libur
     * @return \Illuminate\Http\Response
     */
    public function edit(Libur $libur)
    {
        return view('libur.edit', [
            'libur' => $libur
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libur  $libur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libur $libur)
    {
        $libur->nama_libur = $request->nama_libur;
        $libur->tgl_mulai = $request->tgl_mulai;
        $libur->tgl_selesai = $request->tgl_selesai;
        $libur->keterangan = $request->keterangan;
        $libur->save();


        $this->createNotifikasi([
            'judul' => 'Libur Diubah',
            'keterangan' => 'Terdapat perubahan data pada Libur '.$request->nama_libur,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Libur Berhasil Diubah');
        return redirect()->route('libur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libur  $libur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Libur $libur)
    {
        Libur::destroy($libur->id);
        return response()->json([
            'message' => 'Data Libur Berhasil Dihapus'
        ]);
    }
}
