<?php

namespace App\Http\Controllers;
use App\Cuti;
use App\Lembur;
use App\Karyawan;
use App\Pinjaman;
use App\Kehadiran;
use App\Penggajian;
use App\Notifikasi;
use App\Reimbursement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $karyawan = Karyawan::all();
        $cuti = Cuti::orderBy('id', 'DESC')->limit(5)->get();
        $lembur = Lembur::where('status', 'approve_lead')->get();
        $notifikasi = Notifikasi::orderBy('id', 'DESC')->limit(8)->get();
        $penggajian = Penggajian::where('tanggal', date('Y-m-d'))->get();
        $reimbursement = Reimbursement::where('status', 'pending')->get();
        $pinjaman = Pinjaman::where('status', '!=', 'done')->where('status', '!=', 'cancel_admin')->get();
        $total_pinjaman = 0;
        foreach ($pinjaman as $key => $value) {
            $total_pinjaman += $value->total;
        }
        if (isset($request->month) && !empty($request->month)) {
            $month = date('Y').'-'.$request->month;
            $month_select = $request->month;
        } else {
            $month = date('Y-m');
            $month_select = date('m');
        }
        $kehadiran = Kehadiran::where('tanggal', 'like', $month.'%')->get();
        $data_kehadiran = [];
        $tanggal_kehadiran = [];
        $list_kehadiran = $kehadiran->groupBy('tanggal');
        if (count($list_kehadiran) > 0) {
            foreach ($list_kehadiran as $key => $value) {
                $data_kehadiran[] = count($value); 
                $tanggal_kehadiran[] = $key; 
            }
        }
        return view('dashboard', [
            'cuti' => $cuti,
            'lembur' => $lembur,
            'karyawan' => $karyawan,
            'pinjaman' => $pinjaman,
            'penggajian' => $penggajian,
            'notifikasi' => $notifikasi,
            'month_select' => $month_select,
            'reimbursement' => $reimbursement,
            'total_pinjaman' => $total_pinjaman,
            'data_kehadiran' => json_encode($data_kehadiran),
            'tanggal_kehadiran' => json_encode($tanggal_kehadiran),
        ]);
    }

    function notifikasi() {
        $notifikasi = Notifikasi::orderBy('id', 'DESC')->get();
        return view('notifikasi', [
            'notifikasi' => $notifikasi,
        ]);
    }
}
