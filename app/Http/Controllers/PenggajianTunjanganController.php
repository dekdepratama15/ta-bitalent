<?php

namespace App\Http\Controllers;

use App\PenggajianTunjangan;
use Illuminate\Http\Request;

class PenggajianTunjanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PenggajianTunjangan  $penggajianTunjangan
     * @return \Illuminate\Http\Response
     */
    public function show(PenggajianTunjangan $penggajianTunjangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PenggajianTunjangan  $penggajianTunjangan
     * @return \Illuminate\Http\Response
     */
    public function edit(PenggajianTunjangan $penggajianTunjangan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PenggajianTunjangan  $penggajianTunjangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenggajianTunjangan $penggajianTunjangan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PenggajianTunjangan  $penggajianTunjangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PenggajianTunjangan $penggajianTunjangan)
    {
        //
    }
}
