<?php

namespace App\Http\Controllers;

use App\Tunjangan;
use Illuminate\Http\Request;

class TunjanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $tunjangans  = Tunjangan::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $tunjangans->where('nama_tunjangan', 'like', '%' . $search . '%');
        }
        $tunjangans = $tunjangans->paginate(10);
        return view('tunjangan.index',[
            'tunjangans' => $tunjangans,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tunjangan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tunjangan = new Tunjangan();
        $tunjangan->nama_tunjangan = $request->nama_tunjangan;
        $tunjangan->type = $request->tipe_tunjangan;
        $tunjangan->jenis_tunjangan = $request->jenis;
        $tunjangan->diambil_dari = $request->diambil_dari;
        $tunjangan->tanggal_pemberian = $request->tipe_tunjangan == 'sekali' ? $request->tanggal_pemberian_sekali : date('Y-m-').$request->tanggal_pemberian_berkala;
        if ($request->diambil_dari == 'gaji') {
            $tunjangan->persentase = $request->persentase;
        } else {
            $tunjangan->jumlah = str_replace(',', '', $request->jumlah);
        }
        $tunjangan->save();

        $this->createNotifikasi([
            'judul' => 'Tunjangan Baru',
            'keterangan' => 'Tunjangan '. $request->nama_tunjangan .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);

        session()->flash('success', 'Data Tunjangan Berhasil Ditambah');
        return redirect()->route('tunjangan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function show(Tunjangan $tunjangan)
    {
        return view('tunjangan.show', [
            'tunjangan' => $tunjangan
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function edit(Tunjangan $tunjangan)
    {
        return view('tunjangan.edit', [
            'tunjangan' => $tunjangan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tunjangan $tunjangan)
    {
        $tunjangan->nama_tunjangan = $request->nama_tunjangan;
        $tunjangan->type = $request->tipe_tunjangan;
        $tunjangan->jenis_tunjangan = $request->jenis;
        $tunjangan->diambil_dari = $request->diambil_dari;
        $tunjangan->tanggal_pemberian = $request->tipe_tunjangan == 'sekali' ? $request->tanggal_pemberian_sekali : date('Y-m-').$request->tanggal_pemberian_berkala;
        if ($request->diambil_dari == 'gaji') {
            $tunjangan->persentase = str_replace(',', '', $request->persentase);
        } else {
            $tunjangan->jumlah = str_replace(',', '', $request->jumlah);
        }
        $tunjangan->save();

        $this->createNotifikasi([
            'judul' => 'Tunjangan Diubah',
            'keterangan' => 'Terdapat perubahan data pada Tunjangan '.$request->nama_tunjangan,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Tunjangan Berhasil Diubah');
        return redirect()->route('tunjangan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tunjangan $tunjangan)
    {
        $this->createNotifikasi([
            'judul' => 'Tunjangan Dihapus',
            'keterangan' => 'Tunjangan '.$tunjangan->nama_tunjangan.' telah dihapus',
            'icon' => 'fa-solid fa-trash-can',
            'color' => 'error',
        ]);
        Tunjangan::destroy($tunjangan->id);
        return response()->json([
            'message' => 'Data Tunjangan Berhasil Dihapus'
        ]);
    }
}
