<?php

namespace App\Http\Controllers;

use DB;
use App\Mutasi;
use App\Divisi;
use App\Posisi;
use App\Karyawan;
use App\PosisiLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MutasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $mutasis  = Mutasi::orderBy('id', 'DESC');
        if (isset($search) && !empty($search)) {
            $mutasis->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        $mutasis = $mutasis->paginate(10);
        return view('mutasi.index',[
            'mutasis' => $mutasis,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        $divisis = Divisi::all();
        $posisis = Posisi::all();
        $posisi_levels = PosisiLevel::all();
        return view('mutasi.create',[
            'karyawans' => $karyawans,
            'divisis' => $divisis,
            'posisis' => $posisis,
            'posisi_levels' => $posisi_levels,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'surat_mutasi' => 'required|mimes:pdf'
        ]);

        try {
            DB::beginTransaction();
            $karyawan = Karyawan::find($request->karyawan);
            $posisi_awal = $karyawan->posisi_level->nama_posisi_level. ' '.$karyawan->posisi->nama_posisi;
            $posisi_sebelum_id = $karyawan->posisi->id;
            $posisi_level_sebelum_id = $karyawan->posisi_level->id;

            $karyawan->posisi_id       = $request->posisi;
            $karyawan->posisi_level_id = $request->posisi_level;
            $karyawan->save();

            $posisi = Posisi::find($request->posisi);
            $posisi_level = PosisiLevel::find($request->posisi_level);

            $posisi_sekarang = $posisi_level->nama_posisi_level. ' '.$posisi->nama_posisi;
            $mutasi = new Mutasi();
            $mutasi->karyawan_id = $karyawan->id;
            $mutasi->type = $request->tipe;
            $mutasi->posisi_awal = $posisi_awal;
            $mutasi->posisi_sekarang = $posisi_sekarang;
            $mutasi->posisi_sebelum_id = $posisi_sebelum_id;
            $mutasi->posisi_level_sebelum_id = $posisi_level_sebelum_id;
            $mutasi->alasan = $request->alasan;
            $mutasi->file = uploadFile($request->surat_mutasi, 'mutasi');
            $mutasi->save();

            $this->createNotifikasi([
                'judul' => 'Mutasi Karyawan',
                'keterangan' => 'Karyawan '. $karyawan->nama_lengkap .' telah dimutasi dari '.$posisi_awal. ' menjadi '.$posisi_sekarang,
                'icon' => 'fa-solid fa-up-down-left-right',
                'color' => 'primary',
            ]);
            session()->flash('success', 'Data Mutasi Berhasil Ditambah');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Error mutasi store : ' . $e->getMessage());
            session()->flash('error', 'Terjadi kesalahan saat menyimpan Data Mutasi');
        }
        return redirect()->route('mutasi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mutasi  $mutasi
     * @return \Illuminate\Http\Response
     */
    public function show(Mutasi $mutasi)
    {
        return view('mutasi.show',[
            'mutasi' => $mutasi,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mutasi  $mutasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Mutasi $mutasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mutasi  $mutasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mutasi $mutasi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mutasi  $mutasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mutasi $mutasi)
    {
        //
    }
}
