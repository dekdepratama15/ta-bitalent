<?php

namespace App\Http\Controllers;

use DB;
use App\Karyawan;
use App\Cuti;
use App\Exports\CutiExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use Carbon\Carbon;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $cutis  = Cuti::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $cutis->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $cutis->whereMonth('tgl_mulai', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->year != 'all') {
            $cutis->whereYear('tgl_mulai', $request->tahun);
        }
        $cutis = $cutis->paginate(10);
        return view('cuti.index',[
            'cutis' => $cutis,
            'search' => $search,
            'search_bulan' => $request->month ?? 'all',
            'tahun' => $request->tahun ?? 'all',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawans = Karyawan::where('status', 'aktif')->get();
        return view('cuti.create', [
            'karyawans' => $karyawans
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cuti = new Cuti();
        $cuti->karyawan_id = $request->karyawan;
        $cuti->keterangan = $request->keterangan;
        $cuti->tgl_mulai = $request->tgl_mulai;
        $cuti->tgl_selesai = $request->tgl_selesai;

        $tgl_mulai = Carbon::createFromDate($request->tgl_mulai); 
        $tgl_selesai = Carbon::createFromDate($request->tgl_selesai);
        
        $selisih = $tgl_mulai->diffInDays($tgl_selesai) + 1;
        $cuti->jumlah_cuti = $selisih;
        $cuti->status = $request->status;
        $cuti->save();
        $this->createNotifikasi([
            'judul' => 'Cuti Karyawan Dibuat',
            'keterangan' => 'Admin membuat Cuti Karyawan '. $cuti->karyawan->nama_lengkap,
            'icon' => 'fa-regular fa-plus',
            'color' => 'success',
            'karyawan_id' => $cuti->karyawan->id
        ]);

        session()->flash('success', 'Data Cuti Berhasil Dibuat');
        return redirect()->route('cuti.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function show(Cuti $cuti)
    {
        return view('cuti.show', [
            'cuti' => $cuti
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
        return view('cuti.edit',[
            'cuti' => $cuti,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        $cuti->keterangan = $request->keterangan;
        $cuti->tgl_mulai = $request->tgl_mulai;
        $cuti->tgl_selesai = $request->tgl_selesai;

        $tgl_mulai = Carbon::createFromDate($request->tgl_mulai); 
        $tgl_selesai = Carbon::createFromDate($request->tgl_selesai);
        
        $selisih = $tgl_mulai->diffInDays($tgl_selesai) + 1;
        $cuti->jumlah_cuti = $selisih;
        $cuti->status = $request->status;
        $cuti->save();
        $this->createNotifikasi([
            'judul' => 'Cuti Karyawan Diubah',
            'keterangan' => 'Admin mengubah Cuti Karyawan '. $cuti->karyawan->nama_lengkap,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
            'karyawan_id' => $cuti->karyawan->id
        ]);

        session()->flash('success', 'Data Cuti Berhasil Diubah');
        return redirect()->route('cuti.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        Cuti::destroy($cuti->id);
        return response()->json([
            'message' => 'Data Cuti Berhasil Dihapus'
        ]);
    }

    public function changeStatus(Request $request, Cuti $cuti) {
        $cuti->status = $request->status;
        $cuti->save();

        $this->createNotifikasi([
            'judul' => 'Status Cuti Diubah',
            'keterangan' => 'Status Cuti '.$cuti->karyawan->nama_lengkap.' berhasil diubah',
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
            'karyawan_id' => $cuti->karyawan->id
        ]);
        return response()->json([
            'message' => 'Status Cuti berhasil diubah'
        ]);
    }

    public function exportExcel(Request $request) {
        $search = $request->search;
        $cutis  = Cuti::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $cutis->whereHas('karyawan', function ($query) use ($search) {
                $query->where('nama_lengkap', 'like', '%' . $search . '%');
            });
        }
        if (isset($request->month) && !empty($request->month) && $request->month != 'all') {
            $cutis->whereMonth('tgl_mulai', $request->month);
        }
        if (isset($request->tahun) && !empty($request->tahun) && $request->year != 'all') {
            $cutis->whereYear('tgl_mulai', $request->tahun);
        }
        $cutis = $cutis->get();

        return Excel::download(new CutiExport($cutis), 'cuti.xlsx');
    }
}
