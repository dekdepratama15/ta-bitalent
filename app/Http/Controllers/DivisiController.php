<?php

namespace App\Http\Controllers;

use App\Divisi;
use App\Posisi;
use Illuminate\Http\Request;

class DivisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $divisis  = Divisi::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $divisis->where('nama_divisi', 'like', '%' . $search . '%');
        }
        $divisis = $divisis->paginate(10);
        // dd($divisis);
        return view('divisi.index',[
            'divisis' => $divisis,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('divisi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $divisi = new Divisi();
        $divisi->nama_divisi = $request->nama_divisi;
        $divisi->save();

        $this->createNotifikasi([
            'judul' => 'Divisi Baru',
            'keterangan' => 'Divisi '. $request->nama_divisi .' telah ditambahkan',
            'icon' => 'fa-solid fa-plus',
            'color' => 'success',
        ]);
        session()->flash('success', 'Data Divisi Berhasil Ditambah');
        return redirect()->route('divisi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function show(Divisi $divisi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function edit(Divisi $divisi)
    {
        return view('divisi.edit', [
            'divisi' => $divisi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Divisi $divisi)
    {
        $divisi->nama_divisi = $request->nama_divisi;
        $divisi->save();

        $this->createNotifikasi([
            'judul' => 'Divisi Diubah',
            'keterangan' => 'Terdapat perubahan data pada Divisi '.$request->nama_divisi,
            'icon' => 'fa-regular fa-pen-to-square',
            'color' => 'secondary',
        ]);

        session()->flash('success', 'Data Divisi Berhasil Diubah');
        return redirect()->route('divisi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Divisi $divisi)
    {
        $posisi = Posisi::where('divisi_id', $divisi->id)->get();
        if (count($posisi) == 0) {
            $this->createNotifikasi([
                'judul' => 'Divisi Dihapus',
                'keterangan' => 'Divisi '.$divisi->nama_divisi.' telah dihapus',
                'icon' => 'fa-solid fa-trash-can',
                'color' => 'error',
            ]);
            Divisi::destroy($divisi->id);
            $response = [
                'message' => 'Data Divisi Berhasil Dihapus'
            ];
        } else {
            $response = [
                'error' => true,
                'message' => 'Terdapat Data Posisi didalam Divisi ini'
            ];
        }


        return response()->json($response);
    }
}
