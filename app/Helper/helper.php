<?php

use Illuminate\Support\Facades\Auth;


if (!function_exists('generateCodeBitalent')) {
    function generateCodeBitalent($code, $count) {
        return 'BIT-'.$code.'-'.str_pad(($count + 1), 3, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('generateUIAvatar')) {
    function generateUIAvatar($name) {
        return 'https://ui-avatars.com/api/?name='.urlencode($name);
    }
}

if (!function_exists('uploadFile')) {
    function uploadFile($file, $folder)
    {
        $extension  = $file->getClientOriginalExtension();
        $filename = rand(0, 999) . now()->timestamp . '.' . $extension;
        $file->move('upload/'.$folder, $filename);
        return 'upload/'.$folder.'/'.$filename;
    }
}

if (!function_exists('getYearOption')) {
    function getYearOption()
    {
        $year = date('Y');
        $years = [];
        for ($i= $year - 3; $i < $year; $i++) { 
            $years[] = $i;
        }

        for ($i=$year; $i < $year + 3; $i++) { 
            $years[] = $i;
        }

        return $years;
    }
}

if (!function_exists('staticHelper')) {
    function staticHelper() {
        $data = [
            'code' => [
                'karyawan' => 'KRW',
            ],
            'status' => [
                'aktif' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Aktif',
                ],
                'nonaktif' => [
                    'color' => '',
                    'class' => 'warning',
                    'text'  => 'Nonaktif',
                ],
                'pending' => [
                    'color' => '',
                    'class' => 'warning',
                    'text'  => 'Belum disetujui',
                ],
                'approve_lead' => [
                    'color' => '',
                    'class' => 'secondary',
                    'text'  => 'Disetujui penanggung jawab',
                ],
                'approve_admin' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Disetujui admin',
                ],
                'cancel_admin' => [
                    'color' => '',
                    'class' => 'error',
                    'text'  => 'Ditolak',
                ],
                'ontime' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Tepat Waktu',
                ],
                'late' => [
                    'color' => '',
                    'class' => 'error',
                    'text'  => 'Telat',
                ],
                'cancel' => [
                    'color' => '',
                    'class' => 'error',
                    'text'  => 'Ditolak',
                ],
                'terbayar' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Terbayar',
                ],
                'done' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Selesai',
                ],
            ],
            'status_approve' => [
                'approve_lead' => [
                    'color' => '',
                    'class' => 'secondary',
                    'text'  => 'Disetujui penanggung jawab',
                ],
                'approve_admin' => [
                    'color' => '',
                    'class' => 'primary',
                    'text'  => 'Disetujui admin',
                ],
            ],
            'status_select' => [
                'pending' => [
                    'color' => '',
                    'class' => 'warning',
                    'text'  => 'Belum disetujui',
                ],
                'approve_lead' => [
                    'color' => '',
                    'class' => 'secondary',
                    'text'  => 'Disetujui penanggung jawab',
                ],
                'approve_admin' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Disetujui admin',
                ],
                'cancel' => [
                    'color' => '',
                    'class' => 'error',
                    'text'  => 'Ditolak',
                ],
            ],
            'type_mutasi' => [
                'peningkatan' => [
                    'color' => '',
                    'class' => 'success',
                    'text'  => 'Peningkatan',
                ],
                'penurunan' => [
                    'color' => '',
                    'class' => 'warning',
                    'text'  => 'Penurunan',
                ],
                'perpindahan' => [
                    'color' => '',
                    'class' => 'secondary',
                    'text'  => 'Perpindahan',
                ],
            ],
            'tipe_tunjangan' => [
                'sekali' => 'Sekali',
                'berkala' => 'Berkala'
            ],
            'jenis_tunjangan' => [
                'insentif' => 'Insentif',
                'potongan_pajak' => 'Potongan Pajak',
                'manfaat' => 'Manfaat'
            ],
            'diambil_dari' => [
                'gaji' => 'Gaji',
                'pendapatan' => 'Pendapatan Perusahaan',
            ],
            'jenis_kelamin' => [
                'L' => 'Laki Laki',
                'P' => 'Perempuan',
            ],
            'status_pernikahan' => [
                'belum_kawin' => 'Belum Kawin',
                'kawin' => 'Kawin',
                'janda_duda' => 'Janda / Duda',
            ],
            'golongan_darah' => [
                'A' => 'A',
                'B' => 'B',
                'AB' => 'AB',
                'O' => 'O',
            ],
            'agama' => [
                'Islam' => 'Islam',
                'Kristen' => 'Kristen',
                'Katolik' => 'Katolik',
                'Hindu' => 'Hindu',
                'Khonghucu' => 'Khonghucu',
                'Lainnya' => 'Lainnya',
            ],
            'tipe_identitas' => [
                'KTP' => 'KTP',
                'SIM' => 'SIM',
                'Passport' => 'Passport',
            ],
            'status_karyawan' => [
                'Kontrak' => 'Kontrak',
                'Tetap' => 'Tetap',
                'Probation' => 'Probation',
                'Magang' => 'Magang',
            ],
            'tipe_penggajian' => [
                'month' => 'per Bulan',
                'day' => 'per Hari',
                'hour' => 'per Jam',
            ],
            'select_month' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        ];
        return $data;
    }
}

?>