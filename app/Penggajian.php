<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggajian extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function penggajian_lemburs()
    {
        return $this->hasMany(PenggajianLembur::class);
    }

    public function penggajian_reimbursements()
    {
        return $this->hasMany(PenggajianReimbursement::class);
    }

    public function penggajian_tunjangans()
    {
        return $this->hasMany(PenggajianTunjangan::class);
    }
}
