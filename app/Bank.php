<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public function karyawans()
    {
        return $this->hasMany(Karyawan::class);
    }
}
