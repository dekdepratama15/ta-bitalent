<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function penanggung_jawab()
    {
        return $this->belongsTo(User::class, 'penanggung_jawab_id');
    }

    public function posisi()
    {
        return $this->belongsTo(Posisi::class);
    }

    public function posisi_level()
    {
        return $this->belongsTo(PosisiLevel::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function cutis()
    {
        return $this->hasMany(Cuti::class);
    }

    public function kehadirans()
    {
        return $this->hasMany(Kehadiran::class);
    }

    public function lemburs()
    {
        return $this->hasMany(Lembur::class);
    }

    public function mutasis()
    {
        return $this->hasMany(Mutasi::class);
    }

    public function penggajians()
    {
        return $this->hasMany(Penggajian::class);
    }

    public function pinjamans()
    {
        return $this->hasMany(Pinjaman::class);
    }

    public function reimbursements()
    {
        return $this->hasMany(Reimbursement::class);
    }

    public function tunjangan_karyawans()
    {
        return $this->hasMany(TunjanganKaryawan::class);
    }
}
