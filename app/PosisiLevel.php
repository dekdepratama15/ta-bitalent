<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosisiLevel extends Model
{
    public function karyawans()
    {
        return $this->hasMany(Karyawan::class);
    }
}
