<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }
}
