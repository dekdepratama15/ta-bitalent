<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahIdSebelumMutasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mutasis', function (Blueprint $table) {
            $table->unsignedBigInteger('posisi_sebelum_id');
            $table->foreign('posisi_sebelum_id')->references('id')->on('posisis');

            $table->unsignedBigInteger('posisi_level_sebelum_id');
            $table->foreign('posisi_level_sebelum_id')->references('id')->on('posisi_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
