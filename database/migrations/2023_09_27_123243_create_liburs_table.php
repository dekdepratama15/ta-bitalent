<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liburs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_libur');
            $table->text('keterangan');
            $table->dateTime('tgl_mulai');
            $table->dateTime('tgl_selesai');
            $table->integer('jumlah_libur');
            $table->string('type', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liburs');
    }
}
