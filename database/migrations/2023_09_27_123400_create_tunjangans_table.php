<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTunjangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_tunjangan');
            $table->date('tanggal_pemberian');
            $table->string('type', 20);
            $table->string('jenis_tunjangan', 30);
            $table->text('perhitungan');
            $table->string('diambil_dari', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangans');
    }
}
