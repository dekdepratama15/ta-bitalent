<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggajianReimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggajian_reimbursements', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('penggajian_id');
            $table->foreign('penggajian_id')->references('id')->on('penggajians');

            $table->text('keterangan');
            $table->text('file');
            $table->string('total', 100);
            $table->string('status', 100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggajian_reimbursements');
    }
}
