<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLemburDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lembur_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('lembur_id');
            $table->foreign('lembur_id')->references('id')->on('lemburs');
            
            $table->text('keterangan');
            $table->text('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lembur_details');
    }
}
