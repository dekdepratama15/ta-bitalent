<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawans', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('posisi_id');
            $table->foreign('posisi_id')->references('id')->on('posisis');

            $table->unsignedBigInteger('posisi_level_id');
            $table->foreign('posisi_level_id')->references('id')->on('posisi_levels');

            $table->unsignedBigInteger('bank_id');
            $table->foreign('bank_id')->references('id')->on('banks');

            $table->unsignedBigInteger('penanggung_jawab_id');
            $table->foreign('penanggung_jawab_id')->references('id')->on('users');

            $table->string('nama_lengkap');
            $table->string('nama_panggilan', 100);
            $table->string('status', 20);
            $table->string('jenis_kelamin', 5);
            $table->string('golongan_darah', 5);
            $table->string('agama', 30);
            $table->string('status_perkawinan', 30);
            $table->string('status_karyawan', 30);
            $table->string('jenis_identitas', 30);
            $table->string('no_identitas', 50);
            $table->string('no_telp', 20);
            $table->string('tempat_lahir', 100);
            $table->dateTime('tgl_lahir');
            $table->text('alamat');
            $table->dateTime('tgl_bergabung');
            $table->dateTime('tgl_selesai');
            $table->string('gaji_pokok', 50);
            $table->string('no_akun_bank', 50);
            $table->integer('jumlah_cuti');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawans');
    }
}
