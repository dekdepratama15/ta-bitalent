<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLembur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lemburs', function ($table) {
            $table->dateTime('jam_mulai')->change();
            $table->dateTime('jam_selesai')->change();
            $table->string('bayar', 100)->default(0)->change();
            $table->string('total', 100)->default(0)->change();
            $table->float('jumlah_jam')->default(0)->change();
            $table->dropColumn('tanggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
