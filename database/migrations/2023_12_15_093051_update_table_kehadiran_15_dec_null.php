<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableKehadiran15DecNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kehadirans', function ($table) {
            $table->time('time_out')->nullable()->after('time_in');
            $table->string('radius_in', 100)->after('time_out');
            $table->string('radius_out', 100)->after('radius_in');
            $table->text('alamat_in')->nullable();
            $table->text('alamat_out')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
